#!/usr/bin/env python3
"""
Spring-mass system simulator

Usage:
    springmass.py [options] [<simulation-name>]

Options:
    --width=WIDTH           Width of output window, in pixels [default: 1024]
    --height=HEIGHT         Height of output window, in pixels [default: 576]
    --frame-rate=RATE       Attempted number of frames per second [default: 30]
    --ticks-per-frame=T     Number of simulation steps per frame [default: 1]
    --tracefile=FILENAME    File to which simulator will save all frame data
                            (optional)
"""
from datetime import timedelta
import math
import multiprocessing
import os
import pickle
import string
import sys
import time

from cpif.graphics import DrawingWindow
import docopt
import numpy as np

from .forcefield import (
    FrictionlessMass,
    MassWithFriction,
    ObjectWithMass,
    SimulatorBase,
)
from .simcommon import (
    FrameRecord,
    MassRecord,
    SimHeader,
    SpringRecord,
)


class SimulationFactory:
    """
    Each method in this class with a name beginning with "sim_" is a
    SpringMassSimulator factory method.

    Call list_simulations() to get a list of valid simulation names.

    Call get_simulation() with the name of a simulation to get a
    SpringMassSimulator object that is ready to run.
    """

    def list_simulations(self):
        """
        Return list of (sim_name, sim_description)
        """
        result = []
        for name in sorted(dir(self)):
            if not name.startswith('sim_'):
                continue
            sim_name = name[4:]
            sim_description = getattr(self, name).__doc__
            result.append((sim_name, sim_description))
        return result

    def validate_simulation_name(self, name_or_number):
        """
        Given the short name or integer index of a simulation, return the
        validated, normalized sim_name, or None.

        name_or_number:
            A short name of a simulation (which is the same as the method
            name minus a "sim_" prefix), or the one-based index of the
            method when the method names are listed in ASCII order.

        Return:
            A non-empty string, or None if the simulation does not exist.
        """
        if isinstance(name_or_number, int):
            return self._get_simulation_name_by_index(name_or_number)
        if not isinstance(name_or_number, str):
            raise TypeError("name_or_number parameter must be int or str")
        sim_name = name_or_number.strip().lower().replace(' ', '_')
        if sim_name[0] in string.digits:
            try:
                sim_index = int(sim_name)
            except ValueError:
                return None
            return self._get_simulation_name_by_index(sim_index)
        if hasattr(self, 'sim_' + sim_name):
            return sim_name
        return None

    def _get_simulation_name_by_index(self, index):
        simulations = self.list_simulations()
        if index < 1 or index > len(simulations):
            return None
        return simulations[index - 1][0]

    def get_simulation_constructor_by_name(self, sim_name):
        constructor_method = getattr(self, 'sim_' + sim_name, None)
        if constructor_method is None:
            return None
        return constructor_method

    def sim_simple(self):
        """Simple 1-spring 1-mass system attached to immovable post"""
        zero_force_distance = 0.150  # meters
        m1 = InexorableMass(
            m=1000.0,  # kg
            pos=(-(zero_force_distance / 2.0), 0.0),
            vel=(0.0, 0.0),
            r=0.030,
        )
        m2 = SpringyMass(
            m=1.0,  # kg
            pos=(0.000, 0.000),
            vel=(0.0, 0.0),
            resistance=0.5,
            r=0.015,
        )
        s1 = Spring(
            k=40.0,
            d=zero_force_distance,
            m1=m1,
            m2=m2,
        )
        return SpringMassSimulator(
            width=0.8,
            height=0.2,
            mass_list=[m1, m2],
            spring_list=[s1],
            dt=0.01,
        )

    def sim_compression_wave(self):
        """Simulate a compression wave traveling right."""
        k = 40.0  # spring constant
        n = 49  # number of masses other than the end blocks
        total_distance = 1.0  # meters
        d0 = total_distance / (n + 1)  # distance between masses
        dn = d0 / 8.0  # natural length of each spring
        v0 = (d0 * 0.9, 0.0)  # motion of driving mass
        t0 = 0.25  # time period of driving pulse
        x0 = -((n + 2 - 1) * d0)/2.0  # location of leftmost object
        r0 = d0 / 5.0
        m_block = 1000.0  # kg
        total_mass = 1.0  # kg
        m0 = total_mass / n
        mass_list = []
        spring_list = []
        for i in range(n + 2):
            if i == 0:
                obj = DrivingMass(
                    m=m_block,
                    pos=(x0 + (i * d0), 0.0),
                    motion_func=SinePulse(v0, t0, count=0.25),
                    r=r0,
                )
            elif i == (n + 2 - 1):
                obj = InexorableMass(
                    m=m_block,
                    pos=(x0 + (i * d0), 0.0),
                    vel=(0.0, 0.0),
                    r=r0,
                )
            else:
                obj = SpringyMass(
                    m=m0,
                    pos=(x0 + (i * d0), 0.0),
                    vel=(0.0, 0.0),
                    resistance=0.001,
                    r=r0,
                )
            if i > 0:
                spring = Spring(
                    k=k,
                    d=dn,
                    m1=mass_list[-1],
                    m2=obj,
                )
                spring_list.append(spring)
            mass_list.append(obj)
        return SpringMassSimulator(
            width=d0 * (n + 4),
            height=d0 * 3,
            mass_list=mass_list,
            spring_list=spring_list,
            dt=0.01,
        )

    def sim_compression_wave_frictionless(self):
        """Compression wave without friction"""
        k = 40.0  # spring constant
        n = 49  # number of masses other than the end blocks
        total_distance = 1.0  # meters
        d0 = total_distance / (n + 1)  # distance between masses
        dn = d0 / 8.0  # natural length of each spring
        v0 = (d0 * 0.9, 0.0)  # motion of driving mass
        t0 = 0.25  # time period of driving pulse
        x0 = -((n + 2 - 1) * d0)/2.0  # location of leftmost object
        r0 = d0 / 5.0
        m_block = 1000.0  # kg
        total_mass = 1.0  # kg
        m0 = total_mass / n
        mass_list = []
        spring_list = []
        for i in range(n + 2):
            if i == 0:
                obj = DrivingMass(
                    m=m_block,
                    pos=(x0 + (i * d0), 0.0),
                    motion_func=SinePulse(v0, t0, count=0.25),
                    r=r0,
                )
            elif i == (n + 2 - 1):
                obj = InexorableMass(
                    m=m_block,
                    pos=(x0 + (i * d0), 0.0),
                    vel=(0.0, 0.0),
                    r=r0,
                )
            else:
                obj = FrictionlessSpringyMass(
                    m=m0,
                    pos=(x0 + (i * d0), 0.0),
                    vel=(0.0, 0.0),
                    r=r0,
                )
            if i > 0:
                spring = Spring(
                    k=k,
                    d=dn,
                    m1=mass_list[-1],
                    m2=obj,
                )
                spring_list.append(spring)
            mass_list.append(obj)
        return SpringMassSimulator(
            width=d0 * (n + 4),
            height=d0 * 3,
            mass_list=mass_list,
            spring_list=spring_list,
            dt=0.01,
        )

    def sim_compression_wave_mixed(self):
        """Compression wave with mixed media."""
        k = 40.0  # spring constant
        n1 = 25  # first group of masses
        n2 = 24  # second group of masses
        n = n1 + n2
        total_distance = 1.0  # meters
        d0 = total_distance / (n + 1)  # distance between masses
        dn = d0 / 8.0  # natural length of each spring
        v0 = (d0 * 0.9, 0.0)  # motion of driving mass
        t0 = 0.25  # time period of driving pulse
        x0 = -((n + 2 - 1) * d0)/2.0  # location of leftmost object
        m_block = 1000.0  # kg
        total_mass = 1.0  # kg
        m2_m1_ratio = 3.0
        m1 = total_mass / (n1 + (m2_m1_ratio * n2))
        m2 = m1 * m2_m1_ratio
        r0 = d0 / 5.0
        r1 = r0 / (m2_m1_ratio ** 0.25)
        r2 = r1 * math.sqrt(m2_m1_ratio)
        mass_list = []
        spring_list = []
        for i in range(n + 2):
            if i == 0:
                obj = DrivingMass(
                    m=m_block,
                    pos=(x0 + (i * d0), 0.0),
                    motion_func=SinePulse(v0, t0, count=0.25),
                    r=r0,
                )
            elif i == (n + 2 - 1):
                obj = InexorableMass(
                    m=m_block,
                    pos=(x0 + (i * d0), 0.0),
                    vel=(0.0, 0.0),
                    r=r0,
                )
            else:
                if 0 < i <= n1:
                    m = m1
                    r = r1
                else:
                    m = m2
                    r = r2
                obj = SpringyMass(
                    m=m,
                    pos=(x0 + (i * d0), 0.0),
                    vel=(0.0, 0.0),
                    resistance=0.001,
                    r=r,
                )
            if i > 0:
                spring = Spring(
                    k=k,
                    d=dn,
                    m1=mass_list[-1],
                    m2=obj,
                )
                spring_list.append(spring)
            mass_list.append(obj)
        return SpringMassSimulator(
            width=d0 * (n + 4),
            height=d0 * 3,
            mass_list=mass_list,
            spring_list=spring_list,
            dt=0.01,
        )

    def sim_transverse_wave(self):
        """Simulate a transverse wave traveling right."""
        k = 40.0  # spring constant
        n = 49  # number of masses other than the end blocks
        total_distance = 1.0  # meters
        d0 = total_distance / (n + 1)  # distance between masses
        dn = d0 / 8.0  # natural length of each spring
        v0 = (0.0, d0 * 2.0)  # motion of driving mass
        t0 = 0.25  # time period of driving pulse
        x0 = -((n + 2 - 1) * d0)/2.0  # location of leftmost object
        m_block = 1000.0  # kg
        total_mass = 1.0  # kg
        m0 = total_mass / n
        r0 = d0 / 5.0
        mass_list = []
        spring_list = []
        for i in range(n + 2):
            if i == 0:
                obj = DrivingMass(
                    m=m_block,
                    pos=(x0 + (i * d0), 0.0),
                    motion_func=SinePulse(v0, t0),
                    r=r0,
                )
            elif i == (n + 2 - 1):
                obj = InexorableMass(
                    m=m_block,
                    pos=(x0 + (i * d0), 0.0),
                    vel=(0.0, 0.0),
                    r=r0,
                )
            else:
                obj = SpringyMass(
                    m=m0,
                    pos=(x0 + (i * d0), 0.0),
                    vel=(0.0, 0.0),
                    resistance=0.001,
                    r=r0,
                )
            if i > 0:
                spring = Spring(
                    k=k,
                    d=dn,
                    m1=mass_list[-1],
                    m2=obj,
                )
                spring_list.append(spring)
            mass_list.append(obj)
        return SpringMassSimulator(
            width=d0 * (n + 4),
            height=d0 * 6,
            mass_list=mass_list,
            spring_list=spring_list,
            dt=0.01,
        )

    def sim_transverse_wave_frictionless(self):
        """Simulate a transverse with frictionless medium."""
        k = 40.0  # spring constant
        n = 49  # number of masses other than the end blocks
        total_distance = 1.0  # meters
        d0 = total_distance / (n + 1)  # distance between masses
        dn = d0 / 8.0  # natural length of each spring
        v0 = (0.0, d0 * 2.0)  # motion of driving mass
        t0 = 0.25  # time period of driving pulse
        x0 = -((n + 2 - 1) * d0)/2.0  # location of leftmost object
        m_block = 1000.0  # kg
        total_mass = 1.0  # kg
        m0 = total_mass / n
        r0 = d0 / 5.0
        mass_list = []
        spring_list = []
        for i in range(n + 2):
            if i == 0:
                obj = DrivingMass(
                    m=m_block,
                    pos=(x0 + (i * d0), 0.0),
                    motion_func=SinePulse(v0, t0),
                    r=r0,
                )
            elif i == (n + 2 - 1):
                obj = InexorableMass(
                    m=m_block,
                    pos=(x0 + (i * d0), 0.0),
                    vel=(0.0, 0.0),
                    r=r0,
                )
            else:
                obj = FrictionlessSpringyMass(
                    m=m0,
                    pos=(x0 + (i * d0), 0.0),
                    vel=(0.0, 0.0),
                    r=r0,
                )
            if i > 0:
                spring = Spring(
                    k=k,
                    d=dn,
                    m1=mass_list[-1],
                    m2=obj,
                )
                spring_list.append(spring)
            mass_list.append(obj)
        return SpringMassSimulator(
            width=d0 * (n + 4),
            height=d0 * 6,
            mass_list=mass_list,
            spring_list=spring_list,
            dt=0.01,
        )

    def sim_transverse_wave_mixed(self):
        """Transverse wave with mixed media."""
        k = 40.0  # spring constant
        n1 = 25  # number of heavier masses
        n2 = 24  # number of lighter masses
        n = n1 + n2
        total_distance = 1.0  # meters
        d0 = total_distance / (n + 1)  # distance between masses
        dn = d0 / 8.0  # natural length of each spring
        v0 = (0.0, d0 * 2.0)  # motion of driving mass
        t0 = 0.25  # time period of driving pulse
        x0 = -((n + 2 - 1) * d0)/2.0  # location of leftmost object
        m_block = 1000.0  # kg
        total_mass = 1.0  # kg
        m2_m1_ratio = 3.0
        m1 = total_mass / (n1 + (m2_m1_ratio * n2))
        m2 = m1 * m2_m1_ratio
        r0 = d0 / 5.0
        r1 = r0 / (m2_m1_ratio ** 0.25)
        r2 = r1 * math.sqrt(m2_m1_ratio)
        mass_list = []
        spring_list = []
        for i in range(n + 2):
            if i == 0:
                obj = DrivingMass(
                    m=m_block,
                    pos=(x0 + (i * d0), 0.0),
                    motion_func=SinePulse(v0, t0),
                    r=r0,
                )
            elif i == (n + 2 - 1):
                obj = InexorableMass(
                    m=m_block,
                    pos=(x0 + (i * d0), 0.0),
                    vel=(0.0, 0.0),
                    r=r0,
                )
            else:
                if 0 < i <= n1:
                    m = m1
                    r = r1
                else:
                    m = m2
                    r = r2
                obj = SpringyMass(
                    m=m,
                    pos=(x0 + (i * d0), 0.0),
                    vel=(0.0, 0.0),
                    resistance=0.001,
                    r=r,
                )
            if i > 0:
                spring = Spring(
                    k=k,
                    d=dn,
                    m1=mass_list[-1],
                    m2=obj,
                )
                spring_list.append(spring)
            mass_list.append(obj)
        return SpringMassSimulator(
            width=d0 * (n + 4),
            height=d0 * 6,
            mass_list=mass_list,
            spring_list=spring_list,
            dt=0.01,
        )

    def sim_stretch_rope(self):
        """Horizontal line of objects stretched downward by gravity"""
        k = 200.0  # spring constant, Newtons / meter
        n = 49  # number of masses other than the end blocks
        total_distance = 1.0  # meters
        d0 = total_distance / (n + 1)  # distance between masses
        dn = d0 * 0.25  # natural length of each spring
        r = d0 / 4.0  # radius of each object
        x0 = -((n + 2 - 1) * d0)/2.0  # location of leftmost object
        y0 = total_distance / 4.0  # starting height of line of masses
        m_block = 1000.0  # kg
        total_mass = 1.0  # kg
        g = np.array((0.0, -9.8))  # acceleration of gravity, m / s**2
        m0 = total_mass / n
        mass_list = []
        spring_list = []
        for i in range(n + 2):
            if i == 0 or i == (n + 2 - 1):
                obj = InexorableMass(
                    m=m_block,
                    pos=(x0 + (i * d0), y0),
                    vel=(0.0, 0.0),
                    r=r,
                )
            else:
                obj = SpringyMass(
                    m=m0,
                    pos=(x0 + (i * d0), y0),
                    vel=(0.0, 0.0),
                    resistance=0.01,
                    r=r,
                )
                obj.set_const_force(g * m0)
            if i > 0:
                spring = Spring(
                    k=k,
                    d=dn,
                    m1=mass_list[-1],
                    m2=obj,
                )
                spring_list.append(spring)
            mass_list.append(obj)
        return SpringMassSimulator(
            width=d0 * (n + 4),
            height=(d0 * (n + 4)) / 2.0,
            mass_list=mass_list,
            spring_list=spring_list,
            dt=0.01,
        )

    def sim_decompress_grid(self):
        """Decompress a 2-d grid of spring-connected masses."""
        k = 0.8  # spring constant, Newtons / meter
        resistance = 0.025  # Newtons / (meter / second)
        n = 12  # number of columns of masses in grid
        m = 9  # number of rows of masses in grid
        grid_width = 1.0  # meters
        d0 = grid_width / (n - 1)  # starting distance between masses in grid
        dn = d0 * 2.5  # natural (uncompressed) spring distance
        r = min(dn, d0) / 4.0  # radius of each object
        x0 = -(d0 * (n - 1)) / 2.0  # starting location of left edge of grid
        y0 = -(d0 * (m - 1)) / 2.0  # starting location of bottom of grid
        total_mass = 1.0  # kg
        m0 = total_mass / (m * n)  # mass in kg of each object in grid
        mass_list = []
        spring_list = []
        for i in range(m):
            for j in range(n):
                obj = SpringyMass(
                    m=m0,
                    pos=(x0 + (j * d0), y0 + (i * d0)),
                    vel=(0.0, 0.0),
                    resistance=resistance,
                    r=r,
                )
                if j > 0:
                    # Connect this mass to the one to the left
                    spring = Spring(
                        k=k,
                        d=dn,
                        m1=mass_list[-1],
                        m2=obj,
                    )
                    spring_list.append(spring)
                mass_list.append(obj)
            if i > 0:
                # Connect each mass in this row to the one in the row above
                for j in range(n):
                    spring = Spring(
                        k=k,
                        d=dn,
                        m1=mass_list[-((2 * n) - j)],
                        m2=mass_list[-(n - j)],
                    )
                    spring_list.append(spring)
        return SpringMassSimulator(
            width=(max(dn, d0) * (n + 1)),
            height=(max(dn, d0) * (m + 1)),
            mass_list=mass_list,
            spring_list=spring_list,
            dt=0.01,
        )

    def sim_plane_wave(self):
        """
        Send impulse through 2-D grid mesh of springs and masses.
        """
        k = 40.0  # spring constant, Newtons / meter
        resistance = 0.001  # Newtons / (meter / second)
        n = 15  # number of columns of masses in grid
        m = 15  # number of rows of masses in grid
        grid_width = 1.0  # meters
        d0 = grid_width / (n - 1)  # starting distance between masses in grid
        dn = d0 * 1.0  # natural (uncompressed) spring distance
        v0 = (d0 * 0.9, 0.0)  # motion of driving mass
        t0 = 0.5  # time period of driving pulse
        r = min(dn, d0) / 4.0  # radius of each object
        x0 = -(d0 * (n - 1)) / 2.0  # starting location of left edge of grid
        y0 = -(d0 * (m - 1)) / 2.0  # starting location of bottom of grid
        total_mass = 1.0  # kg
        m0 = total_mass / (m * n)  # mass in kg of each object in grid
        m_block = 1000.0  # kg
        mass_list = []
        spring_list = []
        driver_col = 0
        driver_row = m // 2
        for i in range(m):
            for j in range(n):
                if i == driver_row and j == driver_col:
                    # Instantiate driver
                    obj = DrivingMass(
                        m=m_block,
                        pos=(x0 + (j * d0), y0 + (i * d0)),
                        motion_func=SinePulse(v0, t0, count=1.0),
                        r=r,
                    )
                elif (i == 0 or i == m - 1 or j == 0 or j == n - 1):
                    # Put blocks on edges
                    obj = InexorableMass(
                        m=m_block,
                        pos=(x0 + (j * d0), y0 + (i * d0)),
                        vel=(0.0, 0.0),
                        r=r,
                    )
                else:
                    # Create a regular mass
                    obj = SpringyMass(
                        m=m0,
                        pos=(x0 + (j * d0), y0 + (i * d0)),
                        vel=(0.0, 0.0),
                        resistance=resistance,
                        r=r,
                    )
                if j > 0:
                    # Connect this mass to the one to the left
                    spring = Spring(
                        k=k,
                        d=dn,
                        m1=mass_list[-1],
                        m2=obj,
                    )
                    spring_list.append(spring)
                mass_list.append(obj)
            if i > 0:
                # Connect each mass in this row to the one in the row above
                for j in range(n):
                    spring = Spring(
                        k=k,
                        d=dn,
                        m1=mass_list[-((2 * n) - j)],
                        m2=mass_list[-(n - j)],
                    )
                    spring_list.append(spring)
        return SpringMassSimulator(
            width=(max(dn, d0) * (n + 1)),
            height=(max(dn, d0) * (m + 1)),
            mass_list=mass_list,
            spring_list=spring_list,
            dt=0.01,
        )

    def sim_square_collapse(self):
        """
        A grid of squares collapses under gravity
        """
        k = 256.0  # spring constant, Newtons / meter
        resistance = 0.01  # Newtons / (meter / second)
        n = 15  # number of columns of masses in grid
        m = 15  # number of rows of masses in grid
        grid_width = 1.0  # meters
        d0 = grid_width / (n - 1)  # starting distance between masses in grid
        dn = d0 * 1.0  # natural (uncompressed) spring distance
        v0 = (d0 * 0.9, 0.0)  # motion of driving mass
        t0 = 0.5  # time period of driving pulse
        r = min(dn, d0) / 4.0  # radius of each object
        x0 = -(d0 * (n - 1)) / 2.0  # starting location of left edge of grid
        y0 = -(d0 * (m - 1)) / 2.0  # starting location of bottom of grid
        total_mass = 1.0  # kg
        g = np.array((0.0, -9.8))  # acceleration of gravity, m / s**2
        m0 = total_mass / (m * n)  # mass in kg of each object in grid
        m_block = 1000.0  # kg
        mass_list = []
        spring_list = []
        for i in range(m):
            for j in range(n):
                if i == 0:
                    # Put blocks on bottom edge
                    obj = InexorableMass(
                        m=m_block,
                        pos=(x0 + (j * d0), y0 + (i * d0)),
                        vel=(0.0, 0.0),
                        r=r,
                    )
                else:
                    # Create a regular mass
                    obj = SpringyMass(
                        m=m0,
                        pos=(x0 + (j * d0), y0 + (i * d0)),
                        vel=(0.0, 0.0),
                        resistance=resistance,
                        r=r,
                    )
                    obj.set_const_force(g * m0)
                if i > 0 and j > 0:
                    # Connect this mass to the one to the left
                    spring = Spring(
                        k=k,
                        d=dn,
                        m1=mass_list[-1],
                        m2=obj,
                    )
                    spring_list.append(spring)
                mass_list.append(obj)
            if i > 0:
                # Connect each mass in this row to the one in the row below
                for j in range(n):
                    spring = Spring(
                        k=k,
                        d=dn,
                        m1=mass_list[-((2 * n) - j)],
                        m2=mass_list[-(n - j)],
                    )
                    spring_list.append(spring)
        return SpringMassSimulator(
            width=(max(dn, d0) * (n + 1)),
            height=(max(dn, d0) * (m + 1)),
            mass_list=mass_list,
            spring_list=spring_list,
            dt=0.0025,
        )

    def sim_triangle_stand(self):
        """
        A pyramid made of triangles can stand under pressure
        """
        k = 256.0  # spring constant, Newtons / meter
        resistance = 0.01  # Newtons / (meter / second)
        n = 15  # number of masses at base of pyramid
        grid_width = 1.0  # meters
        d0 = grid_width / (n - 1)  # horizontal distance between masses in grid
        h0 = d0 * math.sin(math.pi / 3.0) # vertical distance between rows
        dn = d0 * 1.0  # natural (uncompressed) spring distance
        v0 = (d0 * 0.9, 0.0)  # motion of driving mass
        t0 = 0.5  # time period of driving pulse
        r = min(dn, d0) / 4.0  # radius of each object
        x0 = -(d0 * (n - 1)) / 2.0  # x coordinate of bottom-left corner
        y0 = -(h0 * (n - 1)) / 2.0  # y coordinate of bottom-left corner
        total_mass = 1.0  # kg
        g = np.array((0.0, -9.8))  # acceleration of gravity, m / s**2
        m0 = total_mass / ((n * (n - 1)) / 2)  # mass of each object in grid
        m_block = 1000.0  # kg
        mass_list = []
        spring_list = []
        for i in range(n):
            for j in range(n - i):
                x = x0 + (i * (d0 / 2.0)) + (j * d0)
                y = y0 + (i * h0)
                if i == 0:
                    # Put blocks on bottom edge
                    obj = InexorableMass(
                        m=m_block,
                        pos=(x, y),
                        vel=(0.0, 0.0),
                        r=r,
                    )
                else:
                    # Create a regular mass
                    obj = SpringyMass(
                        m=m0,
                        pos=(x, y),
                        vel=(0.0, 0.0),
                        resistance=resistance,
                        r=r,
                    )
                    obj.set_const_force(g * m0)
                if i > 0 and j > 0:
                    # Connect this mass to the one to the left
                    spring = Spring(
                        k=k,
                        d=dn,
                        m1=mass_list[-1],
                        m2=obj,
                    )
                    spring_list.append(spring)
                mass_list.append(obj)
            if i > 0:
                # Connect each mass in this row to two in the row below
                for j in range(n - i):
                    obj1 = mass_list[i - n + j]
                    obj2 = mass_list[(2 * (i - n)) + j - 1]
                    obj3 = mass_list[(2 * (i - n)) + j]
                    spring = Spring(
                        k=k,
                        d=dn,
                        m1=obj1,
                        m2=obj2,
                    )
                    spring_list.append(spring)
                    spring = Spring(
                        k=k,
                        d=dn,
                        m1=obj1,
                        m2=obj3,
                    )
                    spring_list.append(spring)
        return SpringMassSimulator(
            width=(max(dn, d0) * (n + 2)),
            height=(max(dn, h0) * (n + 1)),
            mass_list=mass_list,
            spring_list=spring_list,
            dt=0.0025,
        )


class SpringyMass(MassWithFriction):

    def __init__(self, m, pos, vel, resistance, r):
        """
        Object with mass
        m: Mass in kg
        pos: Position in meters
        vel: Velocity in meters per second
        resistance: Friction coefficient in Newtons / (m/s)
        r: Radius in meters
        """
        super(SpringyMass, self).__init__(m, pos, vel, resistance)
        self.r = r

    def get_snapshot(self):
        return MassRecord(
            type='mass',
            r=self.r,
            x=self.x,
            y=self.y,
        )


class FrictionlessSpringyMass(FrictionlessMass):

    def __init__(self, m, pos, vel, r):
        """
        Object with mass
        m: Mass in kg
        pos: Position in meters
        vel: Velocity in meters per second
        resistance: Friction coefficient in Newtons / (m/s)
        r: Radius in meters
        """
        super(FrictionlessSpringyMass, self).__init__(m, pos, vel)
        self.r = r

    def get_snapshot(self):
        return MassRecord(
            type='mass',
            r=self.r,
            x=self.x,
            y=self.y,
        )


class InexorableMass(ObjectWithMass):
    """An object considered so massive its velocity cannot change."""

    def __init__(self, m, pos, vel, r):
        """Mass is recorded just for curiousity. This thing has effectively
        infinite mass."""
        super(InexorableMass, self).__init__(m, pos, vel)
        self.r = r

    def time_step_start(self, dt):
        """
        Calculate the first approximation of the next position, velocity,
        and force.
        """
        self.npos = self.pos + (self.vel * dt)
        self.nvel[:] = self.vel[:]
        self.nf[:] = self.f[:]

    def time_step_refine(self):
        """
        Calculate the next approximation of the next position and velocity.
        """
        pass

    def get_snapshot(self):
        return MassRecord(
            type='block',
            r=self.r,
            x=self.x,
            y=self.y,
        )


class DrivingMass(ObjectWithMass):
    """Object with pre-defined non-linear motion unaffected by force."""

    def __init__(self, m, pos, motion_func, r):
        """
        m: Nominal mass. Recorded for audit purposes only.
        pos: Starting position vector.
        motion_func: Callable object called with dt (time step), returns
            the current position relative to the starting position.
        r: Radius of object.
        """
        pos = self._make_vector(pos)
        self.starting_pos = pos[:]
        super(DrivingMass, self).__init__(m, pos, np.zeros(pos.shape))
        self.motion_func = motion_func
        self.r = r

    def time_step_start(self, dt):
        """
        Calculate the first approximation of the next position, velocity,
        and force.
        """
        self.npos = self.starting_pos + self.motion_func(dt)
        self.nvel[:] = self.vel[:]
        self.nf[:] = self.f[:]

    def time_step_refine(self):
        """
        Calculate the next approximation of the next position and velocity.
        """
        pass

    def get_snapshot(self):
        return MassRecord(
            type='driver',
            r=self.r,
            x=self.x,
            y=self.y,
        )


class SinePulse:
    """
    Callable object to generate a back-and forth motion.

    Suitable for passing as a motion_func parameter to DrivingMass. The
    motion is generated using the sine function.
    """

    def __init__(self, vec, period, count=1.0):
        """
        vec:
            The motion vector.  The direction of this vector is the
            direction in which the pulse will move first. The magnitude of
            this vector is the maximum distance the pulse will move.
        period:
            The time it will take to move from the starting position to the
            end of the vector, back to the starting position, to the end of
            the vector in the opposite direction, and then back to the
            starting position again. (One full sine wave.)
        count:
            The number of full sine wave cycles to execute, before stopping
            motion. Use 0.5 to just do the positive portion of the cycle.
        Return:
            The current pulse postion (a vector)
        """
        vec = np.array(vec)
        if not vec.shape:
            vec.shape = (1,)
        period = float(period)
        count = float(count)
        if period <= 0.0:
            raise ValueError("period must be a positive number")
        if count < 0.0:
            raise ValueError("count must be non-negative")
        self.vec = vec
        self.omega = (2.0 * math.pi) / period
        self.time_limit = period * count
        self.t = 0.0

    def __call__(self, dt):
        """
        Return the change in position given the change in time dt.
        """
        if not dt >= 0.0:
            raise ValueError("dt must be non-negative")
        self.t = min(self.t + dt, self.time_limit)
        return math.sin(self.t * self.omega) * self.vec


class Spring:

    def __init__(self, k, d, m1, m2):
        """
        Ideal spring
        k: Spring constant, in N*s/m
        d: Zero force distance in meters
        m1, m2: SpringyMass objects between which the spring is connected
        """
        self.k = k
        self.d = d
        self.m1 = m1
        self.m2 = m2

    def get_snapshot(self):
        return SpringRecord(
            d=self.d,
            x1=self.m1.x,
            y1=self.m1.y,
            x2=self.m2.x,
            y2=self.m2.y,
        )


class SpringMassSimulator(SimulatorBase):

    def __init__(self, width, height, mass_list, spring_list, dt):
        """
        width, height: Arena dimensions in meters
        mass_list: List of SpringyMass instances to simulate
        spring_list: List of Spring instances to simulate
        dt: delta time, the amount of time in seconds per simulation step
        """
        super(SpringMassSimulator, self).__init__(mass_list, dt)
        self.width = width
        self.height = height
        self.spring_list = spring_list

    def get_snapshot(self):
        masses = [m.get_snapshot() for m in self.object_list]
        springs = [s.get_snapshot() for s in self.spring_list]
        frame = FrameRecord(
            t=self.t,
            masses=masses,
            springs=springs,
        )
        return frame

    def update_forces(self, pos_getter_func):
        for spring in self.spring_list:
            m1 = spring.m1
            m2 = spring.m2
            pos1 = pos_getter_func(m1)
            pos2 = pos_getter_func(m2)
            # d_vec is the direction vector from m1 to m2
            d_vec = pos2 - pos1
            # d is the distance between m1 and m2
            d = np.linalg.norm(d_vec)
            # f is the magnitude of the force due to the spring.
            # If f is positive, spring is compressed, therefore pushing m1
            # and m2 away from each other. If f is negative, spring is
            # stretched, pulling m1 and m2 towards each other.
            f = spring.k * (spring.d - d)
            if d == 0:
                # If distance is zero, direction of force cannot be
                # determined. So pick an arbitrary direction.
                f_vec = np.zeros(m1.dimensions)
                f_vec[0] = f
            else:
                f_vec = (f * d_vec) / d
            m1.accum_force -= f_vec
            m2.accum_force += f_vec


class Animator:

    def __init__(self, pixel_width, pixel_height, frame_rate,
                 conn):
        """
        pixel_width, pixel_height: size of simulator window in pixels
        frame_rate: The number of times per second the simulation updates
        conn: A multiprocessing Connection object, connected to the
            simulator process.
        """
        self.pixel_width = pixel_width
        self.pixel_height = pixel_height
        self.frame_rate = frame_rate
        self.conn = conn
        #####
        # These are None until first message received from simulator process
        self.meter_width = None
        self.meter_height = None
        self.pixels_per_meter = None
        self.meters_per_pixel = None
        #####
        self._dw = None
        self._time_drawing_id = None
        self._button_drawing_id = None
        self._objs_drawing_id = None
        self._num_frames_requested = 0
        self._num_frames = 0
        self._animation_is_running = False
        self._start_time = None
        self._cur_frame = None

    def _rescale(self):
        if self.meter_width is None:
            return False
        meter_width = self.meter_width
        meter_height = self.meter_height
        pixel_aspect_ratio = self.pixel_width / self.pixel_height
        meter_aspect_ratio = meter_width / meter_height
        if pixel_aspect_ratio >= meter_aspect_ratio:
            # Height controls scale
            self.pixels_per_meter = self.pixel_height / meter_height
            self.meters_per_pixel = meter_height / self.pixel_height
        else:
            # Width controls scale
            self.pixels_per_meter = self.pixel_width / meter_width
            self.meters_per_pixel = meter_width / self.pixel_width
        return True

    def _get_message(self):
        # Check for an incoming message.
        # Return None if we are disconnected.
        # Return False if there was not a message.
        # Otherwise, return the message object.
        # Obviously, receiving None or False as a message is not supported.
        if self.conn is None:
            return None
        if not self.conn.poll():
            return False
        try:
            return self.conn.recv()
        except EOFError:
            self.conn.close()
            self.conn = None
            return None

    def _update(self):
        msg = self._get_message()
        if msg is None:
            # We've been suddenly disconnected from the simulator.
            msg = "Simulator crashed"
            print(msg)
            self._dw.draw_text(0, 0, msg, 'red', font_size=DrawingWindow.HUGE)
            # Don't schedule another update.
            return
        if msg is not False:
            if isinstance(msg, SimHeader):
                # Got header message from simulator.
                # It should contain the arena (width, height) in meters.
                self.meter_width, self.meter_height = msg.width, msg.height
                print("Received from simulator width, height:",
                        (self.meter_width, self.meter_height))
                self._rescale()
            elif isinstance(msg, FrameRecord):
                # Got frame message from simulator.
                # Re-draw the objects in the window.
                self._cur_frame = msg
                self._redraw()
                self._num_frames += 1
        if (self.meter_width is not None and (
                self._num_frames_requested == 0 or (
                    self._animation_is_running and 
                    self._num_frames_requested <= self._num_frames))):
            # Signal the simulator to send another frame
            self.conn.send(self._num_frames_requested)
            self._num_frames_requested += 1
        # Schedule the next update
        t = time.time()
        start_time = self._start_time if self._start_time is not None else t
        delay_sec = max(0.001, (1.0 / self.frame_rate) - (t - start_time))
        self._start_time = t
        self._dw.after_delay_call(delay_sec, self._update)

    def _on_resize(self):
        self.pixel_width, self.pixel_height = self._dw.get_size()
        self._dw.clear()
        self._rescale()
        self._redraw()

    def _calc_time_str(self, t):
        return "Time: {0}\nNum. frames: {1}".format(
                timedelta(seconds=t), self._num_frames)

    def _redraw_button(self):
        # (Re-)Draw the current button
        if self._button_drawing_id is not None:
            self._dw.delete(self._button_drawing_id)
        self._button_drawing_id = self._dw.begin_drawing()
        if self._animation_is_running:
            button_label = "Stop"
            button_color = 'Red'
        else:
            button_label = "Start"
            button_color = 'Green'
        w, h = self._dw.get_text_size('M')
        width, height = self._dw.get_text_size(button_label)
        x = self._dw.width - width - (3 * w)
        y = h
        self._dw.box(x, y, x+width+(2*w), y+height+(2*h), 'black',
                fill=button_color)
        self._dw.draw_text(x+w, y+h, button_label, 'white')
        self._dw.end_drawing()
        self._dw.on_click_call(self._button_drawing_id, self._on_button_click)

    def _on_button_click(self, mouse_event):
        self._animation_is_running = not self._animation_is_running
        self._start_time = None

    def _redraw(self):
        frame = self._cur_frame
        if frame is None:
            self._dw.clear()
            return
        t = frame.t
        pixels_per_meter = self.pixels_per_meter
        cx = int(self.pixel_width / 2)
        cy = int(self.pixel_height / 2)
        dw = self._dw
        # (Re-)Draw the current time
        if self._time_drawing_id is not None:
            dw.delete(self._time_drawing_id)
        time_str = self._calc_time_str(t)
        self._time_drawing_id = dw.draw_text(0, 0, time_str)
        self._redraw_button()
        # Delete previous object drawings
        if self._objs_drawing_id is not None:
            dw.delete(self._objs_drawing_id)
        self._objs_drawing_id = dw.begin_drawing()
        for m in frame.masses:
            x = cx + (m.x * pixels_per_meter)
            y = cy - (m.y * pixels_per_meter)
            r = m.r * pixels_per_meter
            if m.type == 'block':
                dw.box(x-r, y-r, x+r, y+r, 'black', fill='gray')
            elif m.type == 'driver':
                dw.polygon([(x, y-r), (x+r, y), (x, y+r), (x-r, y)],
                        'black', fill='gray')
            else:
                dw.circle(x, y, r, 'black', fill='gray')
        for s in frame.springs:
            x1 = cx + (s.x1 * pixels_per_meter)
            y1 = cy - (s.y1 * pixels_per_meter)
            x2 = cx + (s.x2 * pixels_per_meter)
            y2 = cy - (s.y2 * pixels_per_meter)
            d = math.hypot(s.x1 - s.x2, s.y1 - s.y2)
            if d > s.d:
                # Stretched spring
                red = 1.0 - math.exp(2 * (s.d - d)/d)
                color = (red, 0.0, 0.0)
            else:
                # Compressed spring
                x = d / s.d
                blue = math.cos((x*x) * math.pi / 2.0)
                color = (0.0, 0.0, blue)
            dw.line(x1, y1, x2, y2, color, width=3)
        dw.end_drawing()

    def _on_delete_window(self):
        if self.conn is not None:
            print("Sending to simulator shutdown message")
            self.conn.send(None)
            self.conn.close()
            self.conn = None
    
    def run(self):
        self._dw = DrawingWindow(self.pixel_width, self.pixel_height)
        self._dw.title("Spring-Mass Simulator")
        self._dw.on_resize_call(self._on_resize)
        self._dw.on_delete_window_call(self._on_delete_window)
        self._start_time = None  # animation not running yet
        self._dw.after_delay_call(0.0, self._update)
        self._dw.run()


class StopSimulation(Exception):
    """Exception raised to stop the simulation loop."""
    pass


class SimulatorRunner:

    def __init__(self, sim, ticks_per_frame, conn, tracefile=None):
        """
        sim: Simulator object
        ticks_per_frame: int, number of simulation steps per frame
        conn: Connection object to communicate with animator
        tracefile: If not None, file object to store frames
        """
        self.sim = sim
        self.ticks_per_frame = ticks_per_frame
        self.conn = conn
        self.tracefile = tracefile
        self.pickle_protocol = max(pickle.DEFAULT_PROTOCOL, 4)
        assert self.pickle_protocol <= pickle.HIGHEST_PROTOCOL
        #####
        self.tick_count = 0
        self.num_frames_sent = 0
        self.last_frame_requested = None

    def _get_message(self, timeout=0):
        # Read and and interpret a message from the Animator
        # (simulation controller).
        # Return None if the connection had already been closed.
        # Raise StopSimulation if a stop message is received.
        # Return False if a timeout occurred before a message was
        # received, True otherwise.
        if self.conn is None:
            return None
        if not self.conn.poll(timeout):
            return False
        try:
            msg = self.conn.recv()
        except EOFError:
            # The child process doesn't normally get this exception, but
            # just in case, we'll handle it.
            msg = None
        if msg is None:
            # Shutdown message
            self.conn.close()
            self.conn = None
            raise StopSimulation()
        if isinstance(msg, int):
            self.last_frame_requested = msg
        else:
            print("Simulator received unexpected message:", msg,
                    file=sys.stderr)
        return True

    def _animator_is_ready(self):
        # Return true iff the Animator has signaled that it is ready for the
        # Simulator to send another frame.
        result = (self.last_frame_requested is not None and
                (self.last_frame_requested + 1) > self.num_frames_sent)
        return result

    def _send_data(self, data):
        data_bytes = pickle.dumps(data, self.pickle_protocol)
        if self.tracefile is not None:
            self.tracefile.write(data_bytes)
        try:
            if self.conn is not None:
                self.conn.send_bytes(data_bytes)
        except BrokenPipeError:
            self.conn.close()
            self.conn = None
            raise StopSimulation()

    def _send_frame(self, frame):
        # Send a frame and increment the sent frame count.
        self._send_data(frame)
        self.num_frames_sent += 1

    def run(self):
        sim = self.sim
        # Send first message back, containing arena size in meters
        print("Sending to animator width, height:", (sim.width, sim.height))
        self._send_data(SimHeader(width=sim.width, height=sim.height))
        try:
            while True:
                # Wait until Animator is ready, then send frame.
                frame = sim.get_snapshot()
                while not self._animator_is_ready():
                    self._get_message(timeout=5.0)
                self._send_frame(frame)
                for i in range(self.ticks_per_frame):
                    while self._get_message():
                        continue
                    sim.time_step()
                    self.tick_count += 1
        except StopSimulation:
            pass


def simulator_main(sim_constructor, ticks_per_frame, conn, tracefile_name):
    print("Entering simulator_main")
    tracefile = None
    try:
        sim = sim_constructor()
        if sim is None:
            print("simulator_main: Invalid sim_constructor", file=sys.stderr)
            return
        if not (isinstance(ticks_per_frame, int) and ticks_per_frame >= 1):
            print("simulator_main: ticks_per_frame must be an integer >= 1",
                    file=sys.stderr)
            return
        if tracefile_name:
            tracefile = open(tracefile_name, 'wb')
        runner = SimulatorRunner(sim, ticks_per_frame, conn, tracefile)
        runner.run()
    finally:
        print("Leaving simulator_main")
        conn.close()
        if tracefile is not None:
            tracefile.close()


def ensure_relative_filepath(filepath):
    """
    Check the filepath to ensure that it is relative, not absolute, and
    doesn't use "..." to try to crawl into some other direcotry.
    Return the normalized filepath, or None if it is unacceptable.
    """
    filepath = os.path.normpath(filepath)
    if os.path.isabs(filepath):
        return None
    if filepath.startswith(os.path.pardir):
        return None
    return filepath


def run_simulation(factory, sim_name, width=1024, height=576, frame_rate=30,
                   ticks_per_frame=1, tracefile_name=None):
    """
    factory: A SimulationFactory instance
    sim_name: A simulation name
    width: Width in pixels of graphics window
    height: Height in pixels of graphics window
    frame_rate: Attempted number of rendered frames per second
    ticks_per_frame: Number of simulation steps per frame
    tracefile_name: File to which simulator will save all frame data (optional)
    """
    sim_constructor = factory.get_simulation_constructor_by_name(sim_name)

    # Create communication pipe.
    # Communication Notes:
    # When a child process closes its connection, the parent will get an
    # EOFError the next time it calls recv() on its connection. But the
    # reverse is not true! Therefore the parent has to send a None sentinel
    # value to shut down a child.
    render_conn, sim_conn = multiprocessing.Pipe()

    # Create the Animator
    animator = Animator(
        pixel_width=width,
        pixel_height=height,
        frame_rate=frame_rate,
        conn=render_conn,
    )

    # Create and start the simulator process.
    process = multiprocessing.Process(
        target=simulator_main,
        kwargs=dict(
            sim_constructor=sim_constructor,
            ticks_per_frame=ticks_per_frame,
            conn=sim_conn,
            tracefile_name=tracefile_name,
        ),
    )
    process.start()
    sim_conn.close()  # Animator doesn't use this connection object.

    # Run the Animator till done.
    animator.run()
    process.join()


def input_sim_name(factory):
    """
    factory: A SimulationFactory instance
    """
    simulations = factory.list_simulations()
    while True:
        print("Simulation scenarios:")
        print()
        for i, (sim_name, sim_description) in enumerate(simulations):
            if sim_description is None:
                sim_description = ''
            print("{0:2d} {1:10} {2:40}".format(i + 1, sim_name,
                                                sim_description.strip()))
        print()
        prompt = "Enter scenario name or number, or press Enter to quit: "
        name = input(prompt).strip().lower()
        if not name:
            print("Goodbye.")
            return None
        sim_name = factory.validate_simulation_name(name)
        if sim_name is not None:
            return sim_name
        print("That was not a valid scenario name.")
        print("Please choose from the list below, or press Enter to quit.")


def main():
    args = docopt.docopt(__doc__)
    width = int(args['--width'])  # pixels
    height = int(args['--height'])  # pixels
    frame_rate = int(args['--frame-rate'])  # updates per second
    ticks_per_frame = int(args['--ticks-per-frame'])
    tracefile_name = args['--tracefile']
    if tracefile_name:
        normalized_tracefile_name = ensure_relative_filepath(tracefile_name)
        if normalized_tracefile_name is None:
            raise ValueError(
                "Trace file must be relative to simulation directory: {}"
                .format(tracefile_name))
        tracefile_name = normalized_tracefile_name
    sim_name = args['<simulation-name>']

    factory = SimulationFactory()
    if sim_name:
        validated_sim_name = factory.validate_simulation_name(sim_name)
        if not validated_sim_name:
            print("Invalid simulation name:", sim_name, file=sys.stderr)
            return 1
        sim_name = validated_sim_name

    if sim_name:
        run_simulation(factory, sim_name,
                       width=width, height=height,
                       frame_rate=frame_rate, ticks_per_frame=ticks_per_frame,
                       tracefile_name=tracefile_name)
        return 0

    while True:
        sim_name = input_sim_name(factory)
        if sim_name is None:
            return 0
        run_simulation(factory, sim_name,
                       width=width, height=height,
                       frame_rate=frame_rate, ticks_per_frame=ticks_per_frame,
                       tracefile_name=tracefile_name)


if __name__ == '__main__':
    multiprocessing.set_start_method('spawn')
    sys.exit(main())
