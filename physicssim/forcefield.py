#!/usr/bin/env python3
"""
Force Field Simulator
"""

import math
import operator
import sys

import numpy as np
from numpy.polynomial import Polynomial as P


class SimulatorBase:
    """
    Base class for simulating motions of particles.

    object_list: List of objects derived from ObjectWithMass
    dt: Delta Time, in seconds, for each time step.
    """

    num_refinement_steps = 1

    def __init__(self, object_list, dt):
        self.object_list = object_list
        self.dt = dt
        self.t = 0.0
        self.max_err = 0.0

    def time_step(self):
        """
        Calculate one time step.
        Update the current time t and the maximum error estimate max_err.
        """
        dt = self.dt
        if self.t == 0.0:
            # One-time initialization
            self.update_forces(operator.attrgetter('pos'))
            for obj in self.object_list:
                obj.initialize_force(dt)
        max_err = 0.0
        pos_getter_func = operator.attrgetter('npos')
        for obj in self.object_list:
            obj.time_step_start(dt)
        for i in range(self.num_refinement_steps):
            self.update_forces(pos_getter_func)
            for obj in self.object_list:
                obj.update_next_force()
                obj.time_step_refine()
        self.update_forces(pos_getter_func)
        for obj in self.object_list:
            max_err = max(obj.update_next_force(), max_err)
            obj.time_step_stop()
        self.max_err = max_err
        self.t += dt

    def update_forces(self, pos_getter_func):
        """
        Calculate the external force on every object and add it to each
        object's accum_force attribute. pos_getter_func() is a callable that
        takes an ObjectWithMass as a parameter and returns the position of
        interest (the pos or npos attribute.)
        """
        raise NotImplementedError()
        #####
        # Example code that calculates force on each object based on its
        # relationship with every other object.
        object_list = self.object_list
        for i in range(len(object_list) - 1):
            obj = object_list[i]
            for j in range(i + 1, len(object_list)):
                other_obj = object_list[j]
                # Calculate force at current position
                f = calc_force_callback(obj, other_obj, pos_getter_func)
                obj.accum_force += f
                other_obj.accum_force -= -f


class ObjectWithMass:
    """
    An object that moves through a force field according to Newton's laws of
    motion.
    """

    def __init__(self, m, pos, vel):
        """
        m: Mass of object
        pos: Initial position of object (vector)
        vel: Initial velocity of object (vector)
        """
        self.m = m
        self.pos = self._make_vector(pos)
        self.vel = self._make_vector(vel)
        if self.pos.shape != self.vel.shape:
            raise ValueError("Position and velocity different sized vectors")
        if len(self.pos.shape) > 1:
            raise ValueError("Position must be vector, not matrix")
        self.dimensions = self.pos.shape[0]
        # Initial force field value unknown at this time
        self.f = None
        # Constant force, if any
        self.f_const = np.zeros(self.dimensions)
        # Accumulated force, used to calculate next force
        self.accum_force = np.zeros(self.dimensions)
        # Next force, velocity, position are all unknown at this time
        self.nf = None
        self.nvel = None
        self.npos = None

    @property
    def x(self):
        return self.pos[0]

    @property
    def y(self):
        return self.pos[1]

    @property
    def z(self):
        return self.pos[2]

    @property
    def vx(self):
        return self.vel[0]

    @property
    def vy(self):
        return self.vel[1]

    @property
    def vz(self):
        return self.vel[2]

    def set_const_force(self, f_const):
        """
        Set a constant force (could be gravity) which remains unchanged from
        one time step to another. This value stays set until changed by
        again calling this method.
        """
        self.f_const = self._make_vector(f_const)

    def initialize_force(self, dt):
        """
        Set initial force field vector while at starting position
        """
        self.f = self.accum_force
        self.accum_force = np.zeros(self.dimensions)
        if self.f.shape != self.pos.shape:
            raise ValueError("Force and position are different sized vectors")
        self.nf = np.array(self.f)
        self.nvel = np.zeros(self.dimensions)
        self.npos = np.zeros(self.dimensions)

    def update_next_force(self):
        """
        Replace the previously estimated next force field vector with the
        accumulated external force, which has been calculated from the force
        field given the current estimated next position.
        Return the estimated magnitude of force error -- the absolute value
        of the difference between the previously estimated next force and
        this supplied force value.
        """
        nf = self.accum_force
        self.accum_force = np.zeros(self.dimensions)
        err = np.linalg.norm(nf - self.nf)
        self.nf = nf
        return err

    def time_step_stop(self):
        """
        Accept the next position, velocity, and force values, and use those
        as the starting position, velocity, and force for the next time
        step.
        """
        self.pos[:] = self.npos[:]
        self.vel[:] = self.nvel[:]
        self.f[:] = self.nf[:]

    def time_step_start(self, dt):
        """
        Calculate the first approximation of the next position, velocity,
        and force.
        """
        raise NotImplementedError()

    def time_step_refine(self):
        """
        Calculate the next approximation of the next position and velocity.
        """
        raise NotImplementedError()

    ################
    # Utility methods

    @classmethod
    def _make_vector(cls, x):
        # Copy vector, convert scalar to vector if necessary
        x = np.array(x)
        if not x.shape:
            x.shape = (1,)
        return x


class FrictionlessMass(ObjectWithMass):
    """
    Calculate motion of a mass through a vector force field, without
    friction.
    """

    def __init__(self, m, pos, vel):
        super(FrictionlessMass, self).__init__(m, pos, vel)
        # Polynomial equations for position calculation
        self.pos_polys = [None] * self.dimensions

    def time_step_start(self, dt):
        """
        Calculate the first approximation of the next position, velocity,
        and force.
        """
        self.dt = dt
        # Calculate first approximation assuming field force is constant
        force_polys = self._make_force_polys_initial()
        self._calc_next(force_polys, dt)

    def time_step_refine(self):
        """
        Calculate the next approximation of the next position and velocity.
        Use a function of force varying linearly over time, from the
        starting force to the corrected ending force from the previous
        iteration.
        """
        force_polys = self._make_force_polys_linear(self.dt)
        self._calc_next(force_polys, self.dt)

    ####################
    # Internal methods

    def _make_force_polys_initial(self):
        # Calculate a list of force polynomial functions, one per dimension.
        # The force field component is estimated to be the initial force at
        # the start of the time step. The constant force component is added
        # to this.
        dimensions = self.dimensions
        f = self.f
        f_const = self.f_const
        force_polys = [None] * dimensions
        for i in range(dimensions):
            force_polys[i] = P(f[i] + f_const[i])
        return force_polys

    def _make_force_polys_ave(self):
        # Calculate a list of force polynomial functions, one per dimension.
        # The force field component is estimated by averaging, i.e.:
        #   (starting force + corrected ending force) / 2
        # The constant component is added to this.
        dimensions = self.dimensions
        f = self.f
        nf = self.nf
        f_const = self.f_const
        force_polys = [None] * dimensions
        for i in range(dimensions):
            force_polys[i] = P(((f[i] + nf[i]) / 2.0) + f_const[i])
        return force_polys

    def _make_force_polys_linear(self, dt):
        # Calculate a list of force polynomial functions, one per dimension.
        # The force field component is estimated by varying the force
        # linearly over time from the starting force to the corrected ending
        # force from the previous iteration. The constant component is
        # added to this.
        dimensions = self.dimensions
        f = self.f
        nf = self.nf
        f_const = self.f_const
        force_polys = [None] * dimensions
        for i in range(dimensions):
            force_polys[i] = P((f[i] + f_const[i], (nf[i] - f[i]) / dt))
        return force_polys

    def _calc_next(self, force_polys, dt):
        # Calculate next pos, vel, and f given a force function over time.
        # force_polys:
        #   A sequence of polynomials, one per dimension, determining the
        #   force due to the field over time.
        # dt: Delta time for this time step, in seconds.
        dimensions = self.dimensions
        m = self.m
        vel = self.vel
        nvel = self.nvel
        pos = self.pos
        npos = self.npos
        nf = self.nf
        pos_polys = self.pos_polys
        for i in range(dimensions):
            # Integrate force to get velocity
            vel_poly = (force_polys[i] / m).integ(k=vel[i])
            nvel[i] = vel_poly(dt)
            # Integrate velocity to get position
            pos_polys[i] = vel_poly.integ(k=pos[i])
            npos[i] = pos_polys[i](dt)
            # Calculate next force, given force function and delta time
            nf[i] = force_polys[i](dt)


class MassWithFriction(ObjectWithMass):

    def __init__(self, m, pos, vel, resistance):
        super(MassWithFriction, self).__init__(m, pos, vel)
        self.resistance = resistance
        # Delta time per time step
        self.dt = None
        # Coefficients for calculating next pos
        self.pc1 = None  # Multiply * a ((self.nf - self.f) / dt)
        self.pc2 = None  # Multiply * f0 (self.f)
        self.pc3 = None  # Multiply * v0 (self.vel)
        # ... don't forget to add x0 (self.pos)
        # Coefficients for calculating next vel
        self.vc1 = None  # Multiply * a ((self.nf - self.f) / dt)
        self.vc2 = None  # Multiply * f0 (self.f)
        self.vc3 = None  # Multiply * v0 (self.vel)

    def time_step_start(self, dt):
        """
        Calculate the first approximation of the next position, velocity,
        and force.
        """
        if self.dt != dt:
            self._calc_coefficients(dt)
        self._calc_next_pos_vel()

    def time_step_refine(self):
        """
        Calculate the next approximation of the next position and velocity.
        """
        self._calc_next_pos_vel()

    def _calc_coefficients(self, dt):
        t = dt
        m = self.m
        R = self.resistance
        R2 = R * R
        e = math.exp(-(R * t) / m)
        one_minus_e = 1.0 - e
        self.pc1 = (t*t)/(2*R) - (m*t)/R2 + (m*m*one_minus_e)/(R2*R)
        self.pc2 = (t/R) - (m * one_minus_e)/R2
        self.pc3 = (m * one_minus_e) / R
        self.vc1 = (t/R) - (m * one_minus_e)/R2
        self.vc2 = one_minus_e / R
        self.vc3 = e
        self.dt = dt

    def _calc_next_pos_vel(self):
        # Calculate the next position and velocity.
        A = (self.nf - self.f) / self.dt
        f = self.f + self.f_const
        vel = self.vel
        self.npos = (self.pos +
            (A * self.pc1) +
            (f * self.pc2) +
            (vel * self.pc3))
        self.nvel = (A * self.vc1) + (f * self.vc2) + (vel * self.vc3)


def test_simple_harmonic_motion():
    """
    Test FrictionlessMass by comparing its results to known exact solution
    of system with simple harmonic motion, single mass on a spring.
    """
    print()
    print("test_simple_harmonic_motion")
    # https://ccrma.stanford.edu/CCRMA/Courses/150/vibrating_systems.html
    # Equations of harmonic motion of mass attached to spring:
    # m = mass in kg
    # x = displacement of mass from equilibrium (meters)
    # k = spring constant (N / m)
    # F = force on mass = -k * x
    # w = angular frequency in radians / second = sqrt(k / m)
    # x(t) = A * cos((w*t) + p)
    # v(t) = -w * A * sin((w*t) + p)
    k = 1.0  # spring constant, N / m
    m = 1.0  # mass, kg
    x0 = 1.0  # displacement to the right of equilibrium, meters
    v0 = 0.0  # velocity, m / s
    # Calculate coefficients for exact solution
    w = math.sqrt(k / m)
    A = math.sqrt(((v0 * v0) / (w * w)) + (x0 * x0))
    p = math.acos(x0 / A)
    def _calc_exact_pos(t):
        return A * math.cos((w * t) + p)
    def _calc_exact_vel(t):
        return -w * A * math.sin((w * t) + p)
    def _calc_exact_force(x):
        # This only works if spring is either horizontal or vertical
        return -k * x
    # Set up state variables and thresholds
    dt = 0.01  # Delta time per time step
    # Run solver for a fixed length of simulated time
    total_time = 31.4  # seconds
    # Create simulator
    class SimpleHarmonicMotionSystem(SimulatorBase):
        def __init__(self):
            obj = FrictionlessMass(m, x0, v0)
            super(SimpleHarmonicMotionSystem, self).__init__(
                    object_list=[obj],
                    dt=dt,
            )
        def update_forces(self, pos_getter_func):
            for obj in self.object_list:
                obj.accum_force += _calc_exact_force(pos_getter_func(obj))
    sim = SimpleHarmonicMotionSystem()
    # Function to report results for one time step
    print_records = False
    t_values = []
    real_x_values = []
    sim_x_values = []
    err_x_values = []
    def _output_record():
        real_x = _calc_exact_pos(sim.t)
        real_v = _calc_exact_vel(sim.t)
        real_e = (0.5 * m * real_v * real_v) + (0.5 * k * real_x * real_x)
        obj = sim.object_list[0]
        calc_x = obj.x
        calc_v = obj.vx
        calc_e = (0.5 * m * calc_v * calc_v) + (0.5 * k * calc_x * calc_x)
        record = (
            sim.t,
            dt,
            real_x,
            real_v,
            real_e,
            calc_x,
            calc_v,
            calc_e,
            sim.max_err,
        )
        if print_records:
            parts = []
            for item in record:
                parts.append("{:8.5f}".format(item))
            print(*parts)
        t_values.append(sim.t)
        real_x_values.append(real_x)
        sim_x_values.append(obj.x)
        err_x_values.append(obj.x - real_x)
    _output_record()
    # Run the simulator for a period of time
    for i in range(int(total_time / dt)):
        sim.time_step()
        _output_record()
    # Graph the results
    import matplotlib
    #matplotlib.use('TkAgg')  # This ended up being the default
    import matplotlib.pyplot as plt
    fig = plt.figure()  # Create a "figure window"
    ###
    nrows, ncols = 2, 1
    ax1 = fig.add_subplot(nrows, ncols, 1)
    ax1.set_title("Simple Harmonic Motion")
    ax1.set_xlabel('Time')
    ax1.set_ylabel('Position')
    ax1.plot(t_values, real_x_values)
    ax1.plot(t_values, sim_x_values)
    ###
    ax2 = fig.add_subplot(nrows, ncols, 2)
    ax2.set_title("Error between simulated and calculated")
    ax2.set_xlabel('Time')
    ax2.set_ylabel('Error in position')
    ax2.plot(t_values, err_x_values)
    ###
    fig.tight_layout()
    plot_filename = None
    if plot_filename:
        fig.savefig(plot_filename)
    show_interactive = True
    if show_interactive:
        plt.ion()  # Turn on interactive manipulation
        fig.show()
        if matplotlib.get_backend().lower() == 'tkagg':
            # Work around bug in tkagg backend that causes it not to wait
            manager = fig.canvas.manager
            window = manager.window  # a tkinter.Tk object
            window.mainloop()
    plt.close(fig)
    # Check ending values
    ending_exact_vel = _calc_exact_vel(sim.t)
    ending_sim_vel = sim.object_list[0].vel
    vel_err = np.linalg.norm(ending_sim_vel - ending_exact_vel)
    vel_err_percent = (vel_err * 100.0) / np.linalg.norm(ending_exact_vel)
    print("Velocity error percent:", vel_err_percent)
    print("Force error:", sim.max_err)


def test_damped_oscillator():
    """
    Test MassWithFriction by comparing its results to known exact solution
    for the motion of a single mass on a spring with damping.
    """
    print()
    print("test_damped_oscillator")
    import cmath
    # Equations of harmonic motion of mass attached to spring:
    m = 1.0  # mass, kg
    k = 100.0  # spring constant, N / m
    R = 2.0  # damping resistance, N / (m/s)
    x0 = 0.0  # initial displacement to the right of equilibrium, meters
    v0 = 8.0  # initial velocity, m / s
    # Calculate coefficients for exact solution
    rad = cmath.sqrt((R*R) - (4*k*m))
    a1 = (-R - rad) / (2*m)
    a2 = (-R + rad) / (2*m)
    C1 = (1 / (2*rad)) * ((-R*x0) - (2*m*v0) + (x0*rad))
    C2 = (-1 / (R - rad)) * (
            (2*m*v0) + (1/(2*rad))*(R + rad)*((-R*x0) - (2*m*v0) + (x0*rad)))
    # Calculate frequency of oscillation
    f = np.linalg.norm(a1) / (2 * cmath.pi)
    print("Frequency:", f)
    # Calculate data samples
    sample_rate = 100  # Hz
    time_duration = 3.14  # seconds
    t_values = np.linspace(0.0, time_duration, int(time_duration*sample_rate)+1)
    x_values = (C1*np.exp(t_values * a1) + C2*np.exp(t_values*a2)).real
    v_values = (C1*a1*np.exp(t_values*a1) + C2*a2*np.exp(t_values*a2)).real
    def _calc_field_force(x):
        # This only works if spring is either horizontal or vertical
        return -k * x
    # Create simulator
    class SimpleHarmonicMotionWithDamping(SimulatorBase):
        def __init__(self):
            obj = MassWithFriction(m, x0, v0, R)
            dt = 1.0 / sample_rate  # Delta time per time step
            super(SimpleHarmonicMotionWithDamping, self).__init__(
                    object_list=[obj],
                    dt=dt,
            )
        def update_forces(self, pos_getter_func):
            for obj in self.object_list:
                obj.accum_force += _calc_field_force(pos_getter_func(obj))
    sim = SimpleHarmonicMotionWithDamping()
    # Run the simulator for a period of time
    sim_x_values = []
    sim_v_values = []
    err_x_values = []
    for i in range(len(t_values)):
        if i > 0:
            sim.time_step()
        obj = sim.object_list[0]
        sim_x_values.append(obj.x)
        sim_v_values.append(obj.vx)
        err_x_values.append(obj.x - x_values[i])
    # Graph the results
    import matplotlib
    #matplotlib.use('TkAgg')  # This ended up being the default
    import matplotlib.pyplot as plt
    fig = plt.figure()  # Create a "figure window"
    ###
    nrows, ncols = 2, 1
    ax1 = fig.add_subplot(nrows, ncols, 1)
    ax1.set_title("Damped Oscillation")
    ax1.set_xlabel('Time')
    ax1.set_ylabel('Position')
    ax1.plot(t_values, x_values)
    ax1.plot(t_values, sim_x_values)
    ###
    ax2 = fig.add_subplot(nrows, ncols, 2)
    ax2.set_title("Error between simulated and calculated")
    ax2.set_xlabel('Time')
    ax2.set_ylabel('Error in position')
    ax2.plot(t_values, err_x_values)
    ###
    fig.tight_layout()
    plot_filename = None
    if plot_filename:
        fig.savefig(plot_filename)
    show_interactive = True
    if show_interactive:
        plt.ion()  # Turn on interactive manipulation
        fig.show()
        if matplotlib.get_backend().lower() == 'tkagg':
            # Work around bug in tkagg backend that causes it not to wait
            manager = fig.canvas.manager
            window = manager.window  # a tkinter.Tk object
            window.mainloop()
    plt.close(fig)
    # Check ending values
    ending_exact_x = x_values[-1]
    ending_exact_v = v_values[-1]
    ending_sim_x = sim.object_list[0].x
    ending_sim_v = sim.object_list[0].vx
    pos_err = ending_sim_x - ending_exact_x
    if ending_exact_x == 0:
        print("Position error (absolute):", pos_err)
    else:
        print("Position percent error:", (pos_err * 100.0) / ending_exact_x)
    vel_err = ending_sim_v - ending_exact_v
    if ending_exact_v == 0:
        print("Velocity error (absolute):", vel_err)
    else:
        print("Velocity error percent:", (vel_err * 100.0) / ending_exact_v)
    print("Force error:", sim.max_err)


def test_two_mass_three_spring_no_friction():
    """
    Test FrictionlessMass by comparing its results to known exact solution
    for the motion of two masses connected to each other by a spring, and
    each one connected on the other side by a spring to a wall.
    """
    print()
    print("test_two_mass_three_spring_no_friction")
    # Set up parameters
    m = 1.0  # kg
    k = 40.0  # N / m
    x10 = 0.0  # m
    x20 = 0.0  # m
    v10 = 1.0  # m/s
    v20 = 0.0  # m/s
    sqrt_10 = math.sqrt(10)
    sqrt_30 = math.sqrt(30)
    C1 = 0.000104166666666667 * sqrt_30 * 1.0j
    C2 = 0.0003125 * sqrt_10 * 1.0j
    C3 = -0.0003125 * sqrt_10 * 1.0j
    C4 = -0.000104166666666667 * sqrt_30 * 1.0j
    time_duration = 30.0  # sec.
    sample_rate = 100.0  # samples / sec.
    # Calculate the "exact" values
    t_values = np.linspace(0.0, time_duration, int(time_duration*sample_rate)+1)
    x1_values = (
        40.0 * C1 * np.exp(-2 * sqrt_30 * 1.0j * t_values)
        + 40.0 * C2 * np.exp(-2 * sqrt_10 * 1.0j * t_values)
        + 40.0 * C3 * np.exp(2 * sqrt_10 * 1.0j * t_values)
        + 40.0 * C4 * np.exp(2 * sqrt_30 * 1.0j * t_values)
    ).real
    x2_values = (
        -40.0 * C1 * np.exp(-2 * sqrt_30 * 1.0j * t_values)
        + 40.0 * C2 * np.exp(-2 * sqrt_10 * 1.0j * t_values)
        + 40.0 * C3 * np.exp(2 * sqrt_10 * 1.0j * t_values)
        -40.0 * C4 * np.exp(2 * sqrt_30 * 1.0j * t_values)
    ).real
    # Create the simulator
    obj1 = FrictionlessMass(m, x10, v10)
    obj2 = FrictionlessMass(m, x20, v20)
    class TwoMassThreeSpringNoFriction(SimulatorBase):
        def __init__(self):
            dt = 1.0 / sample_rate  # Delta time per time step
            super(TwoMassThreeSpringNoFriction, self).__init__(
                    object_list=[obj1, obj2],
                    dt=dt,
            )
        def update_forces(self, pos_getter_func):
            x1 = pos_getter_func(obj1)
            x2 = pos_getter_func(obj2)
            obj1.accum_force += k * (-2*x1 +   x2)
            obj2.accum_force += k * (   x1 - 2*x2)
    sim = TwoMassThreeSpringNoFriction()
    # Run the simulator for a period of time
    sim_x1_values = []
    sim_x2_values = []
    err_x1_values = []
    err_x2_values = []
    for i in range(len(t_values)):
        if i > 0:
            sim.time_step()
        sim_x1_values.append(obj1.x)
        err_x1_values.append(obj1.x - x1_values[i])
        sim_x2_values.append(obj2.x)
        err_x2_values.append(obj2.x - x2_values[i])
    # Graph the results
    import matplotlib
    #matplotlib.use('TkAgg')  # This ended up being the default
    import matplotlib.pyplot as plt
    fig = plt.figure()  # Create a "figure window"
    nrows, ncols = 2, 2
    ###
    ax11 = fig.add_subplot(nrows, ncols, 1)
    ax11.set_title("Mass 1 position")
    ax11.set_xlabel('Time')
    ax11.set_ylabel('Position')
    ax11.plot(t_values, x1_values)
    ax11.plot(t_values, sim_x1_values)
    ###
    ax12 = fig.add_subplot(nrows, ncols, 2)
    ax12.set_title("Mass 1 position err.")
    ax12.set_xlabel('Time')
    ax12.set_ylabel('Position err.')
    ax12.plot(t_values, err_x1_values)
    ###
    ax21 = fig.add_subplot(nrows, ncols, 3)
    ax21.set_title("Mass 2 position")
    ax21.set_xlabel('Time')
    ax21.set_ylabel('Position')
    ax21.plot(t_values, x2_values)
    ax21.plot(t_values, sim_x2_values)
    ###
    ax22 = fig.add_subplot(nrows, ncols, 4)
    ax22.set_title("Mass 2 position err.")
    ax22.set_xlabel('Time')
    ax22.set_ylabel('Position err.')
    ax22.plot(t_values, err_x2_values)
    ###
    fig.tight_layout()
    plot_filename = None
    if plot_filename:
        fig.savefig(plot_filename)
    show_interactive = True
    if show_interactive:
        plt.ion()  # Turn on interactive manipulation
        fig.show()
        if matplotlib.get_backend().lower() == 'tkagg':
            # Work around bug in tkagg backend that causes it not to wait
            manager = fig.canvas.manager
            window = manager.window  # a tkinter.Tk object
            window.mainloop()
    plt.close(fig)


if __name__ == '__main__':
    test_simple_harmonic_motion()
    test_damped_oscillator()
    test_two_mass_three_spring_no_friction()
