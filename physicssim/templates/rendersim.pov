// Spring-Mass animation experiments
// David H 16 Apr 2016
// To render this file:
// povray +Ospringmass.png +W640 +H480 +P springmass.pov
// +O = Set output file
// +W = Output width in pixels
// +H = Output height in pixels
// +P = Pause when done (so output window doesn't disappear before you can
//      see it)
#version 3.5;

//global_settings { assumed_gamma 2.2 }

#include "colors.inc"
#include "textures.inc"

#declare view_distance = {{ width }};
#declare grid_scale = 0.1;
#declare obj_z = 0.0;

camera {
    location    <0, 0, -view_distance>
    up          y
    right       x * (image_width / image_height)
    look_at     <0, 0, 0>
    angle       60.0
}

light_source {<0, view_distance, -view_distance> color White}

background { color SkyBlue }

{% for spring in springs %}
cylinder {
    <{{ spring.x1 }}, {{ spring.y1 }}, obj_z>,
    <{{ spring.x2 }}, {{ spring.y2 }}, obj_z>,
    {{ band_radius }}
    pigment {Red}
}
{% endfor %}

{% for mass in masses %}

{% if mass.type == 'block' %}
box {
    <{{ mass.x - mass.r }},
     {{ mass.y - mass.r }},
     obj_z - {{  mass.r }}>,
    <{{ mass.x + mass.r }},
     {{ mass.y + mass.r }},
     obj_z + {{ mass.r }}>
    pigment {Gray}
    texture { Silver_Metal }
    finish {
        ambient 0.2
        diffuse 0.6
        phong 1.0
        phong_size 20
    }
}
{% elif mass.type == 'driver' %}
merge {
    prism {
        conic_sweep
        linear_spline
        0, // sweep the following shape from here ...
        1, // ... up through here
        5, // the number of points making up the shape ...
        <1,1>, <-1,1>, <-1,-1>, <1,-1>, <1,1>
        rotate <0, 45, 0>
        translate <0, -1, 0>
        scale <{{ mass.r }}, {{ mass.r }}*1.414, {{ mass.r }}>
        pigment { Gray }
        texture { Silver_Metal }
    }
    prism {
        conic_sweep
        linear_spline
        0, // sweep the following shape from here ...
        1, // ... up through here
        5, // the number of points making up the shape ...
        <1,1>, <-1,1>, <-1,-1>, <1,-1>, <1,1>
        rotate <180, 45, 0>
        translate <0, 1, 0>
        scale <{{ mass.r }}, {{ mass.r }}*1.414, {{ mass.r }}>
        pigment { Gray }
        texture { Silver_Metal }
    }
    translate <{{ mass.x }}, {{ mass.y }}, obj_z>
}
{% else %}
sphere {
    <{{ mass.x }}, {{ mass.y }}, obj_z>, {{ mass.r }}
    pigment {Gray}
    texture { Silver_Metal }
    finish {
        ambient 0.2
        diffuse 0.6
        phong 1.0
        phong_size 20
    }
}
{% endif %}

{% endfor %}

plane { z, 0
    pigment {
        checker color Gray color White
        scale grid_scale
    }
    finish {
        ambient 0.2
        diffuse 0.8
    }
}
