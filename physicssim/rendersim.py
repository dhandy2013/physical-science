"""
Render 3D animation based on simulator output.

Usage:
    rendersim.py [options] [<simulation-data-file>]

Options:
    --output-dir=DIRPATH    Directory to which output files will be written.
                            [default: .]
    --width=WIDTH           Width of output window, in pixels [default: 640]
    --height=HEIGHT         Height of output window, in pixels [default: 360]
    --frame-rate=RATE       Frame rate of output movie file. [default: 30]
    --render-only           Skip generating POV files from data, just render
                            .pov files that already exist in output directory.
    --sample-data           Use internally generated sample data instead of
                            data file.
    --pad-seconds-at-end=SEC
                            Number of seconds of padding to add at end of
                            movie file. Pad by duplicating last frame.
                            [default: 0.0]
"""
import glob
import math
import os
import pickle
import shutil
import subprocess
import sys

import docopt
import jinja2

from .simcommon import (
    FrameRecord,
    MassRecord,
    SimHeader,
    SpringRecord,
)

this_module = sys.modules[__name__]


def generate_sample_data():
    """
    Generate sample data in the same format as comes from a simulator.
    """
    total_horiz_distance = 1.0
    num_beads = 15
    d0 = total_horiz_distance / (num_beads + 1)
    r0 = d0 / 4.0
    x0 = -(total_horiz_distance / 2.0)
    y0 = 0.0
    z0 = 0.0
    amp = (total_horiz_distance / 2.0) * 0.7
    width = total_horiz_distance * 1.2
    height = r0 * 6
    yield SimHeader(width=width, height= height)
    bands = []
    objects = []
    for i in range(num_beads + 2):
        x = x0 + (i * d0)
        t = float(i) / float(num_beads + 2 - 1)
        y = y0 + (amp * math.sin(t * 2.0 * math.pi))
        z = z0
        if i == 0:
            obj_type = 'driver'
        elif i == num_beads + 2 - 1:
            obj_type = 'block'
        else:
            obj_type = 'mass'
        objects.append(MassRecord(obj_type, r0, x, y))
        if i > 0:
            obj1 = objects[-2]
            obj2 = objects[-1]
            bands.append(SpringRecord(d0, obj1.x, obj1.y, obj2.x, obj2.y))
    yield FrameRecord(
        t=0.0,
        masses=objects,
        springs=bands,
    )


def read_simulation_data(filename):
    with open(filename, 'rb') as f:
        while True:
            try:
                yield pickle.load(f)
            except EOFError:
                break


class FrameRenderer:

    frame_digits = 6
    frame_prefix = 'frame-'

    pov_template_filename = 'rendersim.pov'
    povray_ini_template_filename = 'povray.ini'
    povray_ini_filename = 'povray.ini'
    povray_log_filename = 'povray.log'

    avconv_log_filename = 'avconv.log'
    movie_filename = 'movie.mp4'

    def __init__(self, data_source,
            output_dir=os.curdir,
            pixel_width=640, pixel_height=360,
            frame_rate=30,
            pad_seconds_at_end=0.0,
            ):
        """
        data_source:
            A generater returned by one of these functions:
                read_simulation_data(filename)
                generate_sample_data()
        output_dir:
            Directory into which all output files will be written.
        pixel_width, pixel_height:
            Dimensions of each generated graphics frame, in pixels.
        frame_rate:
            The frame rate of the movie file that will be produced.
        """
        self.data_source = data_source
        self.output_dir = output_dir
        self.pixel_width = pixel_width
        self.pixel_height = pixel_height
        self.frame_rate = frame_rate
        self.pad_seconds_at_end = pad_seconds_at_end
        #####
        self.frame_num = 0
        self.width = None
        self.height = None
        self.env = jinja2.Environment(
            loader=jinja2.PackageLoader(this_module.__package__),
            undefined=jinja2.StrictUndefined)
        self.pov_template = self.env.get_template(self.pov_template_filename)
        self.pov_ini_template = self.env.get_template(
                self.povray_ini_template_filename)
        self.povray = shutil.which('povray')
        self.avconv = shutil.which('avconv')

    def run(self):
        if not os.path.isdir(self.output_dir):
            os.makedirs(self.output_dir)
        else:
            if self.data_source is not None:
                print("WARNING: Output directory '{0}' already exists. "
                      .format(self.output_dir))
        if self.data_source is not None:
            self._render_data_to_povs()
        else:
            print("No data source, using existing .pov files in",
                    self.output_dir)
        if self.povray:
            self._render_povs_to_pngs()
        else:
            print("WARNING: povray program not found, no .png files produced.")
        if self.avconv:
            self._render_pngs_to_movie()
        else:
            print("WARNING: avconv program not found, no movie file produced.")

    def _render_data_to_povs(self):
        ctx = {
            'width': self.pixel_width,
            'height': self.pixel_height,
        }
        povray_ini_filepath = self._get_povray_ini_filepath()
        with open(povray_ini_filepath, 'w') as f:
            print(self.pov_ini_template.render(ctx), file=f)
        povray_log_filepath = self._get_povray_log_filepath()
        with open(povray_log_filepath, 'w') as f:
            pass
        print("Rendering data to POV files at:", self.output_dir)
        for msg in self.data_source:
            if isinstance(msg, SimHeader):
                self.width, self.height = msg.width, msg.height
            elif isinstance(msg, FrameRecord):
                self._render_frame_to_pov(msg)
                self.frame_num += 1
                print("\rFinished writing frame #", self.frame_num, end='')
        print()

    def _get_povray_ini_filepath(self):
        return os.path.join(self.output_dir, self.povray_ini_filename)

    def _get_povray_log_filepath(self):
        return os.path.join(self.output_dir, self.povray_log_filename)

    def _render_frame_to_pov(self, frame):
        # Create POV file from frame data.
        if None in (self.width, self.height):
            raise RuntimeError("Frame arrived before header.")
        # TODO: Instead of guessing band radius, supply it with each spring.
        r0 = None
        for mass in frame.masses:
            if r0 is None:
                r0 = mass.r
            else:
                r0 = min(r0, mass.r)
        if r0 is None:
            r0 = 0.1
        band_radius = r0 * 0.5
        ctx = {
            't': frame.t,
            'masses': frame.masses,
            'springs': frame.springs,
            'width': self.width,
            'height': self.height,
            'band_radius': band_radius,
        }
        pov_filename = self._create_frame_filename('.pov')
        with open(pov_filename, 'w') as f:
            for part in self.pov_template.generate(ctx):
                f.write(part)

    def _create_frame_filename(self, ext, frame_num=None):
        if frame_num is None:
            frame_num = self.frame_num
        filename = '{prefix}{frame_num:0{width}d}{ext}'.format(
            prefix=self.frame_prefix,
            frame_num=frame_num,
            width=self.frame_digits,
            ext=ext,
        )
        return os.path.join(self.output_dir, filename)

    def _render_povs_to_pngs(self):
        povray_log_filepath = self._get_povray_log_filepath()
        print("Rendering POV files to PNG, log at:", povray_log_filepath)
        pov_filenames = self._get_frame_filenames('.pov')
        for i, pov_filename in enumerate(pov_filenames):
            print("\rRendering frame # {current} of {total} ...".format(
                current=i + 1,
                total=len(pov_filenames),
            ), end='')
            self._render_pov_to_png(pov_filename)
        print()
        if self.pad_seconds_at_end > 0:
            print("Adding {0} seconds of pad frames.".format(
                self.pad_seconds_at_end))
            png_filenames = self._get_frame_filenames('.png')
            if not png_filenames:
                raise Exception("No PNG files. Need at least one frame to pad.")
            last_filename = png_filenames[-1]
            frame_num = self._get_frame_num_from_filename(last_filename, '.png')
            num_pad_frames = int(self.pad_seconds_at_end * self.frame_rate)
            for i in range(frame_num + 1, frame_num + num_pad_frames + 1):
                filename = self._create_frame_filename('.png', frame_num=i)
                shutil.copy(last_filename, filename)

    def _get_frame_filenames(self, ext):
        file_pattern = '{prefix}{digits}{ext}'.format(
            prefix=self.frame_prefix,
            digits=('?' * self.frame_digits),
            ext=ext,
        )
        path_pattern = os.path.join(self.output_dir, file_pattern)
        return sorted(glob.glob(path_pattern))

    def _get_frame_num_from_filename(self, filename, ext):
        filename = os.path.basename(filename)
        frame_num_str = filename[len(self.frame_prefix):-len(ext)]
        return int(frame_num_str)

    def _render_pov_to_png(self, pov_filename):
        povray_ini_filepath = self._get_povray_ini_filepath()
        povray_log_filepath = self._get_povray_log_filepath()
        png_filename = os.path.splitext(pov_filename)[0] + '.png'
        cmd = [
            self.povray,
            povray_ini_filepath,
            '+O{}'.format(png_filename),
            # Override height and width in povray.ini file, just in case it
            # changed since the .pov files were created.
            '+W{}'.format(self.pixel_width),
            '+H{}'.format(self.pixel_height),
            pov_filename
        ]
        with open(povray_log_filepath, 'a') as f:
            print(subprocess.list2cmdline(cmd), file=f)
            f.flush()
            subprocess.check_call(cmd, stderr=f)

    def _render_pngs_to_movie(self):
        avconv_log_filepath = os.path.join(self.output_dir,
                self.avconv_log_filename)
        movie_filepath = os.path.join(self.output_dir, self.movie_filename)
        print("Creating movie file '{0}', log at '{1}'".format(
            movie_filepath, avconv_log_filepath))
        # Example: avconv -i frame-%06d.png -r 30 -y movie.mp4
        frame_pattern = '{prefix}%0{width}d.png'.format(
                prefix=self.frame_prefix,
                width=self.frame_digits)
        cmd = [
            self.avconv,
            '-i', os.path.join(self.output_dir, frame_pattern),
            '-r', str(self.frame_rate),
            '-y',  # overwrite output files without asking
            movie_filepath,
        ]
        with open(avconv_log_filepath, 'w') as f:
            print(subprocess.list2cmdline(cmd), file=f)
            f.flush()
            subprocess.check_call(cmd, stderr=f)


def main():
    args = docopt.docopt(__doc__)
    simulation_data_file = args['<simulation-data-file>']
    if args['--render-only']:
        data_source = None
    elif args['--sample-data']:
        data_source = generate_sample_data()
    elif simulation_data_file:
        data_source = read_simulation_data(simulation_data_file)
    else:
        print(__doc__)
        return 1
    renderer = FrameRenderer(data_source,
        output_dir=args['--output-dir'],
        pixel_width=int(args['--width']),
        pixel_height=int(args['--height']),
        frame_rate=int(args['--frame-rate']),
        pad_seconds_at_end=float(args['--pad-seconds-at-end']),
    )
    renderer.run()


if __name__ == '__main__':
    sys.exit(main())
