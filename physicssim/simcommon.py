"""
Common simulation code.
"""
from collections import namedtuple

SimHeader = namedtuple('SimHeader', ('width', 'height'))
FrameRecord = namedtuple('FrameRecord', ('t', 'masses', 'springs'))
MassRecord = namedtuple('MassRecord', ('type', 'r', 'x', 'y'))
SpringRecord = namedtuple('SpringRecord', ('d', 'x1', 'y1', 'x2', 'y2'))
