#!/usr/bin/env python3
"""
Orbit Simulator
"""
# Data sheets and formulas used:
# http://nssdc.gsfc.nasa.gov/planetary/factsheet/
# http://nssdc.gsfc.nasa.gov/planetary/factsheet/cometfact.html
# https://en.wikipedia.org/wiki/Orbital_speed

from datetime import timedelta
import math
import random
import sys
import time

import numpy as np

from cpif.graphics import DrawingWindow

from .forcefield import FrictionlessMass, SimulatorBase

# The Gravitational Constant
G = 6.67408e-11 # m^3 / kg * s^2

# The speed of light
c = 299792458 # m / s

# Masses of astronomical objects
m_sun = 1.989e30 # kg
m_earth = 5.972e24 # kg
m_moon = 7.34767309e22 # kg
m_comet_encke = 1e12 # kg - wild guess

# Sizes of astronomical objects
r_sun = 6.957e8 # m
r_earth = 6371e3 # m
r_moon = 1737.1e3 # m
r_comet_encke = 5000 # m - wild guess

# Astronomical distances
d_earth_moon = 384400e3 # m
d_sun_earth = au = 149597870700 # m
d_sun__mars = 227.9e9 # m
d_sun_jupiter = 778.6e9 # m
d_sun_saturn = 1433.5e9 # m
d_sun_comet_encke_perihelion = 0.340 * au # m

# Orbital velocities
v_earth = 29.8e3 # m / s
v_moon = 1.0e3 # m / s
v_mars = 24.1e3 # m / s
v_jupiter = 13.1e3 # m / s
v_saturn = 9.7e3 # m / s
v_comet_encke_perihelion = 69502 # m / s


def circular_orbit_speed(m_center, r):
    """
    Calculate the speed of an object in circular orbit around a much larger
    mass.

    m_center:
        The mass of the central object around which the object of interest
        is orbiting, in kilograms.
    r:
        The radius of the orbit, in meters.

    Return the speed in meters per second.
    """
    return math.sqrt((G * m_center) / r)


def two_body_params(m1, m2, r):
    """
    Two bodies in a circular orbit around a common center of mass.

    m1, m2: The masses of the two bodies.
    r: The distance from the center of m1 to the center of m2

    Return r1, r2, v1, v2

    r1, r2:
        The distances of m1 and m2 from the common center, respectively.
        The sign of r1 is negative to indicate that m1 is on the opposite
        side of the center of mass from m2.
    v1, v2:
        The velocities of m1 and m2, respectively.
        The sign of v1 is negative to indicate that m1 is going in a
        direction opposite to the velocity v1.
    """
    m = m1 + m2
    r1 = (r * m2) / m
    r2 = (r * m1) / m
    r_squared = r * r
    v1 = math.sqrt((G * m2 * r1) / r_squared)
    v2 = math.sqrt((G * m1 * r2) / r_squared)
    return -r1, r2, -v1, v2


class OrbitingObject(FrictionlessMass):

    def __init__(self, name, mass, radius, pos, vel, g_m=None):
        super(OrbitingObject, self).__init__(mass, pos, vel)
        self.name = name
        if g_m is None:
            g_m = G * mass
        self.g_m = g_m
        self.r = radius


class SimulatorFactory:
    """
    Each method in this class with a name beginning with "sim_" is a
    Simulator factory method.

    Call list_scenarios() to get a list of valid scenario names.

    Call get_scenario() with the name of a scenario to get a Simulator
    object that is ready to run.
    """

    def __init__(self, pixel_width, pixel_height):
        self.pixel_width = pixel_width
        self.pixel_height = pixel_height

    def list_scenarios(self):
        return sorted(name[4:] for name in dir(self)
                if name.startswith('sim_'))

    def get_scenario(self, name):
        simulator_factory = getattr(self, 'sim_' + name, None)
        if simulator_factory is None:
            return None
        return simulator_factory()

    def sim_sun_earth(self):
        object_list = [
            OrbitingObject('sun', m_sun, r_sun,
                pos=(0.0, 0.0),
                vel=(0.0, 0.0),
            ),
            OrbitingObject('earth', m_earth, r_earth, 
                pos=(d_sun_earth, 0.0),
                vel=(0.0, v_earth)
            ),
        ]
        frame_rate=20
        seconds_per_year = 365.25 * 24 * 3600
        wall_time_per_revolution = 60 # seconds
        delta_time = seconds_per_year / (wall_time_per_revolution * frame_rate)
        return Simulator(self.pixel_width, self.pixel_height,
            meter_width=au*2.25,
            meter_height=au*2.25,
            object_list=object_list,
            dt=delta_time,
            frame_rate=frame_rate,
        )

    def sim_sun_earth_comet(self):
        object_list = [
            OrbitingObject('sun', m_sun, r_sun,
                pos=(0.0, 0.0),
                vel=(0.0, 0.0),
            ),
            OrbitingObject('earth', m_earth, r_earth,
                pos=(d_sun_earth, 0.0),
                vel=(0.0, v_earth),
            ),
            OrbitingObject('comet-encke', m_comet_encke, r_comet_encke,
                pos=(-d_sun_comet_encke_perihelion, 0.0),
                vel=(0.0, -v_comet_encke_perihelion),
            ),
        ]
        frame_rate=20
        seconds_per_year = 365.25 * 24 * 3600
        wall_time_per_revolution = 60 # seconds
        delta_time = seconds_per_year / (wall_time_per_revolution * frame_rate)
        return Simulator(self.pixel_width, self.pixel_height,
            meter_width=au*7.5,
            meter_height=au*7.5,
            object_list=object_list,
            dt=delta_time,
            frame_rate=frame_rate,
        )

    def sim_earth_moon(self):
        x1, x2, v1, v2 = two_body_params(m_earth, m_moon, d_earth_moon)
        object_list = [
            OrbitingObject('earth', m_earth, r_earth,
                pos=(x1, 0.0),
                vel=(0.0, v1),
            ),
            OrbitingObject('moon', m_moon, r_moon,
                pos=(x2, 0.0),
                vel=(0.0, v2),
            ),
        ]
        frame_rate = 60
        seconds_per_lunar_month = 28 * 24 * 3600
        wall_time_per_revolution = 60 # seconds
        #delta_time = (seconds_per_lunar_month /
        #        (wall_time_per_revolution * frame_rate * 2))
        delta_time = 600  # seconds
        return Simulator(self.pixel_width, self.pixel_height,
            meter_width=d_earth_moon*2.25,
            meter_height=d_earth_moon*2.25,
            object_list=object_list,
            dt=delta_time,
            frame_rate=frame_rate,
            trace_paths=True,
        )

    def sim_star_cluster(self):
        seconds_per_year = 365.25 * 24 * 3600
        light_year = c * seconds_per_year
        object_list = []
        for i in range(400):
            name = 'star{0:03d}'.format(i)
            x = random.random() * random.choice([-1, 1]) * light_year * 5
            y = random.random() * random.choice([-1, 1]) * light_year * 5
            object_list.append(OrbitingObject(name, m_sun, r_sun,
                pos=(x, y),
                vel=(0.0, 0.0),
            ))
        frame_rate=10
        delta_time = seconds_per_year * 100
        return Simulator(self.pixel_width, self.pixel_height,
            meter_width=light_year*10,
            meter_height=light_year*10,
            object_list=object_list,
            dt=delta_time,
            frame_rate=frame_rate,
            trace_paths=True,
            show_names=False,
        )


class Simulator(SimulatorBase):

    def __init__(self, pixel_width, pixel_height, meter_width, meter_height,
                 object_list, dt, frame_rate=30, trace_paths=False,
                 show_names=True):
        """
        pixel_width, pixel_height: The dimensions of the simulator window
        meter_width, meter_height: The dimensions of real space shown
        object_list: List of OrbitingObjects instances to simulate
        dt: delta time, the amount of time in seconds per simulation step
        frame_rate: The number of times per second the simulation updates
        trace_paths: If true, trace paths of objects (default False)
        show_names: If true, show names of objects (default True)
        """
        super(Simulator, self).__init__(object_list, dt)
        self.pixel_width = pixel_width
        self.pixel_height = pixel_height
        self.meter_width = meter_width
        self.meter_height = meter_height
        self.frame_rate = frame_rate
        self.trace_paths = trace_paths
        self.show_names = show_names
        #####
        self._dw = None
        self._time_drawing_id = None
        self._objs_drawing_id = None
        self._name_drawing_ids = []
        self._num_frames = 0
        self._start_time = None
        self._rescale()

    def _rescale(self):
        pixel_aspect_ratio = self.pixel_width / self.pixel_height
        meter_aspect_ratio = self.meter_width / self.meter_height
        if pixel_aspect_ratio >= meter_aspect_ratio:
            # Height controls scale
            self.pixels_per_meter = self.pixel_height / self.meter_height
            self.meters_per_pixel = self.meter_height / self.pixel_height
        else:
            # Width controls scale
            self.pixels_per_meter = self.pixel_width / self.meter_width
            self.meters_per_pixel = self.meter_width / self.pixel_width

    def _calc_time_str(self):
        return ("Time: {0}\n"
                "Num. frames: {1}\n"
                "Max. force err: {2:.3e}".format(
                timedelta(seconds=self.t),
                self._num_frames,
                self.max_err))

    def _update(self):
        pixels_per_meter = self.pixels_per_meter
        cx = int(self.pixel_width / 2)
        cy = int(self.pixel_height / 2)
        dw = self._dw

        # (Re-)Draw the current time
        if self._time_drawing_id is not None:
            dw.delete(self._time_drawing_id)
        time_str = self._calc_time_str()
        self._time_drawing_id = dw.draw_text(0, 0, time_str)

        if self._objs_drawing_id is not None:
            dw.delete(self._objs_drawing_id)
        for name_id in self._name_drawing_ids:
            dw.delete(name_id)
        del self._name_drawing_ids[:]

        try:
            if self.trace_paths:
                self._objs_drawing_id = None
            else:
                self._objs_drawing_id = dw.begin_drawing()
            # Draw the objects
            for obj in self.object_list:
                pos = obj.pos
                x = cx + int(pos[0] * pixels_per_meter)
                y = cy - int(pos[1] * pixels_per_meter)
                r = int(obj.r * pixels_per_meter)
                dw.circle(x, y, r, 'black', fill='black')
                if self.show_names:
                    name_id = dw.draw_text(x + r, y + r, obj.name, color='blue')
                    if self.trace_paths:
                        self._name_drawing_ids.append(name_id)
        finally:
            if self._objs_drawing_id is not None:
                dw.end_drawing()

        # Do physics calculations
        self.time_step()

        # Schedule the next update
        self._num_frames += 1
        next_frame_time = self._start_time + (self._num_frames /
                self.frame_rate)
        delay_sec = max(0.001, next_frame_time - time.time())
        dw.after_delay_call(delay_sec, self._update)

    def update_forces(self, pos_getter_func):
        object_list = self.object_list
        # Calculate force due to attraction between every object
        for i in range(len(object_list) - 1):
            obj = object_list[i]
            pos = pos_getter_func(obj)
            g_m = obj.g_m
            accum_force = np.zeros(obj.dimensions)
            for j in range(i + 1, len(object_list)):
                other_obj = object_list[j]
                d = pos_getter_func(other_obj) - pos
                r = np.linalg.norm(d)
                if r == 0.0:
                    r = sys.float_info.epsilon
                f = (g_m * other_obj.m) / (r * r)
                accum_force += f * (d / r)
                other_obj.accum_force += -f * (d / r)
            obj.accum_force += accum_force

    def _on_resize(self):
        self.pixel_width, self.pixel_height = self._dw.get_size()
        self._dw.clear()
        self._rescale()
    
    def run(self):
        self._dw = DrawingWindow(self.pixel_width, self.pixel_height)
        self._dw.title("Orbit Simulator")
        self._start_time = time.time()
        for obj in self.object_list:
            pass
        self._dw.on_resize_call(self._on_resize)
        self._dw.after_delay_call(0.0, self._update)
        self._dw.run()


def _input_scenario_name(factory):
    while True:
        print("Simulation scenarios:")
        for scenario_name in factory.list_scenarios():
            print("   ", scenario_name)
        scenario_name = input("Enter scenario name: ").strip().lower()
        if not scenario_name:
            print("Goodbye.")
            return None
        sim = factory.get_scenario(scenario_name)
        if sim is not None:
            return sim
        print("That was not a valid scenario name.")
        print("Enter a name from the list below, or press Enter to quit.")


def main():
    width = 640 # 800
    height = 480 # 600
    scenario_name = ''
    factory = SimulatorFactory(width, height)
    if len(sys.argv) > 1:
        scenario_name = sys.argv[1]
        sim = factory.get_scenario(scenario_name)
        if sim is None:
            print("Invalid scenario name:", scenario_name)
            return 1
    else:
        sim = _input_scenario_name(factory)
    if sim is not None:
        sim.run()


if __name__ == '__main__':
    main()
