{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# This line configures matplotlib to show figures embedded in the notebook, \n",
    "# instead of opening a new window for each figure.\n",
    "%matplotlib inline\n",
    "import matplotlib\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Force Field Module\n",
    "\n",
    "I'm developing a Python Module named ``forcefield`` to calculate the motion of particles in space given complex, changing forces on them.\n",
    "\n",
    "For the purposes of this module, a [force field](https://en.wikipedia.org/wiki/Force_field_%28physics%29) is a system in which there is a function $\\mathbf{F}(\\mathbf{x})$ defining the force on a particle at any vector position $\\mathbf{x}$ at a given point in time. The function can be quite complex and computationally expensive, and may depend on the positions of all other objects in the system at that time. Systems like this include multi-body gravitational systems, systems of connected masses and springs, charged particles, etc.\n",
    "\n",
    "The goal is to calculate the position $\\mathbf{x}(t)$ over time of each particle in the system, minimizing the number of times the function $\\mathbf{F}(\\mathbf{x})$ is evaluated while still preserving acceptable accuracy, so that we can simulate a larger number of particles."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Interpolation of force when moving a mass from one location to another\n",
    "\n",
    "In order to calculate the position $\\mathbf{x}(t)$ of a particle over a short duration of time $\\Delta{t}$, I estimate the force on that particle over time given two samples of the force on that particle:\n",
    "\n",
    "$\\mathbf{F}(t) = \\mathbf{F_0} + \\frac{\\mathbf{x}(t) - \\mathbf{x_0}}{\\mathbf{x_1} - \\mathbf{x_0}}(\\mathbf{F_1} - \\mathbf{F_0})$\n",
    "\n",
    "Where:\n",
    "* $\\mathbf{F_0}$ is the force at the starting point $\\mathbf{x_0}$ at time $t = 0$\n",
    "* $\\mathbf{F_1}$ is the force at the ending point $\\mathbf{x_1}$ at time $t = \\Delta{t}$\n",
    "* $\\mathbf{x}(t)$ is the position at time $0 <= t <= \\Delta{t}$\n",
    "\n",
    "Since $\\mathbf{x}(t)$ is the function we are trying to calculate, we simplify the estimation of force over time like this:\n",
    "\n",
    "$\\mathbf{F}(t) = \\mathbf{F_0} + \\frac{\\mathbf{F_1} - \\mathbf{F_0}}{\\Delta t} t$\n",
    "\n",
    "where $\\Delta t$ is the duration of the time step.\n",
    "\n",
    "You might ask, if $\\mathbf{x_1} = \\mathbf{x}(\\Delta{t})$ is not yet known, how do you know what $\\mathbf{F_1} = \\mathbf{F}(\\mathbf{x_1})$ is? I determine $\\mathbf{F_1}$ by an iterative process. I make an initial estimate by assuming that $\\mathbf{F_0} = \\mathbf{F_1}$ (or in other words, assuming the force remains constant). I calculate an estimated value for $\\mathbf{x_1}$ given that assumption, then plug that estimated $\\mathbf{x_1}$ value into our given force field function $\\mathbf{F}(\\mathbf{x_1})$ to get a more realistic value for $\\mathbf{F_1}$. That process can be repeated to refine the accuracy."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Frictionless motion through the force field\n",
    "\n",
    "$\\mathbf{v}(\\Delta t) = \\mathbf{v_0} + \\frac{1}{m} \\int_{0}^{\\Delta t}{\\mathbf{F}(t) dt}$\n",
    "\n",
    "$\\mathbf{x}(\\Delta t) = \\mathbf{x_0} + \\int_{0}^{\\Delta t}{\\mathbf{v}(t) dt}$\n",
    "\n",
    "where $m$ is the mass of the object of interest, $\\mathbf{v_0}$ is the velocity of the object at time $t = 0$, and all other variables are as defined above.\n",
    "\n",
    "Since $\\mathbf{F}(t)$ is a polynomial, this expression can be integrated for any function of force that I come up with. So instead of simple linear interpolation, I can use the known $\\frac{d\\mathbf{F}(t)}{dt}$ and generate a quadratic equation for $\\mathbf{F}(t)$, for example.\n",
    "\n",
    "I can generate any polynomial and ``numpy`` will integrate it for me. Here's an example of integrating $ax^2 + bx + c$:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "poly([ 3.  2.  1.])\n",
      "poly([ 0.          3.          1.          0.33333333])\n",
      "1.79166666667\n"
     ]
    }
   ],
   "source": [
    "import numpy as np\n",
    "from numpy.polynomial import Polynomial as P\n",
    "a = 1.0\n",
    "b = 2.0\n",
    "c = 3.0\n",
    "poly = P([c, b, a])\n",
    "print(poly)\n",
    "ipoly = poly.integ(k=0.0)\n",
    "print(ipoly)\n",
    "print(ipoly(0.5))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Motion of mass through force field with damping force proportional to velocity\n",
    "\n",
    "$m \\frac{d^2\\mathbf{x}}{dt^2} + R \\frac{d\\mathbf{x}}{dt} = \\mathbf{F}(t)$\n",
    "\n",
    "where $R$ is the resistance and $\\mathbf{F}(t)$ is the force due to the \"field\" at time $t$.\n",
    "\n",
    "I went to [a web site describing Variation of Parameters](http://tutorial.math.lamar.edu/Classes/DE/VariationofParameters.aspx) at [Paul's Online Math Notes](http://tutorial.math.lamar.edu/) (highly recommended) to refresh myself on how to solve this differential equation.\n",
    "\n",
    "First I had to get the equation into the form:\n",
    "\n",
    "$y'' + q(t)y' + r(t)y = g(t)$\n",
    "\n",
    "I did this by dividing both sides by $m$.\n",
    "\n",
    "$\\frac{d^2\\mathbf{x}}{dt^2} + \\frac{R}{m} \\frac{d\\mathbf{x}}{dt} = \\frac{1}{m} \\mathbf{F}(t)$\n",
    "\n",
    "Given the initial approximation of $\\mathbf{F}(t) = \\mathbf{F_0}$, I got these equations for position and velocity:\n",
    "\n",
    "$\n",
    "\\mathbf{x}(t) =\n",
    "    \\mathbf{x_0} + \\frac{\\mathbf{F_0}}{R}t + \\frac{m}{R}(\\mathbf{v_0} - \\frac{\\mathbf{F_0}}{R})(1 - e^{-\\frac{R}{m}t})\n",
    "$\n",
    "\n",
    "$\n",
    "\\mathbf{v}(t) = \\mathbf{v_0}e^{-\\frac{R}{m}t} + \\frac{\\mathbf{F_0}}{R}(1 - e^{-\\frac{R}{m}t})\n",
    "$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAakAAAEaCAYAAACrcqiAAAAABHNCSVQICAgIfAhkiAAAAAlwSFlz\nAAALEgAACxIB0t1+/AAAIABJREFUeJzt3XecVPX1//HXoaiIioCKFEFBERuoKGIFSyJorLHHnqg/\nNdFvmiWJwRZrii0xYowRe6/YC8ESrAgKiBQboFhAEFAE9/z+OHdlGGZ3Z2Fn7p3d9/PxuI+dufPh\nztnZvZz9fO7nno+5OyIiIlnULO0AREREaqIkJSIimaUkJSIimaUkJSIimaUkJSIimaUkJSIimaUk\nJU2Gma1nZnPNzGpp85WZrV++qLLNzM42s6FpxyFNl+k+KckqM3sfWAdYDMwHHgdOdfcFDXT854Cb\n3f3fDXG8SmdmA4Bb3H29tGMRqaaelGSZA3u7+xrA1sA2wB/SDalxMLPmhXYTn7lIZihJSdYZgLt/\nDDwGbA5gZh3N7EEz+8LM3jWzn33/D8y2NbNXzWyOmX1sZn9O9nczsyoza2ZmFwI7A9ckQ4BXJW2q\nzKx78ngNMxtmZp+a2Xtm9vuc9zjGzJ43s8vNbJaZTTGzQQW/AbPuSZxbJs87JcfcpYb2vczsOTOb\nbWZvmdk+yf5+yfdjOW0PMLMxyWMzs7PMbLKZfWZmd5jZmnnf+/Fm9gHwTN57rgo8CnRKhjznmtm6\nZjbEzG7OO8axZvahmX1uZv/PzLYxszHJ53B13nGPN7Pxyff/mJl1rfWnLZJHSUoqgpmtB+wFvJHs\nuhP4EFgXOBi4yMwGJq9dCVzh7m2AHsBdOYdyAHf/A/A88HN3X8PdT8t9PXENsDqwPjAQONrMjst5\nvR8wAWgPXA7cUCh2d58KnAHcYmatgBuBG919ZIHvswXwMDG0uTZwGnCrmW3k7q8A84Ddcv7J4cAt\nyePTgH2J5NsJmA38I+8tdgF6AXvmxbgAGAzMcPfVk8/kkwKfSfX3vWHy3lcAv09i2hw4xMx2Tr6X\n/YCzgP2T7+V54PZCn5FIjdxdm7ZMbsB7wFxgVvL4amBloAuwCFg1p+1FwL+Tx/8FhgDt847XDfgO\naJY8fw44Pq9NFdCd+ANuIbBxzmsnAs8mj48B3s15rVVy7HVq+X4eAMYCbwIta2izE5EocvfdBvwx\neXwBcEPyeHUiaXVJno8Hds35dx2Bb5Pvpfp771ZLfAOAD/P2DQGG5X1+6+a8/jlwUM7ze4DTkseP\nAsflvNaMuLa4Xtq/W9oqZ1NPSrJuP3dv5+4buPsv3H0h0UuY5UtPoPgA6Jw8Ph7YGHjHzF42s72X\n433XAloQvbVC7wFQ3dPA3b8mhiZXq+WY/wI2A65290U1tOkEfJS3L/d9bwMOMLOWwIHA6+4+LXmt\nG3B/Muw2i0hai4AOOceaxor7NOfx1wWeV38G3YArc+L5guiV5X6GIrVSkpKsKzRdfAbQzsxa5+zr\nCkwHcPcp7n6Eu68NXAbckwyz5attksDnxH/w3XL2dat+j/pKYr2CGBI8t/paUQEzgPzZdbnf2wQi\nae1FDLfdltPuQ2BwktTbuXtbd2/tcT2vWm3fc0NPmvgIOCkvntXcfVQDv480YkpSUnGSnsNLwMVm\ntrKZ9QZ+ClRf4P+Jma2VNJ9D/OdblTzPTXoziaG9Qu9RRVzL+pOZrWZm3YBfVr/HcrgKeMXdTySG\nwa6rod3LwAIzO8PMWiTX2X4E3JHT5jbgdOLa0905+68jrs11BTCztc1s35zXa7w/LDETaG9ma9TS\npq5j5Pon8Dsz2zSJp42ZHVSPfy+iJCWZVttf9ocDGxA9j3uBc9z9ueS1QcA4M5sL/A04NBkmzD/m\nlcDBycyzKwq8fhqwAJgKjCTuIbqxvvEmieKHwCnJrl8BW5nZ4cscIIYB9yF6Sp8TkzeOcvd3c5rd\nQUyAeMbdZ+V9Pw8CT5rZHCKR96srvpz3nkhMbJiaDNGtW8T3WONzd38AuAS4w8y+JK7HFZwBKVKT\nkt7Ma2YrEyf3Ssn2oLv/Lq/NAOLEmprsus/dLyxZUCIiUjFalPLg7r7QzHZ19wXJzYMvmtmO7v5i\nXtOR7r5voWOIiEjTVfLhvpwZWCsn7ze7QLP6jHOLiEgTUfIkldzdP5qYrjvC3ccXaLa9mb1pZsOr\nL7KKiIiUrcBsMmPoSeBMd/9vzv7VgKpkSHAwcKW79yzw71VTTESkgrl7vUfNyja7z93nAsOJIqG5\n++dVDwm6+2NASzNrV8MxMr0NGTIk9RgUo2JUjIoxi9vyKmmSMrO1zKxN8rgV8AOiJExumw45j/sR\nvbtZiIhIk1fS2X1E7bCbkqrNzYi1e54xs5MAd/ehwEFmdjJxd//XwKEljklERCpEqaegv0WsA5S/\n/7qcx38H/l7KOMpl4MCBaYdQJ8XYMBRjw1CMDaMSYlxeFbMyr5l5pcQqIiJLMzM8yxMnRERE6ktJ\nSkREMqvUs/tWTtbzGW1m48zsohraXWVmk5IbercsZUwiIlI5Uq/dl9zA28PdNzKz7Yjy/v1LGZdU\nroUL4fPPYfbs2L78EubNg6++iq/ffBNtFi6ERYugqio2d2jWDJo3j22llWDllWGVVWDVVWG11WJb\nYw1Yc01o23bJ1kzjDSKpKfUUdLzu2n37AcOSti8na850cPeZpY5NsmXePHjvvdjefx8++gimT4dp\n0+CTT+DTT2HBAmjffkkCWXPNSCyrrQatW0OrVpF82rSBli0jIVUnmeqEtXgxfPttJLI5c+KY8+fH\n+8+ZsyT5zZoV+9q1g3XWgXXXhY4dY+vSBbp1g65d42vbtql+dCKNVsmTlJk1A14HegD/9GVr93Vm\n6eWypyf7lKQaIXeYMQPefhvGjYMJE+Ddd2ObMwfWXx822CC+rrce9OkDnTtHYlhnnUgGVsZyxIsW\nRc/t008jUX78ccQ/YQI88QR8+GEk1ObNoXv32Hr2jG3jjaFXr0ikIrJ8slC772HgYnd/KXn+NHCG\nu7+R9+81Bb3CuMOUKfDKK/DGGzB6dGwtW8Jmm8Hmm8Mmm8R/5j17QqdOlTm05h69rqlTYfJkmDQJ\nJk6M7Z13oldX/f326QO9e8Omm0aPT6SpWN4p6CXvSVVz97lmVl277785L00H1st53iXZt4xzzz33\n+8cDBw5s1DewVaKvv46E9Pzz8NJL8bhVK+jXD/r2hd/8BrbaKobNGhOzGIJs3x623Xbp16qqorc1\nblz0Hp94Ai67LBJar17xufTtG5/RFlvEtTKRxmDEiBGMGDFihY9T6pV51wIWufucpHbfE8B57v5M\nTpu9gFPdfW8z6w9c4e7LTJxQTyp7Fi6EUaPg2WdjGz06egs77QQ77AD9+0fvSJb19dcwdmz0MF97\nLRL61KnR0+rfH3bcMbbGltCl6VrenlSpk9QWwE3EoobVtfv+nFe7DzO7BhgEzAeOyx/qS9ooSWXA\nlCnw2GPw+OMwcmT0BnbfHXbbLRJT69ZpR1i5vvoqEtb//gcvvhhf27WDXXaJbcCAuF4nUokymaQa\nkpJUOr77Dl5+GR56KLbZs2HQoNh+8IP4T1RKo6oKxo+PPwZGjoQRI2L4dLfd4g+D3XeHDh3qPIxI\nJihJSYP57jt44QW4+26477641rLffrDvvrDNNpU5uaExcI9Zhc8+C888A889F7Mgf/hD2HNP2Hln\nXdOS7FKSkhXiHkNNt90Gd94Zf6EffDAcdFDMvJPsWbw4rmU98URsEyZEL2uvveBHP4pp+yJZoSQl\ny2XaNBg2DG66KXpQP/kJHH54XGuSyvLZZ5Gshg+Prz16wD77wP77x8zBct5fJpJPSUqKtmgRPPww\nXH99XG86+GA49tiYVab/yBqHRYtiyPahh+D+++Nm4wMOgAMPjJ+zhmyl3DKZpMysC1HyqANQBVzv\n7lfltRkAPAhMTXbd5+4XFjiWktQK+vBDGDoU/v1v2HBDOPHE+E9r1VXTjkxKyR3GjIlkdc89Udnj\nxz+GQw6B7bdXwpLyyGqSWhdY193fNLPViPJI+7n7OzltBgC/dvd96ziWktRycI+ba6+6Ki64H3kk\nnHRSVECQpmn8+EhWd90Fc+dGsjrssLipWD1pKZVMJqll3szsAeDqvJt5BwC/cfd96vi3SlL1sGhR\n/Cf05z/HjaO/+AUcfTSsvnrakUmWvP12TJS5/fYYEvzJT+CII6KnLdKQMp+kzGx9YASwubvPy9k/\nALgXmEaUQ/ptgSK0SlJFmjcvrjX97W9x4fw3v4HBgzWkI7Vzj5mCt94aSatHDzjmmOhlqcK7NIRM\nJ6lkqG8EcIG7P1jgtapkzanBwJXuvsykZzPzIUOGfP9ctfuWNns2XHMNXH11VCY488y4p0mkvhYt\nitmBw4bF10GD4PjjYY89orclUoz82n3nnXdeNpOUmbUAHgEec/cri2j/HtDX3Wfl7VdPqoDZs+Gv\nf4V//COmG591lqaPS8OZPTuGAm+8MZYqOfZY+OlP4yZikfpY3p5UOQaB/g2MrylBmVmHnMf9iMQ5\nq1BbWWLOHDjvPNhoo1jf6LXX4D//UYKShtW2LZxyCrz6Kjz6aNQX3HbbqHBxzz2xeKRIKZV6dt+O\nwEjgLcCT7XdAN5ICs2Z2KnAysAj4Gvilu79c4FjqSRHLo//jH3DJJTEM88c/6iK3lNc330S5rKFD\nY82s44+HE05Q70pql+lrUg2hqSepqiq4+eZISltuCX/6UyyLIZKmCRPguuvid3OHHeDUU6OWoCbq\nSD4lqUZsxAj41a9glVXg8stjnSGRLFmwIOo+/v3vMcP01FPhuONiVWIRUJJqlKZMiSnkb74Jl14a\n5Yt0s6VkmXusg3X11TEz8Igj4h69jTdOOzJJW5YnTkg9zZ8Pv/89bLddbBMmxP0qSlCSdWYx7Hf7\n7fDWWzHxYuedoyr7M89EEhOpj5ImKTPrYmbPmtk4M3vLzE6rod1VZjbJzN40sy1LGVOWucO998Im\nm8B770W9tbPOimE+kUrTuTNccAF88EGsR3baaXE9ddgwzQqU4mWhdt9g4OfuvreZbUfczNu/wLEa\n9XDf++/HOP5778G118YNuSKNiXsMAf7lLzE6cNppUUdS162ahkwO97n7J+7+ZvJ4HjAB6JzXbD+i\nUjrJ1PM2ufdONXaLF8Nll0V1iJ12iutPSlDSGJnFbRNPPRVLxYwZA927R3WUjz9OOzrJqrJdk0pq\n920J5N8D1Rn4KOf5dJZNZI3SmDFxzempp6Ju2tlna/lvaRq22irqBL7+eswM3HTTWDpmypS0I5Os\naVGON0mG+u4BTs8tLltf55577vePK7l238KFMVY/dGjM2jv2WE2KkKZp/fVjJuAf/xjLyWy3XVSz\nOPts3QdY6fJr9y2v1Gv3mdk/gefc/c7k+TvAAHefmdeuUVyTGj06qkt37x7Xnjp2TDsikeyYOzfO\ni7/9LRZkPOcc2HrrtKOShpDJa1KJWmv3AQ8BRwOYWX/gy/wE1RgsWgTnnx9/Jf72t7FKqhKUyNLW\nWCOuUU2dCgMHwr77wt57w8vLFEqTpiL12n1Ju2uAQcB84Dh3f6PAsSq2JzV5cqyI26YN3HADdOmS\ndkQileGbb+Df/4aLL4YttoAhQ2JIUCqPKk5kkHsscXDmmTFs8fOfq6aZyPJYuDCS1UUXRbI6/3yt\nl1ZpSpakzGzT/JVyzWygu4+o75utiEpLUl9+GbOV3nknZjFtsUXaEYlUvoULYzTioougb99IVn36\npB2VFKOU16TuMrMzLbQys6uBi+sfYtPxyitxsXftteOxEpRIw1h55VjfavJk2G03GDwYDj00/hiU\nxqmYJLUdsB7wEvAqMANQHe4C3ONu+h/9CP7856gIrZJGIg1vlVXg9NNh0qS452rnnaPq+gcfpB2Z\nNLRiklT1YoStgFWA99y9qpiDm9kNZjbTzMbW8PoAM/vSzN5Itj8UHXnGzJkDBx4Id94ZvacDD0w7\nIpHGr3XrqG85aVLUCtx6a/jlL+Gzz9KOTBpKMUnqVSJJbQvsDBxuZncXefwbgT3raDPS3bdOtguL\nPG6mjBkTF3E7d4bnn9cKpSLltuaacOGFMH48fPddFGk+//xY20oqWzFJ6qfu/kd3X+TuH7v7fsS9\nTXVy9xeA2XU0q+haC7feCnvsAeedB9dcE2PmIpKODh2icsWrr8K770LPnnFz8KJFaUcmy6vOJOXu\nrxXYd3MDxrB9skTHcDPbtAGPW1KLF8dquUOGwLPPxuJuIpING2wAt9wCw4fHjfObbQb33af1rCpR\nWWr31eJ1oKu7L0iW7HgA6FlT46zU7vv885hR1LJlXH9q1y6VMESkDlttBU8+Gdtvfwt//WtMauq/\nzGJA0tAqqXZfN+Bhd+9dRNv3gL7uPqvAa5m4T2rcuCjVctBBca9G8+ZpRyQixfjuO7j5ZvjDH2JZ\nnEsu0fXjcspy7T6jhutOuetGmVk/Imkuk6CyYvhw2HVXOPfcqF6uBCVSOZo3jxUHJk6MpUH69o1q\nMHPnph2Z1KbUy8ffRtxf1dPMPjSz48zsJDM7MWlykJm9bWajgSuAQ0sZz/Jyj6rMJ5wADzwARx2V\ndkQisrxat46lQd56K6aqb7wxXH999LQke1S7rw6LF8P//R+MGBE9qW7dyh6CiJTQG2/EOT5nDlx5\nZVRfl4anArMlMG8eHHYYfPst3H13VDEXkcbHHe69NyZX9O0Ll18eMwSl4WT5mlRF+uQTGDAg1nwa\nPlwJSqQxM4vJUOPHw5Zbxs3555wTS9tLupSkCnj3XdhhB9h//1jivWXLtCMSkXJo1Spm/40ZE0Vs\nN9kkRlEqZMCpUdJwX55XXoH99oMLLoCf/azkbyciGfbf/8IvfgFrrRUVZTatmHID2ZPJ4b66Cswm\nba4ys0lJ1YktSxlPXZ54IpaqHjpUCUpEYsj/jTfggAPi8W9/C199lXZUTUuph/tqLTCbVJno4e4b\nAScB/yxxPDW6++6YWv7AA7DPPmlFISJZ06JF9KbefjuqzWyySax2UCGDUBWvpEmqiAKz+wHDkrYv\nA21yb/Atl3/9K9ameeop2FErZYlIAR06wI03RoK66CL44Q/jxmAprbQnTnQGPsp5Pj3ZVzZ//Sv8\n6U8x9qxlqEWkLjvuCK+/HpcGdtwxJlp8/XXaUTVeaReYrZeGLjB74YUwbBiMHAnrrbdisYlI09Gi\nRdwAfMghscji5pvHStyDBqUdWXY0igKzZvZP4Dl3vzN5/g4wwN1nFmjbYLP73OMeiPvvh6efjnuh\nRESW1+OPw6mnxv1VV1yh/1MKyeTsvkSNBWaJxROPBjCz/sCXhRJUQ3KPGTrDh0epI/0yiciKGjQo\nJlZstBH07h0LLVZVpR1V41DSnlRSYHYg0B6YCQwBVgLc3Ycmba4BBgHzgePc/Y0ajrXCPSl3+M1v\n4vrTU09B27YrdDgRkWWMGwcnnhhJauhQ2GKLtCPKBtXuq4MSlIiUS1VVVFb/wx8iYZ1zDqyyStpR\npSvLw32pU4ISkXJq1gxOOgnGjo0ya717x+UFqb9G35Nyh7PPjuT09NNKUCJSfg89FBMrBg2KCutr\nrpl2ROWnnlQNzj8fHn0UnnxSCUpE0rHvvjGxokUL2GyzqGwjxSl5kjKzQWb2jpm9a2ZnFnh9gJl9\naWZvJNsfGuq9L70U7rgjelHt2zfUUUVE6q9Nm5j1d/vtcMYZcY/VzJLOZW4cSl1gthlwDVG/bzPg\ncDPrVaDpSHffOtkubIj3vuaamFnz9NNRzkREJAt22SWWAtlgg7hWdcstqgNYm1L3pPoBk9z9A3df\nBNxB1OvLV+9xytrcfHP0op5+GjqXtciSiEjdWrWK/6OGD4fLLovhwOnT044qm0qdpPJr802jcG2+\n7ZOlOoab2Qqt2PLgg3Gz7hNPaPlnEcm2bbaB116LJeu32ioK2KpXtbQs1O57Hejq7guSpTseAHoW\nalhX7b7nnoMTToiJElqcTEQqwUorwbnnxppVxx0XywYNHQpduqQd2YqpiNp9Samjc919UPL8LKLa\nxKW1/Jv3gL7uPitvf61T0EePhj33jDL6u+7aMPGLiJTTokVw8cVxTf3SS+HYY8Ea9GJIejJZccLM\nmgMTgd2Bj4FXgMPdfUJOmw7V9frMrB9wl7uvX+BYNSapqVNh553hyivhoIMa/vsQESmnMWMiQXXq\nFJUrOnVKO6IVl8n7pNz9O+DnwJPAOOAOd59gZieZ2YlJs4PM7G0zGw1cARxan/f49NPoQf3+90pQ\nItI49OkDr7wC224LW24Jt97adK9VVXTFifnzYeDAuIv7ggvSiUtEpJRefx2OOQZ69oTrroO11047\nouWTyZ5UKS1eDIcdFouNnX9+2tGIiJRG376RqKqXAWlq1SoqsiflHnWwJk+O+wxatkw5OBGRMnjx\nxehV7bgjXHVVVLGoFE2qJ3X55fHDuuceJSgRaTp23BHefBNWXTV6Vc89l3ZEpZd67b6kzVVmNim5\noXfL2o53zz1w9dXRg1pjjdLEvLwa4p6AUlOMDUMxNgzFWH+rrRY1AP/5TzjySPjVr+DJJ0ekHVbJ\npF67L7mBt4e7bwScBPyzpuO98gqcfHKUvc/ijW5Z+2UuRDE2DMXYMBTj8hs8ONar+vxzeOSREWmH\nUzJZqN23HzAMwN1fBtqYWcGSsAccADfcEOVDRESauvbtYdgwaNcu7UhKJwu1+/LbTC/QBohu7b77\nNmh8IiKSYaWuOPFjYE93PzF5fiTQz91Py2nzMHCxu7+UPH8aOMPd38g7VmVMQxQRkYKWZ3ZfqQvM\nTge65jzvkuzLb7NeHW2W65sTEZHKVurhvleBDc2sm5mtBBwGPJTX5iHgaPi+IO2X1bX8RESkaStp\nT8rdvzOz6tp9zYAbqmv3xcs+1N0fNbO9zGwyMB84rpQxiYhI5aiYihMiItL0ZK7iREPf/JtGjGZ2\nhJmNSbYXzGyLrMWY025bM1tkZgeWM77kvYv5WQ80s9FJpfyy319fxM+6vZk9lvwuvmVmx5Y5vhvM\nbKaZja2lTdrnS60xZuR8qfNzTNqleb4U87NO+3yp62dd//PF3TOzEUlzMtANaAm8CfTKazMYGJ48\n3g4YlcEY+wNtkseDshhjTrtngEeAA7MWI9CGWOKlc/J8rQzGOISYnQqwFvAF0KKMMe4EbAmMreH1\nVM+XImNM9XwpJsac34dUzpciP8dUz5ciY6z3+ZK1nlSD3vybVozuPsrd5yRPR1HDfV9pxpj4BXAP\n8Gk5g0sUE+MRwL3uPh3A3T/PYIyfAKsnj1cHvnD3xeUK0N1fAGbX0iTt86XOGDNwvhTzOUK650sx\nMaZ9vhQTY73Pl6wlqQa9+bdEiokx18+Ax0oa0bLqjNHMOgH7u/u1QBrT+4v5HHsC7czsOTN71cyO\nKlt0oZgYrwc2M7MZwBjg9DLFVqy0z5f6SuN8qVMGzpdipH2+FKPe50up75Nq0sxsV2K24k5px1LA\nFUDuNZYsnngtgK2B3YDWwP/M7H/uPjndsJZyNjDG3Xc1sx7AU2bW293npR1YpdH5ssIa5fmStSTV\nYDf/llAxMWJmvYGhwCB3r2sYoaEVE+M2wB1mZsTY8GAzW+Tu+fexpRnjNOBzd/8G+MbMRgJ9iOtE\n5VBMjDsCfwJw9ylm9h7QC3itLBHWLe3zpSgpny/FSPt8KUba50sx6n2+ZG24rxJu/q0zRjPrCtwL\nHOXuU8oYW9Exunv3ZNuAGGc/pcwnXDE/6weBncysuZmtSlz4n5CxGCcAewAk13p6AlPLGCPEX/U1\n/WWf9vlSrcYYM3C+fB8KNcSYgfOlWm0/67TPl2q1xVjv8yVTPSmvgJt/i4kROAdoB/wj+ctrkbv3\ny1iMS/2TcsVWnxjd/R0zewIYC3wHDHX38VmKEbgYuNHMxhAn5hnuPqtcMZrZbcBAoL2ZfUjMnlqJ\njJwvxcRIyudLkTHmSuXm0iJ+1qmeL8XEyHKcL7qZV0REMitrw30iIiLfU5ISEZHMUpISEZHMUpIS\nEZHMUpISEZHMUpISEZHMUpISSZmZtTGzk9OOQySLlKRE0tcWOCXtIESySElKJH0XA93N7A0zuzTt\nYESyRBUnRFJmZt2Ah929d9qxiGSNelIiIpJZSlIiIpJZSlIi6fuKJUtqi0gOJSmRlCVLFbxoZmM1\ncUJkaZo4ISIimaWelIiIZJaSlIiIZJaSlIiIZJaSlIiIZJaSlIiIZJaSlIiIZJaSlIiIZJaSlIiI\nZJaSlIiIZJaSlIiIZJaSlDRKZjbAzD5awWOsZ2ZzzcwaKq5KZGbXmtnv045DmiYlKckkM3vMzM4t\nsH8/M/vYzIr53V2hwpTu/pG7r+FJgUsze87Mjl+RY2admR1jZs/n7nP3k939T2nFJE2bkpRk1U3A\nkQX2Hwnc7O5VZY6n0TGz5oV2s4LJXaQhKUlJVj0AtDeznap3mNmawI+Am5PnK5nZn83sg6R39Q8z\nW7nQwcysV9ITmm1mb5nZPjmvrWJmfzGz95PXR5rZymbWzcyqzKyZmV0I7AxckwwBXmVm15jZn/Pe\n50EzO73A+//DzC7P2/eAmf1fDfHuYGavJPG8bGbbJ/sPMbNX89r+0sweqOszqR4CNbMzzOxj4N/5\nnxFwLbC9mX1lZrOS/Tea2fl5x/itmX1qZtPNbH8zG2xm75rZ52Z2Vs4xzczOMrPJZvaZmd2R/BxF\niuPu2rRlcgOGAkNznp8EvJHz/G9EMmsDtAYeBP6UvDYA+DB53AKYBJyZPN4VmAtslLz+d+BZYF2i\nJ9EfaAl0A74DmiXtngOOz3n/bYFpOc/bA/OAtQp8LzsDH+Q8XxOYD3Qo0LYtMAs4gvhD8rDkeVug\nFTAH6JHT/hXg4CI/k0XARcn3t3KB9z4GGJm370bg/Lxj/B5oDvwM+Ay4FVgV2BRYAHRL2p8OvAR0\nTN7zWuC2tH+3tFXOlnoA2rTVtAE7ArOBlZLnLwCn57w+D9gg5/n2wNTkcW6S2hmYkXfs24A/Jklp\nAbB5gfevNUkl+8YBuyePTwUeqeX7eR/YKXn8M+DpGtodCYzK2/cScHTy+GbgD8njjZKktXKRn8k3\nQMtaYixsF0TiAAAXfklEQVQmSc1nyVp0qwFVwDY57V8D9k0ejwd2zXmtI/Bt9WeqTVtdm4b7JLPc\n/UXir/T9zaw70XO5DcDM1ib+cn/dzGYlQ1OPEb2ZfB2B/Jl+HwCdgbWAVYCpyxnmzSy5dnZk8rwm\ndwKHJ4+PIHofhXRK4stVHS/EZ5B7nAfcfWGRn8ln7r6o9m+pTl+4e/V1q6+Tr5/mvP41kbwgEv39\nOfGMJ3piHVYwBmkilKQk624m/ro/EnjC3T9L9n9O9IA2c/d2ybamu7cpcIwZwHp5+7oC05PjfAP0\nKCKWQhMKbgb2M7PeQC9iqK0mtwMHmVlXYDvg3hrazQDWryFegKeAtc2sDzEUeFuyv5jPpK5JEQ09\naeJDYHBOPG3dvbW7f9zA7yONlJKUZN0wYA9ieOym6p3JX/LXA1ckPQjMrLOZ/bDAMV4GFiQTBlqY\n2UBiAsbtyXH+DfzVzDomkyT6m1nL5N/m3iM1E+iee2B3nw68TiSre919YU3fiLu/CXwB/At43N3n\n1tD0UWAjMzvMzJqb2aHAJsAjyXEWA3cDlxPXqZ5ajs+kJjOBLjnf/4q6DrgoScyY2dpmtm8DHVua\nACUpyTR3/4C4HrMq8FDey2cCk4FRZvYl8CTQs8AxFgH7AHsRvY1rgKPcfVLS5NfAW8CrRBK5hCXn\nRm7P4krgYDP7wsyuyNl/E7A5kVDrchuwOzUP9eHus4gk+psk3t8Aeyf7q92eHOcuX3o6flGfSS2e\nJa6zfWJmn9bVuDrkWp5fSUzeeNLM5hA/y371iEeaOFsytFyGNzO7gTj5Zrp772RfW2KsvhtxYfkQ\nd59TtqBEVlAyTf4Wd18/7VhEGpty96RuBPbM23cWMctpY+KvuLPLHJPIckuGxf6PGGYTkQZW1iTl\n7i8QU4pz7ceSaw03AfuXMyaR5ZXc/DqbmKl2ZcrhiDRKLdIOAFjH3WcCuPsnZrZO2gGJFMPd32HJ\nVGsRKYEsJKl8BS+SmZnqiYmIVDB3r/eKAlmY3TfTzDoAmNm6LH1T4FImTnSOOMJZZx3nssucefPS\nvxs6dxsyZEjqMShGxagYFWMWt+WVRpIylr735CHg2OTxMcR01YJ69oRbb4VnnoFXXoENN4S//Q2+\n/rqmfyEiIpWsrEnKzG4j7pPoaWYfmtlxxD0pPzCzicR9H5fUdZzNN4e774bHH4eRI6FHD7jiCiUr\nEZHGptyz+45w907uvrK7d3X3G919trvv4e4bu/sP3f3LYo/Xpw/cfz88+uiSZPXXv8L8+aX8Lmo2\ncODAdN64HhRjw1CMDUMxNoxKiHF5lfVm3hVhZl5XrGPGwIUXwvPPw69+BSefDKuvXqYARUSkRmaG\nV+jEiQbTp08MAz79NIweDd27wwUXwJdF981ERCRLMpGkzOxsMxtnZmPN7FYzW2lFjrf55nD77dGj\nmjIlhgF/9zv4tNhKZCIikgmpJykz6wacAGzlUc+vBbH8wArr1Qv+8x947TWYPTuen346fJS/spCI\niGRS6kmKWMb7W6C1mbUgql3PaMg32GADuPZaGDcOVloJttwSjj8e3nmnId9FREQaWupJyt1nA38h\nFkebDnzp7k+X4r06doTLL4dJk+J61YABcMAB8PLLpXg3ERFZUanP7kuWBX8E2AmYA9wD3O3ut+W1\n8yFDhnz/fODAgSs87XLBArjhBvjLX6BbNzjjDBg8GJqlnrpFRCrbiBEjGDFixPfPzzvvvOWa3ZeF\nJHUI8AN3PyF5fhSwnbv/PK9dnVPQl9fixXDPPXDZZfDtt/DrX8MRR8DKK5fk7UREmpxKnoI+Eehv\nZquYmRFVJyaUM4AWLeCww+D116NyxZ13xnWsiy6CWbPq/vciIlIaqScpdx9DLLv9OjCGqOs3NI1Y\nzGCPPaLc0uOPw8SJUR/w5z+P61giIlJeqQ/3FauUw321+fhj+Pvf4brrYIcd4Je/jAkXVu9Oq4hI\n07W8w31KUkVasABuvjmGA1dZJe63OuyweCwiIrWr+CRlZm2AfwGbA1XA8e7+cs7rqSapalVV8OST\nkazefBNOPDFqBHbsmHZkIiLZVckTJ6pdCTzq7psAfSjz5IliNWsGgwbFNasRI+CLL2CzzWI24P/+\nBxnIoyIijUYmelJmtgYw2t171NImEz2pQr78MsovXXMNrLkmnHpqDAW2apV2ZCIi2VDRw31m1oeY\n0Tee6EW9Bpzu7l/ntMlskqpWVRU9rL//PVYOPvbYGArs3j3tyERE0rW8SapFKYJZDi2ArYFT3f01\nM7sCOAsYktvo3HPP/f5xQ1ScaGjNmsFee8U2ZUrUC9xuO9hmm0hWe+8NzZunHaWISOnlV5xYXlnp\nSXUA/ufu3ZPnOwFnuvs+OW0y35Mq5Ouv4a67ImHNmBETLX76U020EJGmpaInTrj7TOAjM+uZ7Nqd\nGPqreK1awTHHwKhR8OCDMG0abLopHHRQzBKsqko7QhGR7MpETwq+vy71L6AlMBU4zt3n5LxekT2p\nQubOhdtug6FDY9LFCSfE9Sv1rkSksaroiRPFaExJqpp71Au87roocDtgQCSsPfeMeoIiIo2FklSF\n++qruHZ1/fUxJHjMMbEwY48aJ+WLiFSOir4mJbD66jGhYtSomMa+YAH07w+77hrlmBYsSDtCEZHy\ny0xPysyaEfdHTXP3fQu83qh7UoUsXAgPPQQ33hjVLA4+OK5dbb+9CtyKSGWp+OE+M/sl0BdYQ0lq\nWdOnw7BhcNNNMSPwmGPgqKOga9e0IxMRqVtFD/eZWRdgL2J2nxTQuTOcfTZMmBDJato02Gor2H33\nSFzz5qUdoYhIw8tEkgL+BvwWaLpdpSKZxbWqa6+N3tXJJ8O990KXLnDkkfDEE7B4cdpRiog0jNQn\nOpvZ3sBMd3/TzAYSK/MWlPWySOW2yipxU/BBB8Fnn8Edd8A558R1q8MOg5/8BPr21fUrESm/RlMW\nycwuAo4EFgOtgNWB+9z96Lx2TfqaVH1MnBg3C99yC7RsGcuIHH44bLRR2pGJSFNV8RMnAMxsAPBr\nTZxoGO7w8suRsO68E7p1i2R1yCFxjUtEpFwqeuKElEb19aurrorrVxdeCGPHwhZbwMCB8M9/xjCh\niEhWZaonVRv1pBrON9/EDcN33AGPPRbLiRx6KBxwALRrl3Z0ItIYNYrhvtooSZXG/PkwfHgMBz79\nNOywQ9w0vP/+Slgi0nCUpGSFzZsHjzwCd98dCWv77WPm4P77w1prpR2diFSyik5Syc28w4AOQBVw\nvbtflddGSaqM5s2DRx+NhPXkk7G68I9/HEOCWlJEROqr0pPUusC6yb1SqwGvA/u5+zs5bZSkUrJg\nQdwkfO+9MTS42WaRrA44ALp3Tzs6EakEFZ2k8pnZA8DV7v5Mzj4lqQz49lt49lm4775YabhjxxgO\n3H9/6NNHNw6LSGElT1Jm9ivgTnefXt83qVdAZusDI4DN3X1ezn4lqYz57ruozv7AA3D//fF8v/1g\n331hl13iRmIREVj+JFWfskirA0+a2SzgTuBud59Z3zesTTLUdw9wem6CqqaySNnSvDnstFNsl18O\n48ZF7+rss2HyZBg0KBLWoEGw5pppRysi5ZRaWSQz6w0cCvyYWPtpjxWOIo7bAngEeMzdryzwunpS\nFWTGjJgp+NBDMHIkbLst/OhHsM8+sOGGaUcnIuVWtmtSySSHg4HDgNXdvXd937SG4w4DPnf3X9Xw\nupJUhZo/P6a0P/JIbGusEQlr771h5501LCjSFJTjmtQpwCHA2sDdwF3uPr6+b1jDsXcERgJvEct1\nOPA7d388p42SVCNQVQVvvBGzBIcPh3ffhT32gL32gsGDNb1dpLEqR5K6mJg48WZ936QhKEk1TjNn\nRommRx+N+7HWXz+S1aBBcTOxelkijUOjmoJeiJJU47d4MYwaFfUEH3sMpk6NlYf33DOSVteuaUco\nIsuropOUmQ0CriCqst/g7pcWaKMk1cTMnBm9q8cfj6/t20fC2nNPGDAAWrdOO0IRKVbFJikzawa8\nC+wOzABeBQ7LrTaRtFOSasKqqmD06Kh88cQT8PrrMWPwBz+IbeutY0q8iGRTJSep/sAQdx+cPD8L\n8PzelJKU5Jo3D/773+hhPf00fPwx7LprTMLYY4+Y5q7qFyLZUclJ6sfAnu5+YvL8SKCfu5+W105J\nSmo0Y0Ykq2eeia/Nm8f1rN13h912g06d0o5QpGnTyrzSpHXqBEcfDTfdBNOmLancfv/9sRJxr15w\n8slw113w6adpRysixapPWaRSmQ7kztvqkuxbhsoiSTHMIin16gWnnhrXs8aMgeeeg2HD4IQTYL31\nYODAGCLcZRdYe+20oxZpXFIri9TQzKw5MJGYOPEx8ApwuLtPyGun4T5pEIsXxySMESMicb34InTp\nEjMGBwyIpKWbikUaVsVek4Lvp6BfyZIp6JcUaKMkJSWxeDG8+WZMxPjvf+GFF2Il4l12ibJNO+8M\nG2ygiRgiK6Kik1QxlKSkXKqq4O23ozDu88/H1qzZkorvO+8Mm2+uKe8i9aEkJVIi7lH94vnno5f1\n/PPwySfQv38krR13hH79YLXV0o5UJLuUpETK6LPP4KWXImm9+GJMzOjVC3bYIWoO7rADdOumIUKR\nahWbpMzsMmAfYCEwBTjO3ecWaKckJZn1zTdR3f2ll2K14pdeigTVv38krf79oW9fWHXVtCMVSUcl\nJ6k9gGfdvcrMLiGqTZxdoJ2SlFQMd/jgg0hYo0bF13HjorfVvz9st11sG20U17tEGruKTVK5zGx/\n4MfuflSB15SkpKJ9801MfR81Cl5+ObYvv4wahP36LdnWXTftSEUaXmNJUg8Bd7j7bQVeU5KSRufT\nT+GVV5beWreOxFW99e0LbdumHanIisl0kjKzp4AOubuI1Xd/7+4PJ21+D2zt7j+u4Rg+ZMiQ75+r\n4oQ0RtUzCV99dck2ejR06BBlnvr2ja9bbw1t2qQdrUjN8itOnHfeedlNUnUGYXYscAKwm7svrKGN\nelLSJH33HUycCK+9FkuUvP563HzcsWMkra23jq9bbQXt2qUdrUhhme5J1RpAVJv4C7CLu39RSzsl\nKZFEdeKqTlpvvBGJq23bSFpbbQVbbhlfu3TRVHhJXyUnqUnASkB1ghrl7qcUaKckJVKLqiqYMiUS\n1ujRkbRGj46EVp2w+vSJxxtvDC1bph2xNCUVm6SKpSQlUn/uUR2jOmGNGROPP/oopsP36QO9ey/5\nutZaaUcsjZWSlIgUbf78qE84ZgyMHRtf33orbjbu3TvW4KreNtkEVlkl7Yil0ilJicgKcY8e1tix\nkbCqv06ZEiWettgiCutuvjlsthlsuCG0yMKKdFIRKj5JmdmvgcuBtdx9VoHXlaREUvDtt/Duu5Gw\n3norKmeMGwfTp0PPnpGwqrdNN4UePVQhXpZV0UnKzLoA/wI2BvoqSYlk3/z5MGHCkqQ1fnxsn3wS\n5Z422SSS1iabxLbRRrDyymlHLWmp9CR1N3A+8BBKUiIVbf78mB5fnbQmTIjt/feha9dIWL16Lfm6\n8caqqNEUVGySMrN9gYHu/iszew8lKZFG6dtvYfLkSFjvvLPk68SJUQpq442X3dZfX1PlG4vlTVJl\nuexZS1mkPwC/A36Q91pB55577vePVRZJpLKstFIM/2266dL73WHGjCUJa+JEeOqpuA42Y0Ykqp49\nl2wbbRRbp06qIJ9l+WWRlleqPSkz2xx4GlhAJKcuwHSgn7t/mtdWPSmRJuabb2J24aRJkbQmTozH\nkybB3Lkxw3DDDSNp5X7t2FEJLGsqdrgvVzLct7W7zy7wmpKUiHxv7twYPpw8eUnimjIlns+dCxts\nEAmrR4+lt27dNISYhsaSpKYC2+ialIisiK++imry1UlsypTYpk6NIcROnSJhde++5OsGG8TXtm1V\n67AUGkWSqo2SlIg0hG+/jVWTp06NxPXee0s/hkhYudv66y/5utpqaUZfuZSkRERWkDvMnh3JKn/7\n4IOYRt+6dSSrbt2WfM19rHW+ClOSEhEpMXeYOXNJwnr//Xhcvb3/fpSK6tp1SfLq2nXprWPHplmR\no6KTlJn9AjgFWAwMd/ezCrTJfJIaMWJE5qfFK8aGoRgbRmOLsbonVp20Pvwwtg8+iLqIH34IX3wR\niWq99QpvXbrA2mvX77pYJXyOmb5PqjZmNhDYB9jC3RebWcUuFlAJvyiKsWEoxobR2GI0i9WR27WL\n9bsKWbgw6h5+9NGSxDV+PDzxRDyfNg3mzYPOnSNhVW/Vz6u/duiwpMBvJXyOyyv1JAWcDFzi7osB\n3P3zlOMRESmZlVeOWYTdu9fc5uuvI1nlbhMnwrPPLnn+xRewzjqRtPr1K1/85ZaFJNUT2MXMLgK+\nBn7r7q+lHJOISGpatVpSWaMmixbBxx9Hr+zhh8sXW7mV5ZpUHWWR/gQ86+6nm9m2wJ3uvszfGGaW\n7QtSIiJSq8xek3L3H9T0mpn9P+C+pN2rZlZlZu3d/Yu8Y+j2OhGRJiYL1a0eAHYDMLOeQMv8BCUi\nIk1TFq5J3Qj828zeAhYCR6ccj4iIZEQm7pMSEREpJAvDfUsxs0Fm9o6ZvWtmZ9bQ5iozm2Rmb5rZ\nllmL0cyOMLMxyfaCmW2RtRhz2m1rZovM7MByxpe8dzE/64FmNtrM3jaz57IWo5m1N7PHkt/Ft8zs\n2DLHd4OZzTSzsbW0Sft8qTXGjJwvdX6OSbs0z5diftZpny91/azrf764e2Y2ImlOBroBLYE3gV55\nbQYTVSkAtgNGZTDG/kCb5PGgLMaY0+4Z4BHgwKzFCLQBxgGdk+drZTDGIcDF1fEBXwAtyhjjTsCW\nwNgaXk/1fCkyxlTPl2JizPl9SOV8KfJzTPV8KTLGep8vWetJ9QMmufsH7r4IuAPYL6/NfsAwAHd/\nGWhjZh0onzpjdPdR7j4neToK6FzG+IqKMfEL4B7g0wKvlVoxMR4B3Ovu0yGVG72LifETYPXk8erA\nF57cmF4O7v4CsMz6aznSPl/qjDED50sxnyOke74UE2Pa50sxMdb7fMlakuoMfJTzfBrL/sLmt5le\noE0pFRNjrp8Bj5U0omXVGaOZdQL2d/drifvWyq2Yz7En0M7MnjOzV83sqLJFF4qJ8XpgMzObAYwB\nTi9TbMVK+3yprzTOlzpl4HwpRtrnSzHqfb5kYXZfo2VmuwLHEV3grLkCyL3GksUTrwWwNXGLQmvg\nf2b2P3efnG5YSzkbGOPuu5pZD+ApM+vt7vPSDqzS6HxZYY3yfMlakpoOdM153iXZl99mvTralFIx\nMWJmvYGhwCB3r2sYoaEVE+M2wB1mZsTY8GAzW+TuD2UoxmnA5+7+DfCNmY0E+hDXicqhmBh3JKqm\n4O5TzOw9oBeQldJeaZ8vRUn5fClG2udLMdI+X4pR7/Mla8N9rwIbmlk3M1sJOAzI/yV4iOReKjPr\nD3zp7jOzFKOZdQXuBY5y9ylljK3oGN29e7JtQIyzn1LmE66Yn/WDwE5m1tzMViUu/E/IWIwTgD0A\nkms9PYGpZYwR4q/6mv6yT/t8qVZjjBk4X74PhRpizMD5Uq22n3Xa50u12mKs9/mSqZ6Uu39nZj8H\nniQS6A3uPsHMToqXfai7P2pme5nZZGA+MTyQqRiBc4B2wD+Sv7wWuXvZ6hQXGeNS/6RcsdUnRnd/\nx8yeAMYC3wFD3X18lmIELgZuNLMxxIl5hrvPKleMZnYbMBBob2YfErOnViIj50sxMZLy+VJkjLlS\nubm0iJ91qudLMTGyHOeLbuYVEZHMytpwn4iIyPeUpEREJLOUpEREJLOUpEREJLOUpEREJLOUpERE\nJLOUpERSZmZtzOzktOMQySIlKZH0tQVOSTsIkSxSkhJJ38VAdzN7w8wuTTsYkSxRxQmRlJlZN+Bh\nd++ddiwiWaOelIiIZJaSlIiIZJaSlEj6vmLJktoikkNJSiRlyVIFL5rZWE2cEFmaJk6IiEhmqScl\nIiKZpSQlIiKZpSQlIiKZpSQlIiKZpSQlIiKZpSQlIiKZpSQlIiKZ9f8BwSkstGJxZWIAAAAASUVO\nRK5CYII=\n",
      "text/plain": [
       "<matplotlib.figure.Figure at 0x7f6430f78ef0>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "%matplotlib inline\n",
    "import matplotlib\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "\n",
    "m = 1.0  # kg\n",
    "R = 1.0  # kg / s\n",
    "F0 = -9.8 * m  # kg * m / s^2\n",
    "x0 = 0  # m\n",
    "v0 = 10.0  # m / s\n",
    "delta_t = 1.6  # s\n",
    "t = np.linspace(0.0, delta_t, 100+1)\n",
    "x = x0 + F0*t/R + (m/R)*(v0 - F0/R)*(1 - np.exp(-R*t/m))\n",
    "v = v0*np.exp(-R*t/m) + (F0/R)*(1 - np.exp(-R*t/m))\n",
    "\n",
    "fig = plt.figure()\n",
    "nrows, ncols = 2, 1\n",
    "\n",
    "ax1 = fig.add_subplot(nrows, ncols, 1)\n",
    "ax1.set_title(\"Position x over time\")\n",
    "ax1.set_xlabel(\"t\")\n",
    "ax1.set_ylabel(\"x\")\n",
    "ax1.plot(t, x)\n",
    "\n",
    "ax2 = fig.add_subplot(nrows, ncols, 2)\n",
    "ax2.set_title(\"Velocity v over time\")\n",
    "ax2.set_xlabel(\"t\")\n",
    "ax2.set_ylabel(\"v\")\n",
    "ax2.plot(t, v)\n",
    "\n",
    "fig.tight_layout()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
