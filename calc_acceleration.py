"""
Calculate acceleration based on photosensor data

Usage:
    calc_acceleration.py test
    calc_acceleration.py sim-vs-measured
    calc_acceleration.py sensitivity-single-time-var
    calc_acceleration.py [options] sensitivity-single-pos-var

Options:
    --num-sensors=N     [default: 3]
    --distance=D        Distance in meters from first to last sensor

If --distance is not given, default is distance from actual apparatus,
about 0.14 meters.
"""
import math
import sys

import docopt
import matplotlib.pyplot as plt
import numpy as np

# Light sensor positions, measured in meters from the top of the drop tube,
# of an actual device I constructed.
EXAMPLE_Y_ARRAY = np.array([0.0914, 0.1602, 0.2321])


def calc_acceleration(x, y):
    """
    Calculate the acceleration of a falling object.
    x: Sequence of time measurements of falling object
    y: Sequence of vertical position measurements
    Return (y0, v0, accel) where:
        y0 is the position at time zero
        v0 is the velocity at time zero
        accel is the acceleration
    Calculated using quadratic polynomial regression.
    """
    # https://en.wikipedia.org/wiki/Polynomial_regression
    if len(x) != len(y):
        raise ValueError("x and y must be arrays with the same length")
    if len(x) < 3:
        raise ValueError("x and y must have at least three items")
    A = np.vstack([np.ones(len(x)), x, x * x]).T
    y0, v0, a = np.linalg.lstsq(A, y, rcond=None)[0]
    return (y0, v0, a * 2)


def calc_ideal_curve(y0, v0, accel, t_array, t_len_scale=10):
    """
    y0: Vertical position at time zero
    v0: Vertical velocity at time zero
    accel: Acceleration
    t_array: Measured time values
    Return (t_ideal, y_ideal), where:
    t_ideal:
        Calculated time values, evenly spread over same range as t_array,
        but with t_len_scale times as many values.
    y_ideal:
        Calculated y values (vertical positions) for each time in t_ideal.
    """
    if t_len_scale == 1:
        t_ideal = t_array[:]
    else:
        t_ideal = np.linspace(t_array[0], t_array[-1],
                              len(t_array) * t_len_scale)
    y_ideal = y0 + (v0 * t_ideal) + (0.5 * accel * t_ideal * t_ideal)
    return (t_ideal, y_ideal)


def calc_t_for_y(y0, v0, accel, y):
    """
    Calculate a time t to for the given y value
    y0: Vertical position at time zero
    v0: Vertical velocity at time zero
    accel: Acceleration
    Return a time in seconds >= 0
    Raise ValueError if no unique real solution >= 0
    """
    r = (v0 * v0) - (2 * accel * (y0 - y))
    if r < 0.0:
        raise ValueError("No real solution")
    r = math.sqrt(r)
    t1 = (-v0 + r) / accel
    t2 = (-v0 - r) / accel
    if t1 < 0.0:
        if t2 < 0.0:
            raise ValueError("No real solution >= 0")
        return t2
    elif t2 >= 0.0:
        if t1 == t2:
            return t1
        raise ValueError("No unique real solution >= 0")
    else:
        return t1


def test():
    """Ideal test showing nice parabolic curve"""
    g = 9.8
    n = 11
    t_array = np.zeros(n)
    y_array = np.zeros(n)
    for i in range(n):
        t = i * 0.1
        y = 0.5 * g * (t ** 2)
        t_array[i] = t
        y_array[i] = y
    print("g =", g)
    print("t_array =", t_array)
    print("y_array =", y_array)
    y0, v0, accel = calc_acceleration(t_array, y_array)
    print(f"y0={y0}, v0={v0}, accel={accel}")
    curve_t, fitted_curve = calc_ideal_curve(y0, v0, accel, t_array)
    # Plot results
    fig = plt.figure(num=None, figsize=(8, 6), dpi=100)
    nrows, ncols = 1, 1
    ax1 = fig.add_subplot(nrows, ncols, 1)
    ax1.set_title("Simulated measurements and fitted quadratic curve")
    ax1.set_xlabel("t - time")
    ax1.set_ylabel("y - distance")
    ax1.plot(t_array, y_array, 'o', label="measured data", markersize=10)
    ax1.plot(curve_t, fitted_curve, 'r', label='fitted curve')
    ax1.legend()
    plt.show()


def sim_vs_measured():
    """Compare curve fits for simulated vs. measured drop data"""
    # Light sensor positions, measured in meters from the top of the drop tube
    y_array = EXAMPLE_Y_ARRAY

    # Generated simulated t measurements
    g = 9.8
    y0 = 0
    v0 = 0
    ideal_t = np.zeros(len(y_array))
    for i, y in enumerate(y_array):
        ideal_t[i] = calc_t_for_y(y0, v0, g, y)
    ideal_t -= ideal_t[0]

    # Compare with some real t measurements
    t_arrays = []
    for data in (
        [22.548994, 22.593104, 22.630960],
        [175.621487, 175.663531, 175.700481],
        [21.878047, 21.923396, 21.961608],
    ):
        t_array = np.array(data)
        t_array -= t_array[0]
        t_arrays.append(t_array)

    # Plot results
    fig = plt.figure(num=None, figsize=(8, 6), dpi=100)
    nrows, ncols = 1, 1
    ax1 = fig.add_subplot(nrows, ncols, 1)
    ax1.set_title("Measurements and fitted quadratic curves")
    ax1.set_xlabel("t - time")
    ax1.set_ylabel("y - distance")

    # Plot simulated data
    ax1.plot(ideal_t, y_array, 'o', color='black', markersize=5)
    y0, v0, accel = calc_acceleration(ideal_t, y_array)
    curve_t, fitted_curve = calc_ideal_curve(y0, v0, accel, ideal_t)
    ax1.plot(curve_t, fitted_curve,
             label=f'simulated, v0={v0:.4f}, accel={accel:.4f}')

    # Plot real data
    for t_array in t_arrays:
        ax1.plot(t_array, y_array, 'o', color='black', markersize=5)
        y0, v0, accel = calc_acceleration(t_array, y_array)
        curve_t, fitted_curve = calc_ideal_curve(y0, v0, accel, t_array)
        ax1.plot(curve_t, fitted_curve,
                 label=f'measured, v0={v0:.4f}, accel={accel:.4f}')

    ax1.legend()
    plt.show()


def sensitivity_single_time_var():
    """Measure sensitivity to changes in a single time measurement"""
    # Light sensor positions, measured in meters from the top of the drop tube
    y_array = EXAMPLE_Y_ARRAY

    # Generated simulated t measurements
    g = 9.8
    y0 = 0
    v0 = 0
    ideal_t = np.zeros(len(y_array))
    for i, y in enumerate(y_array):
        ideal_t[i] = calc_t_for_y(y0, v0, g, y)
    ideal_t -= ideal_t[0]

    # Modify the simulated measurements
    data = []
    for i in range(len(ideal_t)):
        data.append([])
        for err in (-30, -15, 0, 15, 30):
            t_array = np.zeros(len(ideal_t))
            t_array[:] = ideal_t[:]
            t_array[i] += err * 1e-6
            _, v0, accel = calc_acceleration(t_array, y_array)
            data[-1].append({
                "err": err,
                "v0": v0,
                "accel": accel,
            })

    # Show the results
    fig = plt.figure(num=None, figsize=(8, 6), dpi=100)
    nrows, ncols = 1, 1
    ax1 = fig.add_subplot(nrows, ncols, 1)
    ax1.set_title("Sensitivity to single time measurement error")
    ax1.set_xlabel("time error in microseconds")
    ax1.set_ylabel("apparent acceleration")

    for i, var_series in enumerate(data):
        ax1.plot([d['err'] for d in var_series],
                 [d['accel'] for d in var_series],
                 'o', label=f"t{i}", markersize=5)

    ax1.legend()
    plt.show()


def sensitivity_single_pos_var(n, d=None):
    """
    Measure sensitivity to changes in a single position measurement
    with n >= 3 light sensors evenly spaced
    """
    offset = 0.1
    # Light sensor positions, measured in meters from the top of the drop tube
    if not isinstance(n, int):
        raise TypeError("n must be an integer")
    if n < 3:
        raise ValueError("n must be an integer >= 3")
    if n == 3 and d is None:
        y_array = EXAMPLE_Y_ARRAY
    else:
        if d is None:
            y_array = np.linspace(EXAMPLE_Y_ARRAY[0], EXAMPLE_Y_ARRAY[-1], n)
        else:
            y_array = np.linspace(offset, d + offset, n)

    # Generated simulated t measurements
    g = 9.8
    y0 = 0
    v0 = 0
    ideal_t = np.zeros(len(y_array))
    for i, y in enumerate(y_array):
        ideal_t[i] = calc_t_for_y(y0, v0, g, y)
    ideal_t -= ideal_t[0]

    # Modify the simulated measurements
    data = []
    for i in range(len(ideal_t)):
        data.append([])
        for err in (-2, -1, 0, 1, 2):
            err_y_array = np.zeros(len(y_array))
            err_y_array[:] = y_array[:]
            err_y_array[i] += err * 0.001
            _, v0, accel = calc_acceleration(ideal_t, err_y_array)
            data[-1].append({
                "err": err,
                "v0": v0,
                "accel": accel,
            })

    # Show the results
    fig = plt.figure(num=None, figsize=(8, 6), dpi=100)
    nrows, ncols = 1, 1
    ax1 = fig.add_subplot(nrows, ncols, 1)
    ax1.set_title("Sensitivity to single position measurement error")
    ax1.set_xlabel("position error in millimeters")
    ax1.set_ylabel("apparent acceleration")

    for i, var_series in enumerate(data):
        ax1.plot([d['err'] for d in var_series],
                 [d['accel'] for d in var_series],
                 'o', label=f"t{i}", markersize=5)

    ax1.legend()
    plt.show()


def main():
    args = docopt.docopt(__doc__)
    if args['test']:
        return test()
    if args['sim-vs-measured']:
        return sim_vs_measured()
    if args['sensitivity-single-time-var']:
        return sensitivity_single_time_var()
    if args['sensitivity-single-pos-var']:
        n = int(args['--num-sensors'])
        d = args['--distance']
        if d is not None:
            d = float(d)
        return sensitivity_single_pos_var(n, d)
    print(f"Command not implemented: args={args}")
    return 1


if __name__ == '__main__':
    sys.exit(main())
