# Bug in sympy: Infinite loop in dsolve()
from sympy import Eq, dsolve, Symbol, symbols
x1, x2 = symbols('x1 x2', function=True, real=True)
t = Symbol('t', real=True, nonnegative=True)
eq1 = Eq(x1(t).diff(t, 2), -2*x1(t) + x2(t))
eq2 = Eq(x2(t).diff(t, 2), 0.5*x1(t) - x2(t))
# Uncomment this line, no more infinite loop (but not the answer I need.)
#eq2 = Eq(x2(t).diff(t, 2), x1(t) - 2*x2(t))
print(eq1)
print(eq2)
sol = dsolve((eq1, eq2))
print(sol)
