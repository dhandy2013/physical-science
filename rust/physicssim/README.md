# physicssim crate

Physics simulation and visualization

## Setup

This crate uses the [plotters](https://docs.rs/plotters/latest/plotters/) crate.
To build this crate on Linux you have to install some system prerequisites
first.

Ubuntu Linux:
```
sudo apt install pkg-config libfreetype6-dev libfontconfig1-dev
```

Fedora Linux:
```
sudo dnf install pkgconf freetype-devel fontconfig-devel
```
