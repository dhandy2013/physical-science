use anyhow::Context;
use clap::{Parser, Subcommand};
use physicssim::{euler::EulerForwardSolver, TimeSeriesGenerator};
use piston_window::{PistonWindow, WindowSettings};
use plotters::{
    coord::Shift,
    prelude::*,
    style::full_palette::{BLUEGREY_100, GREEN_400, ORANGE},
};
use plotters_piston::draw_piston_window;
use std::path::PathBuf;

/// Physics Simulator - solving fun differential equations and more
#[derive(Parser)]
#[command(version)]
struct Args {
    /// Name of image file to write output to, instead of displaying a graphical
    /// window.
    #[arg(short, long, value_name = "FILE")]
    png_path: Option<PathBuf>,

    #[command(subcommand)]
    command: Command,
}

#[derive(Copy, Clone, Debug, Subcommand)]
enum Command {
    /// Simple test graph of X^2
    PlotXSquared,
    /// Show forward Euler method
    ForwardEuler,
}

fn main() -> anyhow::Result<()> {
    let args = Args::parse();
    println!("Command: {:?}", args.command);
    if let Some(png_path) = args.png_path {
        println!("Output PNG path: {png_path:?}");
        let root =
            BitMapBackend::new(&png_path, (800, 600)).into_drawing_area();
        run_command_on_root_drawing_area(args.command, &root)?;
        root.present().context("rendering to bitmap")?;
        return Ok(());
    }

    // Display interactive window
    let mut window: PistonWindow =
        WindowSettings::new(format!("{:?}", args.command), [800, 600])
            .samples(4)
            .build()
            .map_err(|e| anyhow::anyhow!("building window: {:?}", e))?;

    while let Some(_) = draw_piston_window(&mut window, |backend| {
        let root = backend.into_drawing_area();
        run_command_on_root_drawing_area(args.command, &root)?;
        root.present().context("rendering to piston window")?;
        Ok(())
    }) {}

    Ok(())
}

fn run_command_on_root_drawing_area<DB>(
    command: Command,
    root: &DrawingArea<DB, Shift>,
) -> anyhow::Result<()>
where
    DB: DrawingBackend,
    DB::ErrorType: 'static,
{
    match command {
        Command::PlotXSquared => {
            plot_x_squared(&root).context("plot x squared")?;
        }
        Command::ForwardEuler => {
            plot_exponential_growth_forward_euler(&root)
                .context("plot exponential growth")?;
        }
    }
    return Ok(());
}

fn plot_x_squared<DB>(root: &DrawingArea<DB, Shift>) -> anyhow::Result<()>
where
    DB: DrawingBackend,
    DB::ErrorType: 'static,
{
    root.fill(&BLUEGREY_100)?;

    let mut chart = ChartBuilder::on(&root)
        .caption("y=x^2", ("sans-serif", 50).into_font())
        .margin(5)
        .x_label_area_size(30)
        .y_label_area_size(30)
        .build_cartesian_2d(-1f32..1f32, -0.1f32..1f32)
        .context("building ChartContext")?;

    chart.configure_mesh().draw().context("drawing mesh")?;

    let x_values = (-1f32..1f32).step(0.02);
    let series = x_values.values().map(|x| (x, x * x));

    chart
        .draw_series(LineSeries::new(series, &RED))
        .context("drawing line series")?
        .label("y = x^2")
        .legend(|(x, y)| PathElement::new(vec![(x, y), (x + 20, y)], &RED));

    chart
        .configure_series_labels()
        .background_style(&WHITE.mix(0.8))
        .border_style(&BLACK)
        .draw()
        .context("drawing series lablels")?;
    Ok(())
}

fn plot_exponential_growth_forward_euler<DB>(
    root: &DrawingArea<DB, Shift>,
) -> anyhow::Result<()>
where
    DB: DrawingBackend,
    DB::ErrorType: 'static,
{
    root.fill(&WHITE).context("fill background white")?;

    let max_x = 5.0;
    let mut chart = ChartBuilder::on(&root)
        .caption(
            "Computed solution y vs. time for different alphas",
            ("sans-serif", 30).into_font(),
        )
        .margin(5)
        .x_label_area_size(30)
        .y_label_area_size(30)
        .build_cartesian_2d(0f64..max_x, 0f64..4.5f64)
        .context("building ChartContext")?;

    chart.configure_mesh().draw().context("drawing mesh")?;

    let alpha_color_pairs =
        [(-0.8, BLUE), (-0.1, ORANGE), (0.1, GREEN_400), (0.3, RED)];

    for (alpha, color) in alpha_color_pairs {
        let dt = 0.1;
        let exact_solution =
            TimeSeriesGenerator::new(move |t| (alpha * t).exp(), dt, max_x);
        let y0 = 1.0;
        let solver = EulerForwardSolver::new(
            move |_t: f64, y: f64| alpha * y,
            dt,
            y0,
            max_x,
        );
        chart
            .draw_series(LineSeries::new(exact_solution, &RED))
            .with_context(|| format!("drawing line series for alpha={alpha}"))?
            .label(format!("alpha = {alpha}"))
            .legend(move |(x, y)| {
                PathElement::new(vec![(x, y), (x + 20, y)], &color)
            });
        chart
            .draw_series(PointSeries::of_element(
                solver,
                5,      // size
                &color, // style
                // element constructor
                &|coord, size, style| {
                    return EmptyElement::at(coord)
                        + Circle::new((0, 0), size, style);
                },
            ))
            .with_context(|| format!("drawing dots for alpha={alpha}"))?;
    }

    chart
        .configure_series_labels()
        .background_style(&WHITE.mix(0.8))
        .border_style(&BLACK)
        .label_font(("sans-serif", 20))
        .draw()
        .context("drawing series labels")?;

    Ok(())
}
