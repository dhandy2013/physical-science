//! Solve differential equations using variations of Euler's method

/// First-order Ordinary Differential Equation in the form:
///     dy/dt = f(t, y)
/// This trait object is closure that takes parameters (t, y) and returns dy/dt.
type FirstOrderODE = dyn Fn(f64, f64) -> f64;

/// Solve a first-order ordinary differential equation using the Forward Euler
/// method.
/// https://math.libretexts.org/Bookshelves/Differential_Equations/Numerically_Solving_Ordinary_Differential_Equations_(Brorson)/01%3A_Chapters/1.02%3A_Forward_Euler_method
pub struct EulerForwardSolver {
    diff_eq: Box<FirstOrderODE>,
    /// The time step for the solver to use
    dt: f64,
    t: f64,
    y: f64,
    t_limit: f64,
}

impl EulerForwardSolver {
    /// Construct the solver
    /// `diff_eq`:
    ///     The differential equation to solve, in the form dy/dx = f(t, y)
    /// `dt`:
    ///     The time step used by the solver (amount `t` changes each iteration)
    /// `y0`:
    ///     The initial value of `y`
    /// `t_limit`:
    ///     Iteration stops when `t` exceeds this value.
    pub fn new(
        diff_eq: impl Fn(f64, f64) -> f64 + 'static,
        dt: f64,
        y0: f64,
        t_limit: f64,
    ) -> Self {
        Self {
            diff_eq: Box::new(diff_eq),
            dt,
            t: 0.0,
            y: y0,
            t_limit,
        }
    }
}

impl Iterator for EulerForwardSolver {
    type Item = (f64, f64);

    fn next(&mut self) -> Option<Self::Item> {
        if self.t > self.t_limit {
            return None;
        }
        let result = Some((self.t, self.y));
        let dy_dt = (self.diff_eq)(self.t, self.y);
        self.t += self.dt;
        self.y += dy_dt * self.dt;
        result
    }
}
