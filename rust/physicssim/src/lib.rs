//! Physics Simulation Modules
pub mod euler;

pub fn gen_sine(dt: f64, n: usize) -> Vec<f64> {
    let mut result = Vec::with_capacity(n);
    let mut t: f64 = 0.0;
    for _ in 0..n {
        result.push(t.sin());
        t += dt;
    }
    result
}

pub struct TimeSeriesGenerator {
    f: Box<dyn FnMut(f64) -> f64 + 'static>,
    dt: f64,
    t_limit: f64,
    t: f64,
}

impl TimeSeriesGenerator {
    pub fn new<F>(f: F, dt: f64, t_limit: f64) -> Self
    where
        F: FnMut(f64) -> f64 + 'static,
    {
        Self {
            f: Box::new(f),
            dt,
            t_limit,
            t: 0.0,
        }
    }
}

impl Iterator for TimeSeriesGenerator {
    type Item = (f64, f64);

    fn next(&mut self) -> Option<Self::Item> {
        if self.t > self.t_limit {
            return None;
        }
        let result = (self.t, (self.f)(self.t));
        self.t += self.dt;
        Some(result)
    }
}
