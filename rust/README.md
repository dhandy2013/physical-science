# Physical Science apps written in the Rust programming language

- `bounce`: Collision simulation
- `bounce_py`: Pyo3 wrapper around ``bounce`` so it can be used in Python
- `bounce_web`: Animation of colliding objects in web browser
- `physicssim`: Particle motion simulation - gravity and springs
