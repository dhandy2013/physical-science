//! Collision-related types and functions
use crate::discid::DiscID;
use crate::types::{Length, Position, Time, Velocity};

#[derive(Clone, Copy, Debug)]
pub(crate) struct CollisionEvent {
    /// Time-Of-Impact, must be >= 0
    pub(crate) toi: Time,

    /// What happens at the event (in this case, a collision)
    pub(crate) collision: Collision,
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub(crate) enum Collision {
    /// Velocity is (-1, 1) for a horizontal bounce, (1, -1) for vertical.
    Wall(DiscID, Velocity),

    /// Collision between two discs identified by the IDs
    Disc(DiscID, DiscID),
}

impl CollisionEvent {
    pub fn new(toi: Time, collision: Collision) -> Self {
        Self { toi, collision }
    }

    pub fn replace_toi(self, toi: Time) -> Self {
        Self {
            toi,
            collision: self.collision,
        }
    }

    pub fn merge(self, other: Self) -> Self {
        if other.toi >= 0.0 && other.toi < self.toi {
            other
        } else {
            self
        }
    }
}

/// Combine two maybe-collision-events, returning the one that is non None
/// and has a lower time-of-impact.
pub(crate) fn merge_maybe_events(
    event1: Option<CollisionEvent>,
    event2: Option<CollisionEvent>,
) -> Option<CollisionEvent> {
    match (event1, event2) {
        (Some(ev1), Some(ev2)) => Some(ev1.merge(ev2)),
        (Some(ev1), None) => Some(ev1),
        (None, Some(ev2)) => Some(ev2),
        (None, None) => None,
    }
}

pub(crate) const CLOSE_ENOUGH_TO_ZERO: f64 = 1.0e-7;
pub(crate) const CLOSE_ENOUGH_TO_ZERO2: f64 = 1.0e-14;

/// Return the soonest time >=0 two circular objects will collide,
/// or None if they will not collide.
pub(crate) fn collision_toi(
    p1: &Position,
    v1: &Velocity,
    r1: Length,
    p2: &Position,
    v2: &Velocity,
    r2: Length,
) -> Option<Time> {
    let p = p2 - p1;
    let v = v2 - v1;
    let r = r1 + r2;
    let a = v.dot(&v);
    let b = 2.0 * p.dot(&v);
    let c = p.dot(&p) - (r * r);
    let d = (b * b) - 4.0 * a * c;
    if a.abs() < CLOSE_ENOUGH_TO_ZERO || d < CLOSE_ENOUGH_TO_ZERO2 {
        // No collision
        return None;
    }
    let d = d.sqrt();
    let t1 = (-b + d) / (2.0 * a);
    let t2 = (-b - d) / (2.0 * a);
    // We are looking for two distinct, real solutions >= 0.
    // We'll use the smaller of those.
    if t1 < -CLOSE_ENOUGH_TO_ZERO || t2 < -CLOSE_ENOUGH_TO_ZERO {
        None
    } else {
        // The max_f() call is here for the cases where the smallest time value
        // is between zero and -CLOSE_ENOUGH_TO_ZERO. The final answer must be
        // strictly >= 0.0.
        Some(crate::max_f(crate::min_f(t1, t2), 0.0))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::assert_f64_eq;

    #[test]
    fn test_collision_toi_1() {
        let p1 = Position::new(0.0, 0.0);
        let v1 = Velocity::new(0.0, 0.0);
        let r1: Length = 0.1;

        let p2 = Position::new(0.3, 0.0);
        let v2 = Velocity::new(-0.1, 0.0);
        let r2: Length = 0.1;

        let maybe_toi = collision_toi(&p1, &v1, r1, &p2, &v2, r2);
        if let Some(toi) = maybe_toi {
            assert_f64_eq(toi, 1.0);
        } else {
            panic!("There should have been a collision!");
        }
    }
}
