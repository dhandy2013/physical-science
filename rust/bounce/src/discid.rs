//! DiscID type
use std::{convert::TryFrom, num::NonZeroU32};

/// Unique external ID of a Disc.
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
#[repr(transparent)]
pub struct DiscID(NonZeroU32);

impl DiscID {
    /// Return the next DiscID in numeric sequence, wrapping around if
    /// necessary.
    pub(crate) fn next(&self) -> Self {
        let disc_id_num = self.0.get().wrapping_add(1);
        if disc_id_num == 0 {
            // We wrapped around the largest possible object ID number.
            // Start over at one (the default).
            Self::default()
        } else {
            Self(
                NonZeroU32::new(disc_id_num)
                    .expect("next disc ID should have been valid!"),
            )
        }
    }

    /// Return the DiscID representation as an unsigned 32-bit number
    pub fn as_u32(&self) -> u32 {
        self.0.get()
    }
}

impl Default for DiscID {
    fn default() -> Self {
        Self(NonZeroU32::new(1).expect("1 is a valid DiscID!"))
    }
}

impl TryFrom<u32> for DiscID {
    type Error = &'static str;

    fn try_from(value: u32) -> Result<Self, Self::Error> {
        let disc_id_num =
            NonZeroU32::new(value).ok_or("DiscID number cannot be zero")?;
        Ok(Self(disc_id_num))
    }
}

/// Disc marker to help other crates implement custom behavior
#[derive(Debug, Default, Copy, Clone, Eq, PartialEq, Hash)]
#[repr(transparent)]
pub struct DiscTag(u8);

impl DiscTag {
    /// The number of possible tag values, used for array size
    pub const NUM_TAGS: usize = 100;

    /// Turn a tag into an array index
    #[inline(always)]
    pub fn index(&self) -> usize {
        self.0 as usize
    }
}

impl TryFrom<i32> for DiscTag {
    type Error = &'static str;

    fn try_from(value: i32) -> Result<Self, Self::Error> {
        if value < 0 || value >= Self::NUM_TAGS as i32 {
            return Err("DiscTag value must be >=0, < NUM_TAGS (100)");
        }
        Ok(Self(value as u8))
    }
}
