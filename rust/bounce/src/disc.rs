//! Disc - the main moving object being simulated
use crate::collision::{collision_toi, Collision, CollisionEvent};
use crate::discid::{DiscID, DiscTag};
use crate::types::{Length, Mass, Position, Time, Velocity};

#[derive(Clone, Debug)]
pub struct Disc {
    /// Position of disc in universe coordinates, units are meters.
    /// Increasing x is "right", increasing "y" is "up".
    pub(crate) pos: Position,

    /// Velocity of disc in meters per second in x and y dimensions.
    pub(crate) vel: Velocity,

    /// Radius of disc in universe units (meters)
    pub(crate) r: Length,

    /// Mass of disc in kilograms
    pub(crate) m: Mass,

    /// Optional marker on disc to implement custom behavior
    pub(crate) tag: DiscTag,
}

impl Disc {
    /// Create a disc with the given position, velocity, radius, mass, and tag.
    ///
    /// Return Err if the mass `m` is less than or equal to zero.
    /// Return Err if the radius `r` is less than zero.
    pub fn try_new(
        pos: Position,
        vel: Velocity,
        r: Length,
        m: Mass,
        tag: DiscTag,
    ) -> Result<Self, &'static str> {
        if m <= 0.0 {
            return Err("Disc mass must be greater than zero");
        }
        if r < 0.0 {
            return Err("Disc radius must be greater than or equal to zero");
        }
        Ok(Self {
            pos,
            vel,
            r,
            m,
            tag,
        })
    }

    #[inline(always)]
    pub fn pos(&self) -> Position {
        self.pos
    }

    #[inline(always)]
    pub fn x(&self) -> Length {
        self.pos.coords[0]
    }

    #[inline(always)]
    pub fn y(&self) -> Length {
        self.pos.coords[1]
    }

    #[inline(always)]
    pub fn vel(&self) -> Velocity {
        self.vel
    }

    #[inline(always)]
    pub fn vx(&self) -> Length {
        self.vel[0]
    }

    #[inline(always)]
    pub fn vy(&self) -> Length {
        self.vel[1]
    }

    #[inline(always)]
    pub fn r(&self) -> Length {
        self.r
    }

    #[inline(always)]
    pub fn m(&self) -> Mass {
        self.m
    }

    #[inline(always)]
    pub fn tag(&self) -> DiscTag {
        self.tag
    }

    /// Update position of Disc assuming no collisions
    pub(crate) fn free_update(&mut self, delta_t: Time) {
        self.pos += self.vel * delta_t;
    }

    /// Calculate the soonest time >= 0 this disc will collide with another.
    /// Return Some(CollisionEvent(toi, Collision::Disc(disc1_id, disc2_id)))
    /// if the two discs will collide now or in the future, None otherwise.
    pub(crate) fn calc_toi(
        &self,
        this_disc_id: DiscID,
        other_disc_id: DiscID,
        other_disc: &Self,
    ) -> Option<CollisionEvent> {
        collision_toi(
            &self.pos,
            &self.vel,
            self.r(),
            &other_disc.pos,
            &other_disc.vel,
            other_disc.r(),
        )
        .map(|toi| {
            CollisionEvent::new(
                toi,
                Collision::Disc(this_disc_id, other_disc_id),
            )
        })
    }
}
