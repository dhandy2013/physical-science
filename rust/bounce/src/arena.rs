//! Arena - boundaries of simulation
use crate::collision::{Collision, CollisionEvent};
use crate::disc::Disc;
use crate::discid::DiscID;
use crate::types::{Length, Momentum, Position, Time, Velocity};

/// Arena in which discs are confined.
#[derive(Clone, Debug)]
pub struct Arena {
    /// Upper-left corner of arena
    pub(crate) left_top: Position,

    /// Bottom-right corner of arena
    pub(crate) right_bottom: Position,

    /// Accumulated momentum
    pub(crate) momentum: Momentum,
}

impl Arena {
    /// Construct a new Arena.
    /// `left` must be less than `right`,
    /// `bottom` must be less than `top`.
    pub fn try_new(
        left: Length,
        top: Length,
        right: Length,
        bottom: Length,
    ) -> Result<Self, &'static str> {
        if left >= right {
            return Err("left must be less than right");
        }
        if bottom >= top {
            return Err("bottom must be less than top");
        }
        Ok(Self {
            left_top: Position::new(left, top),
            right_bottom: Position::new(right, bottom),
            momentum: Momentum::new(0.0, 0.0),
        })
    }

    /// Convenience method for tests only
    /// PANIC if parameters are incorrect
    #[cfg(test)]
    pub(crate) fn new(
        left: Length,
        top: Length,
        right: Length,
        bottom: Length,
    ) -> Self {
        Self::try_new(left, top, right, bottom).unwrap()
    }

    #[inline(always)]
    pub fn left(&self) -> Length {
        self.left_top.coords[0]
    }

    #[inline(always)]
    pub fn top(&self) -> Length {
        self.left_top.coords[1]
    }

    #[inline(always)]
    pub fn right(&self) -> Length {
        self.right_bottom.coords[0]
    }

    #[inline(always)]
    pub fn bottom(&self) -> Length {
        self.right_bottom.coords[1]
    }

    pub fn width(&self) -> Length {
        self.right() - self.left()
    }

    pub fn height(&self) -> Length {
        self.top() - self.bottom()
    }

    /// Return true iff a shape with center point `pos` and maximum distance
    /// from center to edge `size` is contained within this arena.
    pub fn contains_shape(&self, pos: Position, size: f64) -> bool {
        if pos.x - size < self.left() {
            return false;
        }
        if pos.x + size > self.right() {
            return false;
        }
        if pos.y + size > self.top() {
            return false;
        }
        if pos.y - size < self.bottom() {
            return false;
        }
        return true;
    }

    /// Calculate the next collision between a disc and the arena, if any.
    /// Return None if there is no collision (because the disc is not moving)
    /// Return Some(CollisionEvent::new(toi, Collision::Wall(disc_id, direction))
    /// otherwise.
    /// ``toi``: Time-Of-Impact
    /// ``direction``:
    ///     Velocity(-1, 1) for a horizontal bounce,
    ///     Velocity(1, -1) for a vertical bounce.
    pub(crate) fn calc_toi(
        &self,
        disc_id: DiscID,
        disc: &Disc,
    ) -> Option<CollisionEvent> {
        let mut maybe_toi: Option<Time> = None;
        let mut maybe_direction: Option<Velocity> = None;

        // Bouncing in x dimension
        if disc.vx() != 0.0 {
            maybe_direction = Some(Velocity::new(-1.0, 1.0));
            if disc.vx() < 0.0 {
                // bounce off left wall
                let left_bound = self.left() + disc.r();
                maybe_toi = Some(crate::max_f(
                    (left_bound - disc.x()) / disc.vx(),
                    0.0,
                ));
            } else {
                // bounce off right wall
                let right_bound = self.right() - disc.r();
                maybe_toi = Some(crate::max_f(
                    (right_bound - disc.x()) / disc.vx(),
                    0.0,
                ));
            }
        }

        // Bouncing in y dimension
        if disc.vy() != 0.0 {
            let cur_toi = if disc.vy() < 0.0 {
                // bounce off bottom wall
                let bottom_bound = self.bottom() + disc.r();
                crate::max_f((bottom_bound - disc.y()) / disc.vy(), 0.0)
            } else {
                // bounce off top wall
                let top_bound = self.top() - disc.r();
                crate::max_f((top_bound - disc.y()) / disc.vy(), 0.0)
            };
            if maybe_toi.is_none() || maybe_toi.unwrap() > cur_toi {
                maybe_toi = Some(cur_toi);
                maybe_direction = Some(Velocity::new(1.0, -1.0));
            }
        }

        if let (Some(toi), Some(direction)) = (maybe_toi, maybe_direction) {
            Some(CollisionEvent::new(
                toi,
                Collision::Wall(disc_id, direction),
            ))
        } else {
            None
        }
    }
}

impl Default for Arena {
    fn default() -> Self {
        Self {
            left_top: Position::new(-6.0, 5.0),
            right_bottom: Position::new(6.0, -5.0),
            momentum: Momentum::new(0.0, 0.0),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn arena_construct_happy() {
        Arena::try_new(-4.0, 3.0, 4.0, -3.0).expect("should have been valid!");
    }

    #[test]
    fn arena_construct_unhappy() {
        assert!(Arena::try_new(4.0, 3.0, -4.0, -3.0).is_err());
        assert!(Arena::try_new(-4.0, -3.0, 4.0, 3.0).is_err());
        assert!(Arena::try_new(4.0, 3.0, 4.0, -3.0).is_err());
        assert!(Arena::try_new(-4.0, 3.0, 4.0, 3.0).is_err());
    }

    #[test]
    fn shape_in_arena() {
        let arena = Arena::try_new(0.5, 3.5, 2.5, 1.5).unwrap();
        let pos = Position::new(1.5, 2.0);
        let is_in_arena = arena.contains_shape(pos, 0.5);
        assert!(is_in_arena);
    }

    #[test]
    fn shape_out_of_arena() {
        let arena = Arena::try_new(0.5, 3.5, 2.5, 1.5).unwrap();
        let pos = Position::new(1.5, 2.0);
        assert!(!arena.contains_shape(pos, 1.5));
        let pos = Position::new(-0.5, 2.0);
        assert!(!arena.contains_shape(pos, 1.0));
        let pos = Position::new(2.0, 2.0);
        assert!(!arena.contains_shape(pos, 1.0));
        let pos = Position::new(1.5, 0.5);
        assert!(!arena.contains_shape(pos, 1.0));
        let pos = Position::new(1.5, 1.5);
        assert!(!arena.contains_shape(pos, 1.0));
    }
}
