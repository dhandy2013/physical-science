//! Miscellaneous types to support the bounce simulator
use nalgebra as na;

pub type Length = f64;
pub type Time = f64;
pub type Mass = f64;
pub type Position = na::Point2<Length>;
pub type Velocity = na::Vector2<f64>; // units are Length / Time
pub type Momentum = na::Vector2<f64>; // units are Mass * (Length / Time)
