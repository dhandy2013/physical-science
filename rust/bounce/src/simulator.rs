pub use crate::arena::Arena;
use crate::collision::{
    merge_maybe_events, Collision, CollisionEvent, CLOSE_ENOUGH_TO_ZERO,
};
pub use crate::disc::Disc;
pub use crate::discid::{DiscID, DiscTag};
pub use crate::types::{Length, Mass, Momentum, Position, Time, Velocity};
use indexmap::IndexMap;
use nalgebra as na;
use rustc_hash::FxBuildHasher;

#[derive(Debug, Default)]
pub struct Simulator {
    /// The number of times the `.update()` method has been called.
    frame_count: u32,

    /// Absolute simulator time.
    /// Total accumulated time from all `.update()` delta times.
    time: Time,

    arena: Arena,
    discs: IndexMap<DiscID, Disc, FxBuildHasher>,

    /// ID of most-recently-created Disc, if any.
    /// Used by `.create_disc()` to generate always-increasing DiscIDs.
    most_recent_disc_id: Option<DiscID>,

    /// Current number of Disc objects in simulator that are "collidable",
    /// meaning that any disc can collide with them. Must always be <=
    /// discs.len().
    num_collidable_discs: usize,

    /// Next event that could cause a disc to change velocity
    next_event: Option<CollisionEvent>,

    /// Discs for which the client wants to know about collision events
    collisions_of_interest: IndexMap<DiscID, CollisionCounts, FxBuildHasher>,

    /// The friction half-lives and scale values, by disc tag
    friction: Friction,
}

pub struct DiscIterator<'a> {
    iter: indexmap::map::Iter<'a, DiscID, Disc>,
}

impl<'a> DiscIterator<'a> {
    fn new(iter: indexmap::map::Iter<'a, DiscID, Disc>) -> Self {
        Self { iter }
    }
}

impl<'a> Iterator for DiscIterator<'a> {
    type Item = (DiscID, &'a Disc);
    fn next(&mut self) -> Option<Self::Item> {
        match self.iter.next() {
            Some((&disc_id, disc)) => Some((disc_id, disc)),
            None => None,
        }
    }
}

/// Count of collisions by type
#[derive(Clone, Copy, Debug, Default, PartialEq)]
pub struct CollisionCounts {
    num_collisions_with_wall: usize,
    num_collisions_with_disc: usize,
}

impl CollisionCounts {
    pub fn clear(&mut self) {
        *self = Self::default();
    }
    pub fn num_collisions_with_wall(&self) -> usize {
        self.num_collisions_with_wall
    }
    pub fn num_collisions_with_disc(&self) -> usize {
        self.num_collisions_with_disc
    }
    /// Return the number of collisions of any kind since the last update
    pub fn num_collisions(&self) -> usize {
        self.num_collisions_with_wall + self.num_collisions_with_disc
    }
}

#[derive(Debug)]
struct Friction {
    /// The most recent time step value used to generate friction coefficients.
    /// This is checked in order to determine whether they need to be
    /// re-generated. Set this to 0.0 to force re-generation on next update.
    dt: Time,

    /// The time it takes for friction to reduce the velocity of a disc by half,
    /// indexed by disc tag. None means no friction for that tag.
    half_life_by_tag: [Option<f64>; DiscTag::NUM_TAGS],

    /// The amount by which disc velocity will be scaled down each time step
    /// based on the friction half-life. If there is no friction the scale is
    /// 1.0, otherwise it is a number greater than zero and less than one.
    /// Indexed by disc tag.  This number must be recalculated each time `dt`
    /// changes.
    scale_by_tag: [f64; DiscTag::NUM_TAGS],

    /// Track the number of tags that have a friction half-life other than None
    num_tags_with_friction: usize,
}

impl Default for Friction {
    fn default() -> Self {
        Self {
            dt: 0.0,
            half_life_by_tag: [None; DiscTag::NUM_TAGS],
            scale_by_tag: [1.0; DiscTag::NUM_TAGS],
            num_tags_with_friction: 0,
        }
    }
}

impl Friction {
    /// Set the friction half-life discs with the given tag.
    /// The half-life is the time it takes for friction to reduce the velocity
    /// by one half. `half_life` must be `None` for no friction, otherwise a
    /// number greater than zero. Return Err if half_life is <= 0.0.
    fn set_tag_half_life(
        &mut self,
        tag: DiscTag,
        half_life: Option<f64>,
    ) -> Result<(), &'static str> {
        if half_life.unwrap_or(1.0) <= 0.0 {
            return Err("Friction half-life must be a time > 0.0");
        }
        self.dt = 0.0; // force recalculation of scales on next update
        let prev_half_life = self.half_life_by_tag[tag.index()];
        self.half_life_by_tag[tag.index()] = half_life;
        // Set scale to safe default value. update() will recalculate it.
        self.scale_by_tag[tag.index()] = 1.0;
        if half_life.is_some() {
            if prev_half_life.is_none() {
                self.num_tags_with_friction += 1;
            }
        } else {
            if prev_half_life.is_some() {
                self.num_tags_with_friction -= 1;
            }
        }
        Ok(())
    }

    fn num_tags_with_friction(&self) -> usize {
        self.num_tags_with_friction
    }

    /// Re-calcuate the friction scale values based on `dt` and the tag
    /// half-life values. PANIC if dt <= 0.0.
    fn update(&mut self, dt: f64) {
        assert!(dt > 0.0);
        if dt == self.dt {
            // Re-use existing scale values
            return;
        }
        for i in 0..DiscTag::NUM_TAGS {
            match self.half_life_by_tag[i] {
                Some(half_life) => {
                    self.scale_by_tag[i] =
                        f64::exp(dt * f64::ln(0.5) / half_life);
                }
                None => {
                    self.scale_by_tag[i] = 1.0;
                }
            }
        }
    }

    /// Return the amount to scale the disc velocity given the disc tag
    #[inline(always)]
    fn scale(&self, tag: DiscTag) -> f64 {
        self.scale_by_tag[tag.index()]
    }
}

impl Simulator {
    pub fn new(arena: Arena) -> Self {
        Self {
            arena,
            ..Self::default()
        }
    }

    pub fn arena(&self) -> &Arena {
        &self.arena
    }

    /// Store the given Disc in the Simulator's indexmap of discs.
    ///
    /// If the disc radius is exactly equal to zero then it is a "non-colliding"
    /// disc, meaning it will collide with walls and with discs with radius
    /// greater than zero, but it will not collide with any other disc with
    /// radius zero.
    ///
    /// Return the DiscID used to track this disc during simulation.
    /// Return Err if the disc was outside the arena bounds.
    pub fn create_disc(&mut self, disc: Disc) -> Result<DiscID, &'static str> {
        if !self.arena.contains_shape(disc.pos(), disc.r()) {
            return Err("Cannot create disc outside of simulator arena");
        }
        let id = self.next_disc_id();
        let maybe_prev_disc = if disc.r() == 0.0 {
            // This will insert the disc at the _end_ of the indexmap.
            // Its index will be >= self.num_collidable_discs
            let (index, maybe_prev_disc) = self.discs.insert_full(id, disc);
            assert!(index >= self.num_collidable_discs);
            maybe_prev_disc
        } else {
            let (index, maybe_prev_disc) =
                self.discs
                    .insert_before(self.num_collidable_discs, id, disc);
            assert_eq!(index, self.num_collidable_discs);
            self.num_collidable_discs += 1;
            maybe_prev_disc
        };
        assert!(maybe_prev_disc.is_none()); // assert DiscID is unique
        self.invalidate_predictions();
        Ok(id)
    }

    /// Create a disc using `.create_disc()`. PANIC if the disc is outside of
    /// the arena bounds. Use only in tests.
    #[cfg(test)]
    pub(crate) fn create_disc_or_panic(&mut self, disc: Disc) -> DiscID {
        self.create_disc(disc)
            .expect("disc should have been inside simulator arena")
    }

    /// Compute the next available disc ID
    fn next_disc_id(&mut self) -> DiscID {
        // In practice this loop will not be infinite because even in the case
        // where we create more than 4 billion objects during the lifetime of
        // the simulator, we will run out of memory or other resources before
        // we accumulate 4 billion objects alive at the same time.
        //
        // We could run into a situation where after we have created 4B objects
        // and the object ID has wrapped around, object creation becomes slower
        // because we hit a run of many (> 1K) previous object IDs still in use.
        // I'll deal with that problem if it ever happens.
        let mut disc_id = match self.most_recent_disc_id {
            Some(disc_id) => disc_id.next(),
            None => DiscID::default(),
        };
        loop {
            // Guarantee `disc_id` is unique, even in case of wraparound.
            if !self.discs.contains_key(&disc_id) {
                self.most_recent_disc_id = Some(disc_id);
                return disc_id;
            }
            disc_id = disc_id.next();
        }
    }

    /// Arbitrarily set a new position and velocity on a disc.
    /// Return the old position and velocity, or Err if `disc_id` did not
    /// reference a valid disc or `pos` is outside of the arena bounds.
    pub fn set_disc_pos_vel(
        &mut self,
        disc_id: DiscID,
        pos: Position,
        vel: Velocity,
    ) -> Result<(Position, Velocity), &'static str> {
        let disc = self
            .discs
            .get_mut(&disc_id)
            .ok_or("set_disc_pos_vel(): disc_id is not valid")?;
        if !self.arena.contains_shape(pos, disc.r()) {
            return Err("set_disc_pos_vel(): pos is outside of arena bounds");
        }
        let (old_pos, old_vel) = (disc.pos(), disc.vel());
        disc.pos = pos;
        disc.vel = vel;
        self.invalidate_predictions();
        Ok((old_pos, old_vel))
    }

    /// Launch a new "child" disc from a parent disc.
    ///
    /// The `child_disc` position is ignored and overridden to become the
    /// position of the parent disc. The `child_disc` velocity is the desired
    /// velocity relative to the parent disc, i.e. the launch speed.
    ///
    /// If all goes well, return the ID of the new disc. Return Err if:
    /// - `parent_disc_id` does not reference an existing Disc
    /// - child disc `m` is greater than or equal to mass of parent disc
    /// - there is some other error creating the child disc
    pub fn launch_disc_from_disc(
        &mut self,
        parent_disc_id: DiscID,
        mut child_disc: Disc,
    ) -> Result<DiscID, &'static str> {
        let parent_disc = self
            .discs
            .get_mut(&parent_disc_id)
            .ok_or("Parent disc ID is invalid")?;
        if child_disc.m() >= parent_disc.m() {
            return Err("Child disc mass must be less than parent disc mass");
        }

        child_disc.pos = parent_disc.pos();
        child_disc.vel += parent_disc.vel();

        let new_parent_m = parent_disc.m() - child_disc.m();
        let new_parent_vel = ((parent_disc.m() * parent_disc.vel())
            - (child_disc.m() * child_disc.vel()))
            / new_parent_m;
        parent_disc.m = new_parent_m;
        parent_disc.vel = new_parent_vel;

        self.create_disc(child_disc)
    }

    /// Expel mass `m` from a disc at a velocity `v` relative to the disc.
    ///
    /// This reduces the mass of the disc by `m` and changes the velocity of the
    /// disc in order to conserve total momentum.  This simulates burning rocket
    /// fuel in discrete chunks.
    ///
    /// If all goes well, return the mass of the disc after reduction by `m`.
    /// Return Err if:
    /// - `disc_id` does not reference a valid disc
    /// - `m` is less than zero
    /// - `m` is greater than or equal to the current disc mass
    pub fn expel_mass_from_disc(
        &mut self,
        disc_id: DiscID,
        m: Mass,
        v: Velocity,
    ) -> Result<f64, &'static str> {
        if m < 0.0 {
            return Err("Expelled mass must be >= 0.0");
        }

        let disc = self
            .discs
            .get_mut(&disc_id)
            .ok_or("expel_mass_from_disc(): invalid disc_id")?;
        if m >= disc.m() {
            return Err("Expelled mass must be less than disc mass");
        }
        let expelled_mass_vel = disc.vel() + v;
        let new_disc_m = disc.m() - m;
        disc.vel =
            ((disc.m() * disc.vel()) - (m * expelled_mass_vel)) / new_disc_m;
        disc.m = new_disc_m;

        self.invalidate_predictions();
        Ok(new_disc_m)
    }

    /// Add mass `m` to a disc.
    ///
    /// This simulates things like replenishing fuel. Or, `m` can be negative to
    /// reduce mass for some specialized simulation need. The velocity of the
    /// disc is not changed.
    ///
    /// If all goes well, return the mass of the disc after adjustment.
    /// Return Err if:
    /// - `disc_id` does not reference a valid disc
    /// - The adjusted mass would be less than or equal to zero.
    pub fn add_disc_mass(
        &mut self,
        disc_id: DiscID,
        m: Mass,
    ) -> Result<f64, &'static str> {
        let disc = self
            .discs
            .get_mut(&disc_id)
            .ok_or("add_disc_mass(): invalid disc_id")?;
        let new_disc_m = disc.m() + m;
        if new_disc_m <= 0.0 {
            return Err("add_disc_mass(): final disc mass must be > 0.0");
        }
        disc.m = new_disc_m;
        // Invalidating predictions is not necessary because no velocity was
        // changed.
        // self.invalidate_predictions();
        Ok(new_disc_m)
    }

    /// Remove a Disc from the simulator. If `disc_id` is the ID of an existing
    /// Disc, return it. Otherwise return `None`.
    ///
    /// With the current design, removing collidable discs (discs with radius >
    /// 0) is slower than removing non-collidable discs. It is anticipated that
    /// collidable discs are mostly permanent and non-collidable discs are
    /// mostly ephemeral.
    pub fn remove_disc(&mut self, disc_id: DiscID) -> Option<Disc> {
        // When removing a disc, be careful to preserve the boundary between
        // collidable and non-collidable discs.
        let index = self.discs.get_index_of(&disc_id)?;
        self.invalidate_predictions();
        self.unsubscribe_disc_collisions(disc_id);
        if index < self.num_collidable_discs {
            // This is a collidable disc. Preserve order and maintain the count.
            self.num_collidable_discs -= 1;
            self.discs.shift_remove(&disc_id)
        } else {
            self.discs.swap_remove(&disc_id)
        }
    }

    pub fn get_disc_by_id(&self, disc_id: DiscID) -> Option<&Disc> {
        self.discs.get(&disc_id)
    }

    pub fn iter_discs(&self) -> DiscIterator {
        DiscIterator::new(self.discs.iter())
    }

    /// Total number of times update() has been called
    pub fn frame_count(&self) -> u32 {
        self.frame_count
    }

    /// Total elapsed simulation time in seconds
    pub fn time(&self) -> Time {
        self.time
    }

    /// Update the state of the simulator based on the laws of physics and the
    /// passage of an incremental amount of time.
    ///
    /// `delta_t`: Time step in seconds
    ///
    /// Return Err if `delta_t` is less than or equal to zero.
    pub fn update(&mut self, delta_t: Time) -> Result<(), &'static str> {
        // Make sure `delta_t` parameter is sane
        if delta_t <= 0.0 {
            return Err("delta_t must be > 0.0");
        }

        // Clear recorded collisions left over from previous frame
        for (_disc_id, collisions) in self.collisions_of_interest.iter_mut() {
            collisions.clear();
        }

        // self.next_event will be None on first update of system or if
        // next_event got invalidated because system objects were added,
        // changed, or removed.
        let mut maybe_cur_event =
            self.next_event.or_else(|| self.calc_next_event());
        let mut cur_delta_t = delta_t;
        while cur_delta_t > 0.0 {
            while let Some(event) = maybe_cur_event {
                if event.toi <= cur_delta_t {
                    self.free_update(event.toi);
                    self.apply_collision(event.collision);
                    maybe_cur_event = self.calc_next_event();
                    cur_delta_t -= event.toi;
                } else {
                    self.free_update(cur_delta_t);
                    maybe_cur_event =
                        Some(event.replace_toi(event.toi - cur_delta_t));
                    cur_delta_t = 0.0;
                    break;
                }
            }
            self.free_update(cur_delta_t);
            cur_delta_t = 0.0;
        }
        self.next_event = maybe_cur_event;

        if self.friction.num_tags_with_friction() > 0 {
            self.friction.update(delta_t);
            let mut accum_scale = 0.0;
            for (_disc_id, disc) in self.discs.iter_mut() {
                let scale = self.friction.scale(disc.tag());
                disc.vel *= scale;
                accum_scale += scale;
            }
            if accum_scale != self.discs.len() as f64 {
                // If each scale value applied was exactly 1.0 there was no
                // friction.  If it was anything else, there was some friction
                // and predictions are now invalid.
                self.invalidate_predictions();
            }
        }

        self.frame_count += 1;
        self.time += delta_t;
        Ok(())
    }

    /// Record any collisions involving this disc.
    pub fn subscribe_disc_collisions(&mut self, disc_id: DiscID) {
        self.collisions_of_interest
            .insert(disc_id, CollisionCounts::default());
    }

    /// Stop recording collisions involving this disc.
    pub fn unsubscribe_disc_collisions(&mut self, disc_id: DiscID) {
        self.collisions_of_interest.swap_remove(&disc_id);
    }

    /// Return a struct containing counts of collisions involving this disc by
    /// type of collision (with wall, with other disc). The collisions event
    /// records are cleared at the start of update on every frame.
    ///
    /// If no Disc with ID `disc_id` exists, or if no subscription was made for
    /// that disc, then return CollisionCounts with all zeros.
    pub fn count_disc_collisions(&self, disc_id: DiscID) -> CollisionCounts {
        self.collisions_of_interest
            .get(&disc_id)
            .map(|counts| counts.clone())
            .unwrap_or_default()
    }

    /// Return the number of discs that are subscribed to receive collision
    /// statistics via `.subscribe_disc_collisions()`. This method is for
    /// diagnostic and debug purposes, to make sure we aren't leaking memory nor
    /// slowing updates by accumulating forgotten subscriptions.
    pub fn num_disc_collision_subscriptions(&self) -> usize {
        self.collisions_of_interest.len()
    }

    /// Clear any pre-calculated events from the previous call to update,
    /// so that when the next call to update runs it will start fresh.
    /// Call this method whenever objects are added or removed or their
    /// positions, velocities, masses, or sizes are changed.
    fn invalidate_predictions(&mut self) {
        self.next_event = None;
    }

    fn apply_collision(&mut self, collision: Collision) {
        match collision {
            Collision::Wall(disc_id, direction) => {
                let disc = self
                    .discs
                    .get_mut(&disc_id)
                    .expect("disc_id had better be valid!");
                // Bounce disc off of solid wall
                disc.vel.component_mul_assign(&direction);
                // Wall absorbs momentum too!
                // momentum -= 2 * m * v
                // direction is either [1, -1] or [-1, 1]
                self.arena.momentum += disc.m
                    * (disc.vel.component_mul(
                        &(direction + Velocity::new(-1.0, -1.0)),
                    ));
                // Notify the subscriber (if any) of the collision
                self.collisions_of_interest.get_mut(&disc_id).and_then(
                    |collision_counts| {
                        collision_counts.num_collisions_with_wall += 1;
                        Some(())
                    },
                );
            }
            Collision::Disc(disc1_id, disc2_id) => {
                let (index1, _, disc1) = self
                    .discs
                    .get_full(&disc1_id)
                    .expect("disc1_id had better be valid!");
                let (index2, _, disc2) = self
                    .discs
                    .get_full(&disc2_id)
                    .expect("disc2_id had better be valid!");
                // Perfectly elastic collision between two discs
                let (p1, v1, m1) = (disc1.pos, disc1.vel, disc1.m);
                let (p2, v2, m2) = (disc2.pos, disc2.vel, disc2.m);
                let m = m1 + m2;
                let mut p = p1 - p2;
                let mut n = (v1 - v2).dot(&p);
                let mut d = p.magnitude_squared();
                if d < CLOSE_ENOUGH_TO_ZERO {
                    // Oops, too close! Pick an arbitrary bounce direction.
                    // It's either that or panic.
                    p = na::Vector2::<Length>::new(1.0, 0.0);
                    n = (v1 - v2).dot(&p);
                    d = p.magnitude_squared();
                }
                self.discs[index1].vel -= ((2.0 * m2) / m) * (n / d) * p;
                self.discs[index2].vel += ((2.0 * m1) / m) * (n / d) * p;
                // Notify subscribers (if any) of the collision
                for disc_id in [disc1_id, disc2_id] {
                    self.collisions_of_interest.get_mut(&disc_id).and_then(
                        |collision_counts| {
                            collision_counts.num_collisions_with_disc += 1;
                            Some(())
                        },
                    );
                }
            }
        }
    }

    /// Execute the between-collisions motion of all objects
    fn free_update(&mut self, delta_t: Time) {
        if delta_t <= 0.0 {
            return;
        }
        for (_, disc) in self.discs.iter_mut() {
            disc.free_update(delta_t);
        }
    }

    fn calc_next_event(&self) -> Option<CollisionEvent> {
        let mut maybe_event: Option<CollisionEvent> = None;

        // Calculate collisions of collidable objects with the arena walls
        // or with other collidable objects.
        let n = self.num_collidable_discs;
        for (i, (&disc_id, disc)) in self.discs[0..n].iter().enumerate() {
            maybe_event = merge_maybe_events(
                self.arena.calc_toi(disc_id, disc),
                maybe_event,
            );
            for j in (i + 1)..n {
                let (&other_disc_id, other_disc) = self
                    .discs
                    .get_index(j)
                    .expect("index j had better be valid!");
                maybe_event = merge_maybe_events(
                    disc.calc_toi(disc_id, other_disc_id, other_disc),
                    maybe_event,
                );
            }
        }

        // Calculate collisions of non-collidable objects with the arena walls
        // or with collidable objects, but not with non-collidable objects.
        for (&disc_id, disc) in self.discs[n..].iter() {
            maybe_event = merge_maybe_events(
                self.arena.calc_toi(disc_id, disc),
                maybe_event,
            );
            for j in 0..n {
                let (&other_disc_id, other_disc) = self
                    .discs
                    .get_index(j)
                    .expect("index j had better be valid!");
                maybe_event = merge_maybe_events(
                    disc.calc_toi(disc_id, other_disc_id, other_disc),
                    maybe_event,
                );
            }
        }

        maybe_event
    }

    /// Return (disc_id, disc) pair at index `index` if index is in range
    /// 0..num_discs(), otherwise return `None`.
    ///
    /// The order of discs is an implementation detail and changes dynamically.
    /// This method mainly exists to implement read-only iteration from Python.
    pub fn get_id_disc_by_index(&self, index: usize) -> Option<(DiscID, Disc)> {
        self.discs
            .get_index(index)
            .map(|(&disc_id, disc)| (disc_id, disc.clone()))
    }

    pub fn num_discs(&self) -> usize {
        self.discs.len()
    }

    pub fn num_collidable_discs(&self) -> usize {
        self.num_collidable_discs
    }

    /// Set the friction half-life for all discs with the given tag.
    ///
    /// If `half_life` is `None` then the disc is frictionless.  Otherwise,
    /// `half_life` is the time in seconds it will take for frictional forces to
    /// reduce the disc velocity by half. Return Err if `half_life` is <= 0.0.
    pub fn set_disc_tag_friction_half_life(
        &mut self,
        tag: DiscTag,
        half_life: Option<f64>,
    ) -> Result<(), &'static str> {
        self.friction.set_tag_half_life(tag, half_life)
    }

    /// Return the number of disc tags with frictional half-lives set (in other
    /// words, that are not frictionless.)
    pub fn num_tags_with_friction(&self) -> usize {
        self.friction.num_tags_with_friction()
    }
}

#[cfg(test)]
mod tests {
    use std::convert::TryFrom;

    use super::*;
    use crate::assert_f64_eq;

    #[test]
    fn test_basic_sim() {
        let mut sim = Simulator::default();
        let disc_id = sim.create_disc_or_panic(
            Disc::try_new(
                Position::new(0.0, 0.0),
                Velocity::new(1.5, 1.0),
                0.25, // radius in meters
                1.0,  // mass in kilograms
                DiscTag::default(),
            )
            .unwrap(),
        );

        // Check disc's initial conditions
        {
            let disc = sim.get_disc_by_id(disc_id).unwrap();
            assert_f64_eq(disc.x(), 0.0);
            assert_f64_eq(disc.y(), 0.0);
            assert_f64_eq(disc.r(), 0.25);
            assert_f64_eq(disc.vx(), 1.5);
            assert_f64_eq(disc.vy(), 1.0);
        }

        // Do a time step and check the number of collisions.
        // The following bounces occur during this time step:
        // 1. Bounce off right wall     3.833333 seconds after start
        // 2. Bounce off top wall       0.916667 seconds later
        // 3. Bounce off left wall      6.75     seconds later
        //                             -----
        //                             11.50     total seconds
        sim.subscribe_disc_collisions(disc_id);
        sim.update(11.50).unwrap();
        assert_eq!(
            sim.count_disc_collisions(disc_id)
                .num_collisions_with_wall(),
            3
        );

        // Do a time step and check the final location.
        // The following bounces occur during this time step
        // 4. Bounce off bottom wall    2.75     seconds later
        // 5. Travel a little farther   1.00     seconds later
        //                             -----
        //                              3.75     total seconds
        sim.unsubscribe_disc_collisions(disc_id);
        sim.update(3.75).unwrap();
        assert_eq!(
            sim.count_disc_collisions(disc_id)
                .num_collisions_with_wall(),
            0 // there were 1 wall collision, but we unsubscribed
        );

        // Check final position and velocity of disc
        {
            let disc = &sim.get_disc_by_id(disc_id).unwrap();
            assert_f64_eq(disc.x(), -0.125);
            assert_f64_eq(disc.y(), -3.75);
            assert_f64_eq(disc.vx(), 1.5);
            assert_f64_eq(disc.vy(), 1.0);
        }
    }

    #[test]
    fn test_corner_bounce() {
        let arena = Arena::new(-1.0, 0.7, 1.0, -0.7);
        let mut sim = Simulator::new(arena);
        let disc_id = sim.create_disc_or_panic(
            Disc::try_new(
                Position::new(-0.8, 0.5),
                Velocity::new(-0.15, 0.15),
                0.05,
                1.0,
                DiscTag::default(),
            )
            .unwrap(),
        );
        sim.subscribe_disc_collisions(disc_id);

        // Run simulator long enough for disc to bounce off top left corner
        // pocket and travel back past the place it started.
        sim.update(4.0).unwrap();

        // Check position and velocity of disc
        {
            let disc = &mut sim.get_disc_by_id(disc_id).unwrap();
            assert_f64_eq(disc.x(), -0.5);
            assert_f64_eq(disc.y(), 0.2);
            assert_f64_eq(disc.vx(), 0.15);
            assert_f64_eq(disc.vy(), -0.15);
        }

        // Check number of collisions
        let collision_counts = sim.count_disc_collisions(disc_id);
        assert_eq!(collision_counts.num_collisions_with_wall(), 2);
        assert_eq!(collision_counts.num_collisions_with_disc(), 0);
    }

    #[test]
    fn test_two_discs_equal_mass_collide() {
        let arena = Arena::new(-0.5, 0.5, 0.5, -0.5);
        let mut sim = Simulator::new(arena);
        let disc1_id = sim.create_disc_or_panic(
            Disc::try_new(
                Position::new(0.0, 0.0),
                Velocity::new(0.0, 0.0),
                0.1, // meters
                1.0, // kilograms
                DiscTag::default(),
            )
            .unwrap(),
        );
        let disc2_id = sim.create_disc_or_panic(
            Disc::try_new(
                Position::new(0.3, 0.0),
                Velocity::new(-0.1, 0.0),
                0.1, // meters
                1.0, // kilograms
                DiscTag::default(),
            )
            .unwrap(),
        );

        // disc2 should travel 0.1 meters left and make contact with disc1 at t=1
        // disc2 should come to a halt, disc 1 should begin leftward motion
        sim.update(2.0).unwrap();

        {
            let disc1 = &sim.get_disc_by_id(disc1_id).unwrap();
            let disc2 = &sim.get_disc_by_id(disc2_id).unwrap();
            assert_f64_eq(disc2.x(), 0.2);
            assert_f64_eq(disc2.vx(), 0.0);
            assert_f64_eq(disc1.vx(), -0.1);
            assert_f64_eq(disc1.x(), -0.1);
        }
    }

    #[test]
    fn test_collidable_and_non_collidable_discs() {
        let arena = Arena::new(-0.5, 0.5, 0.5, -0.5);
        let mut sim = Simulator::new(arena);
        let disc0_id = sim.create_disc_or_panic(
            Disc::try_new(
                Position::new(-0.25, 0.0),
                Velocity::new(0.5, 0.0),
                0.0, // radius of zero meters means non-collidable
                1.0, // kilograms
                DiscTag::default(),
            )
            .unwrap(),
        );
        sim.subscribe_disc_collisions(disc0_id);
        let disc1_id = sim.create_disc_or_panic(
            Disc::try_new(
                Position::new(0.0, 0.0),
                Velocity::new(0.0, 0.0),
                0.0, // radius of zero meters means non-collidable
                1.0, // kilograms
                DiscTag::default(),
            )
            .unwrap(),
        );
        sim.subscribe_disc_collisions(disc1_id);
        let disc2_id = sim.create_disc_or_panic(
            Disc::try_new(
                Position::new(0.25, 0.0),
                Velocity::new(0.0, 0.0),
                0.1, // meters
                1.0, // kilograms
                DiscTag::default(),
            )
            .unwrap(),
        );
        sim.subscribe_disc_collisions(disc2_id);

        // In 1 second disc0 should pass through disc1 and hit disc2
        sim.update(1.0).unwrap();

        assert_eq!(
            sim.count_disc_collisions(disc0_id)
                .num_collisions_with_disc(),
            1
        );
        assert_eq!(
            sim.count_disc_collisions(disc1_id)
                .num_collisions_with_disc(),
            0
        );
        assert_eq!(
            sim.count_disc_collisions(disc2_id)
                .num_collisions_with_disc(),
            1
        );
    }

    #[test]
    fn try_na() {
        let p1 = Position::new(-0.5, -0.12);
        let p2 = Position::new(1.9, 0.35);
        let v1 = Velocity::new(0.9, 0.8);
        let v2 = Velocity::new(0.75, -0.25);
        let col_vector = na::Vector2::<Length>::new(1.0, -1.0);
        let row_vector = na::RowVector2::<Length>::new(1.0, -1.0);
        let t: Time = 0.1;
        println!("p2 - p1 = {:?}", p2 - p1);
        println!("v2 + v1 = {:?}", v2 + v1);
        println!("p1 + (v1 * t) = {:?}", p1 + (v1 * t));
        println!(
            "v2 = {}, row_vector = {}, col_vector = {}",
            v2, row_vector, col_vector
        );
        println!("v2 * row_vector = {}", v2 * row_vector);
        println!("row_vector * v2 = {}", row_vector * v2);
        println!(
            "v2.component_mul(&col_vector) = {}",
            v2.component_mul(&col_vector)
        );
    }

    fn make_simulator() -> Simulator {
        let arena = Arena::new(-1.3, 1.2, 1.1, -1.0);
        Simulator::new(arena)
    }

    #[test]
    fn test_calc_toi_arena_right() {
        // Calculate Time-Of-Impact between arena and a disc moving right
        let mut sim = make_simulator();
        let disc_id = sim.create_disc_or_panic(
            Disc::try_new(
                Position::new(0.0, 0.0),
                Velocity::new(2.5, 0.3),
                0.1,
                1.0,
                DiscTag::default(),
            )
            .unwrap(),
        );
        let disc = &sim.get_disc_by_id(disc_id).unwrap();
        if let Some(CollisionEvent {
            toi,
            collision: Collision::Wall(_disc_id, direction),
        }) = sim.arena.calc_toi(disc_id, &disc)
        {
            assert_f64_eq(toi, 0.4);
            assert_eq!(direction, Velocity::new(-1.0, 1.0));
        } else {
            panic!("Should have collided!");
        }
    }

    #[test]
    fn test_calc_toi_arena_left() {
        // Calculate Time-Of-Impact between arena and a disc moving left
        let mut sim = make_simulator();
        let disc_id = sim.create_disc_or_panic(
            Disc::try_new(
                Position::new(0.0, 0.0),
                Velocity::new(-2.5, 0.3),
                0.1,
                1.0,
                DiscTag::default(),
            )
            .unwrap(),
        );
        let disc = &sim.get_disc_by_id(disc_id).unwrap();
        if let Some(CollisionEvent {
            toi,
            collision: Collision::Wall(_disc_id, direction),
        }) = sim.arena.calc_toi(disc_id, &disc)
        {
            assert_f64_eq(toi, 0.48);
            assert_eq!(direction, Velocity::new(-1.0, 1.0));
        } else {
            panic!("Should have collided!");
        }
    }

    #[test]
    fn test_calc_toi_arena_up() {
        // Calculate Time-Of-Impact between arena and a disc moving up
        let mut sim = make_simulator();
        let disc_id = sim.create_disc_or_panic(
            Disc::try_new(
                Position::new(0.0, 0.0),
                Velocity::new(0.6, 2.5),
                0.1,
                1.0,
                DiscTag::default(),
            )
            .unwrap(),
        );
        let disc = &sim.get_disc_by_id(disc_id).unwrap();
        if let Some(CollisionEvent {
            toi,
            collision: Collision::Wall(_disc_id, direction),
        }) = sim.arena.calc_toi(disc_id, &disc)
        {
            assert_f64_eq(toi, 0.44);
            assert_eq!(direction, Velocity::new(1.0, -1.0));
        } else {
            panic!("Should have collided!");
        }
    }

    #[test]
    fn test_calc_toi_arena_down() {
        // Calculate Time-Of-Impact between arena and a disc moving down
        let mut sim = make_simulator();
        let disc_id = sim.create_disc_or_panic(
            Disc::try_new(
                Position::new(0.0, 0.0),
                Velocity::new(0.6, -2.5),
                0.1,
                1.0,
                DiscTag::default(),
            )
            .unwrap(),
        );
        let disc = sim.get_disc_by_id(disc_id).unwrap();
        if let Some(CollisionEvent {
            toi,
            collision: Collision::Wall(_disc_id, direction),
        }) = sim.arena.calc_toi(disc_id, &disc)
        {
            assert_f64_eq(toi, 0.36);
            assert_eq!(direction, Velocity::new(1.0, -1.0));
        } else {
            panic!("Should have collided!");
        }
    }

    #[test]
    fn test_calc_toi_arena_nomove() {
        // There should be no Time-Of-Impact when the disc isn't moving.
        let mut sim = make_simulator();
        let disc_id = sim.create_disc_or_panic(
            Disc::try_new(
                Position::new(0.0, 0.0),
                Velocity::new(0.0, 0.0),
                0.1,
                1.0,
                DiscTag::default(),
            )
            .unwrap(),
        );
        let disc = sim.get_disc_by_id(disc_id).unwrap();
        if let Some(CollisionEvent {
            toi: _toi,
            collision: Collision::Wall(_disc_id, _direction),
        }) = sim.arena.calc_toi(disc_id, &disc)
        {
            panic!("Should not have collided!");
        }
    }

    #[test]
    fn test_threeway_collision() {
        const PI: f64 = std::f64::consts::PI;

        let mut sim = Simulator::new(Arena::new(
            -0.5, // left
            0.7,  // top
            0.5,  // right
            -0.7, // bottom
        ));
        let s = 2.0; // speed scale
        let v2 = 0.3156854249492381 * s;
        let v1 = v2 * 0.25;
        let a = PI / 4.0;
        let disc0_id = sim.create_disc_or_panic(
            Disc::try_new(
                Position::new(0.0, 0.0),
                Velocity::new(0.0, 0.0),
                0.2, // radius in meters
                1.0, // mass in kilograms
                DiscTag::default(),
            )
            .unwrap(),
        );
        sim.subscribe_disc_collisions(disc0_id);
        let disc1_id = sim.create_disc_or_panic(
            Disc::try_new(
                Position::new(-0.26793786053180507, -0.26793786053180507),
                Velocity::new(v1 * a.cos(), v1 * a.sin()),
                0.1,  // radius in meters
                0.25, // mass in kilograms
                DiscTag::default(),
            )
            .unwrap(),
        );
        sim.subscribe_disc_collisions(disc1_id);
        let disc2_id = sim.create_disc_or_panic(
            Disc::try_new(
                Position::new(0.4, 0.4),
                Velocity::new(-v2 * a.cos(), -v2 * a.sin()),
                0.05,   // radius in meters
                0.0625, // mass in kilograms
                DiscTag::default(),
            )
            .unwrap(),
        );
        // Do _not_ subscribe to disc2 collisions.

        // In the current scenario, objects 1 and 2 should both collide with
        // object 0 at time t=0.5, and then rebound in opposite directions.
        // In a bug in previous code, object 1 failed to collide with object 0.

        let (start_vx1, start_vy1, m1, start_vx2, start_vy2, m2) = {
            let disc1 = sim.get_disc_by_id(disc1_id).unwrap();
            let disc2 = sim.get_disc_by_id(disc2_id).unwrap();
            (
                disc1.vx(),
                disc1.vy(),
                disc1.m(),
                disc2.vx(),
                disc2.vy(),
                disc2.m(),
            )
        };

        // Object 0 has zero momentum.
        // Objects 1 and 2 should have equal and opposite momentum.
        assert_f64_eq(start_vx1 * m1 + start_vx2 * m2, 0.0);
        assert_f64_eq(start_vy1 * m1 + start_vy2 * m2, 0.0);

        // Object 2 is traveling down and to the left
        assert!(start_vx2 < 0.0);
        assert!(start_vy2 < 0.0);

        // Object 1 is traveling up and to the right
        assert!(start_vx1 > 0.0);
        assert!(start_vy1 > 0.0);

        // Previous buggy code would fail with n = 5
        let n = 5;
        for i in 0..n {
            sim.update(1.0 / n as f64).unwrap();
            if i == 2 {
                // The collisions all happen during the third update (i == 2).
                assert_eq!(
                    sim.count_disc_collisions(disc0_id)
                        .num_collisions_with_wall(),
                    0
                );
                assert_eq!(
                    sim.count_disc_collisions(disc0_id)
                        .num_collisions_with_disc(),
                    2
                );
                assert_eq!(
                    sim.count_disc_collisions(disc1_id)
                        .num_collisions_with_wall(),
                    0
                );
                assert_eq!(
                    sim.count_disc_collisions(disc1_id)
                        .num_collisions_with_disc(),
                    1
                );
                // We didn't subscribe to disc2 collisions, so we had better
                // not get any collision counts for disc2.
                assert_eq!(
                    sim.count_disc_collisions(disc2_id),
                    CollisionCounts::default()
                );
            } else {
                // No collisions during the other updates
                assert_eq!(
                    sim.count_disc_collisions(disc0_id),
                    CollisionCounts::default()
                );
                assert_eq!(
                    sim.count_disc_collisions(disc1_id),
                    CollisionCounts::default()
                );
                assert_eq!(
                    sim.count_disc_collisions(disc2_id),
                    CollisionCounts::default()
                );
            }
        }

        let (end_vx0, end_vy0, m0, end_vx1, end_vy1, end_vx2, end_vy2) = {
            let disc0 = sim.get_disc_by_id(disc0_id).unwrap();
            let disc1 = sim.get_disc_by_id(disc1_id).unwrap();
            let disc2 = sim.get_disc_by_id(disc2_id).unwrap();
            (
                disc0.vx(),
                disc0.vy(),
                disc0.m(),
                disc1.vx(),
                disc1.vy(),
                disc2.vx(),
                disc2.vy(),
            )
        };

        // The sum of all momemtums (momenta?) should still be zero.
        assert_f64_eq(end_vx0 * m0 + end_vx1 * m1 + end_vx2 * m2, 0.0);
        assert_f64_eq(end_vy0 * m0 + end_vy1 * m1 + end_vy2 * m2, 0.0);

        // Object 2 should have bounced back in opposite direction
        assert!(end_vx2 > 0.0);
        assert!(end_vy2 > 0.0);

        // Object 1 should have bounced back in opposite direction
        assert!(end_vx1 < 0.0);
        assert!(end_vy1 < 0.0);
    }

    #[test]
    fn test_launch_disc_from_disc() {
        let mut sim = Simulator::new(Arena::new(
            -10.0, // left
            10.0,  // top
            10.0,  // right
            -10.0, // bottom
        ));

        let disc0_id = sim.create_disc_or_panic(
            Disc::try_new(
                Position::new(-0.25, 0.25),
                Velocity::new(0.4, 0.3),
                0.1, // radius in meters
                1.0, // mass in kilograms
                DiscTag::default(),
            )
            .unwrap(),
        );

        // Step 0.1 seconds
        sim.update(0.1).unwrap();
        let launch_pos = {
            let disc0 = sim.get_disc_by_id(disc0_id).unwrap();
            assert_f64_eq(disc0.x(), -0.25 + 0.04);
            assert_f64_eq(disc0.y(), 0.25 + 0.03);
            Position::new(disc0.x(), disc0.y())
        };

        // Launch a projectile
        let dm = 0.01; // mass of projectile (1% of original disc)
        let r = 0.0; // radius of projectile (zero means non-collilding)
        let v = Velocity::new(40.0, 30.0); // relative velocity of projectile
        let disc1_id = sim
            .launch_disc_from_disc(
                disc0_id,
                Disc::try_new(
                    Position::new(0.0, 0.0), // ignored, will be set to parent pos
                    v,  // child velocity relative to parent (launch speed)
                    r,  // child disc radius
                    dm, // child disc mass
                    DiscTag::default(), // child disc tag
                )
                .unwrap(),
            )
            .unwrap();
        {
            let disc0 = sim.get_disc_by_id(disc0_id).unwrap();
            assert_f64_eq(disc0.x(), launch_pos.x);
            assert_f64_eq(disc0.y(), launch_pos.y);
            assert_f64_eq(disc0.m(), 0.99);
            let v0: f64 = -0.0050505050505; // speed of original disc
            assert_f64_eq(disc0.vx(), v0 * (4.0 / 5.0));
            assert_f64_eq(disc0.vy(), v0 * (3.0 / 5.0));

            let disc1 = sim.get_disc_by_id(disc1_id).unwrap();
            assert_f64_eq(disc1.x(), launch_pos.x);
            assert_f64_eq(disc1.y(), launch_pos.y);
            assert_f64_eq(disc1.m(), dm);
            let v1: f64 = 50.5; // speed of projectile
            assert_f64_eq(disc1.vx(), v1 * (4.0 / 5.0));
            assert_f64_eq(disc1.vy(), v1 * (3.0 / 5.0));
        }

        // Step 0.1 seconds
        sim.subscribe_disc_collisions(disc1_id);
        sim.update(0.1).unwrap();
        let collisions = sim.count_disc_collisions(disc1_id);
        assert_eq!(collisions.num_collisions_with_wall(), 0);
        assert_eq!(collisions.num_collisions_with_disc(), 0);
        {
            let disc0 = sim.get_disc_by_id(disc0_id).unwrap();
            assert_f64_eq(disc0.x(), launch_pos.x + disc0.vx() * 0.1);
            assert_f64_eq(disc0.y(), launch_pos.y + disc0.vy() * 0.1);

            let disc1 = sim.get_disc_by_id(disc1_id).unwrap();
            assert_f64_eq(disc1.x(), launch_pos.x + disc1.vx() * 0.1);
            assert_f64_eq(disc1.y(), launch_pos.y + disc1.vy() * 0.1);
        }
    }

    #[test]
    fn test_remove_disc() {
        let mut sim = Simulator::new(Arena::new(
            -1.0, // left
            1.0,  // top
            1.0,  // right
            -1.0, // bottom
        ));

        let disc0_id = sim.create_disc_or_panic(
            Disc::try_new(
                Position::new(-0.25, 0.25),
                Velocity::new(0.4, 0.3),
                0.1, // radius in meters
                1.0, // mass in kilograms
                DiscTag::default(),
            )
            .unwrap(),
        );

        let disc1_id = sim.create_disc_or_panic(
            Disc::try_new(
                Position::new(0.25, -0.25),
                Velocity::new(-0.5, 0.0),
                0.0, // radius in meters (zero means non-collidable)
                1.0, // mass in kilograms
                DiscTag::default(),
            )
            .unwrap(),
        );

        assert_eq!(sim.num_collidable_discs(), 1);
        assert_eq!(sim.num_discs(), 2);

        let disc0 = sim.remove_disc(disc0_id).unwrap();
        assert_f64_eq(disc0.r(), 0.1);
        assert_eq!(sim.num_collidable_discs(), 0);
        assert_eq!(sim.num_discs(), 1);

        // Removing same disc again should always return None
        let should_be_none = sim.remove_disc(disc0_id);
        assert!(should_be_none.is_none());

        let disc1 = sim.remove_disc(disc1_id).unwrap();
        assert_f64_eq(disc1.r(), 0.0);
        assert_eq!(sim.num_collidable_discs(), 0);
        assert_eq!(sim.num_discs(), 0);
    }

    #[test]
    fn test_expel_mass_from_disc() {
        let mut sim = Simulator::new(Arena::new(
            -10.0, // left
            10.0,  // top
            10.0,  // right
            -10.0, // bottom
        ));

        let disc0_id = sim.create_disc_or_panic(
            Disc::try_new(
                Position::new(0.0, 0.0),
                Velocity::new(0.4, 0.3),
                0.1, // radius in meters
                1.0, // mass in kilograms
                DiscTag::default(),
            )
            .unwrap(),
        );

        // Expel some mass
        let dm = 0.01; // mass to be ejected (1% of original disc)
        let v = Velocity::new(40.0, 30.0); // relative velocity of exhaust
        let new_m = sim.expel_mass_from_disc(disc0_id, dm, v).unwrap();
        let expected_new_m = {
            let disc0 = sim.get_disc_by_id(disc0_id).unwrap();
            assert_f64_eq(disc0.x(), 0.0);
            assert_f64_eq(disc0.y(), 0.0);
            assert_f64_eq(disc0.m(), 0.99);
            let v0: f64 = -0.0050505050505; // speed of original disc
            assert_f64_eq(disc0.vx(), v0 * (4.0 / 5.0));
            assert_f64_eq(disc0.vy(), v0 * (3.0 / 5.0));
            disc0.m()
        };
        assert_f64_eq(new_m, expected_new_m);
    }

    #[test]
    fn test_add_disc_mass() {
        let mut sim = Simulator::new(Arena::new(
            -1.0, // left
            1.0,  // top
            1.0,  // right
            -1.0, // bottom
        ));

        let disc_id = sim.create_disc_or_panic(
            Disc::try_new(
                Position::new(-0.25, 0.25),
                Velocity::new(0.4, 0.3),
                0.1, // radius in meters
                1.0, // mass in kilograms
                DiscTag::default(),
            )
            .unwrap(),
        );

        let new_m = sim.add_disc_mass(disc_id, 0.5).unwrap();
        assert_eq!(new_m, 1.5);

        let new_m = sim.add_disc_mass(disc_id, -0.75).unwrap();
        assert_eq!(new_m, 0.75);

        let maybe_new_m = sim.add_disc_mass(disc_id, -0.8);
        assert!(maybe_new_m.is_err());
    }

    #[test]
    fn test_set_disc_pos_vel() {
        let mut sim = Simulator::new(Arena::new(
            -1.0, // left
            1.0,  // top
            1.0,  // right
            -1.0, // bottom
        ));

        let disc_id = sim.create_disc_or_panic(
            Disc::try_new(
                Position::new(-0.25, 0.25),
                Velocity::new(0.4, 0.3),
                0.1, // radius in meters
                1.0, // mass in kilograms
                DiscTag::default(),
            )
            .unwrap(),
        );

        // Setting disc position out-of-bounds is an error
        assert!(sim
            .set_disc_pos_vel(
                disc_id,
                Position::new(5.3, -2.9),
                Velocity::new(-1.0, 1.0),
            )
            .is_err());

        // Set new position that switches x and y coordinates
        // and return the old position and velocity.
        let (prev_pos, prev_vel) = sim
            .set_disc_pos_vel(
                disc_id,
                Position::new(0.25, -0.25),
                Velocity::new(-1.0, 1.0),
            )
            .unwrap();

        assert_f64_eq(prev_pos.x, -0.25);
        assert_f64_eq(prev_pos.y, 0.25);
        assert_f64_eq(prev_vel.x, 0.4);
        assert_f64_eq(prev_vel.y, 0.3);

        let pos = sim.get_disc_by_id(disc_id).unwrap().pos();
        let vel = sim.get_disc_by_id(disc_id).unwrap().vel();
        assert_f64_eq(pos.x, 0.25);
        assert_f64_eq(pos.y, -0.25);
        assert_f64_eq(vel.x, -1.0);
        assert_f64_eq(vel.y, 1.0);

        // Setting a position and velocity on a disc that does not exist
        // is an error.
        sim.remove_disc(disc_id);
        assert!(sim
            .set_disc_pos_vel(
                disc_id,
                Position::new(0.25, -0.25),
                Velocity::new(-1.0, 1.0)
            )
            .is_err());
    }

    #[test]
    fn test_disc_friction() {
        // Create a Simulator with an arena with lots of room so we don't
        // have any wall collisions.
        let mut sim = Simulator::new(Arena::new(
            -12.0, // left
            12.0,  // top
            12.0,  // right
            -12.0, // bottom
        ));

        // Create three unique tags
        let tag0 = DiscTag::default();
        let tag1 = DiscTag::try_from(1).unwrap();
        let tag2 = DiscTag::try_from(2).unwrap();
        assert!(tag0 != tag1);
        assert!(tag0 != tag2);
        assert!(tag1 != tag2);

        // Create disc0 at orgin traveling right and up at 5 meters / second
        let tag0 = DiscTag::default();
        let disc0_id = sim.create_disc_or_panic(
            Disc::try_new(
                Position::new(0.0, 0.0),
                Velocity::new(4.0, 3.0),
                1.0, // radius 1 meter
                0.5, // mass 0.5 kg
                tag0,
            )
            .unwrap(),
        );

        // Create disc1 lower center traveling left at 5 meters / second
        let disc1_id = sim.create_disc_or_panic(
            Disc::try_new(
                Position::new(0.0, -2.0),
                Velocity::new(-5.0, 0.0),
                1.0, // radius 1 meter
                0.5, // mass 0.5 kg
                tag1,
            )
            .unwrap(),
        );

        // Create disc1 upper center traveling upward at 5 meters / second
        let disc2_id = sim.create_disc_or_panic(
            Disc::try_new(
                Position::new(0.0, 2.0),
                Velocity::new(0.0, 5.0),
                1.0, // radius 1 meter
                0.5, // mass 0.5 kg
                tag2,
            )
            .unwrap(),
        );

        // Set a different friction value for each tag
        let disc0_half_life = 1.0;
        let disc1_half_life = 0.5;
        sim.set_disc_tag_friction_half_life(tag0, Some(disc0_half_life))
            .unwrap();
        sim.set_disc_tag_friction_half_life(tag1, Some(disc1_half_life))
            .unwrap();
        sim.set_disc_tag_friction_half_life(tag2, None).unwrap(); // no friction
        assert_eq!(sim.num_tags_with_friction(), 2);

        // Step the simulator 10 times for a total of 1 second.
        for i in 1..=10 {
            sim.update(0.1).unwrap();
            if i == 5 {
                assert_f64_eq(sim.time(), disc1_half_life);
                // Check that disc1 velocity is half of its starting velocity
                let disc1 = sim.get_disc_by_id(disc1_id).unwrap();
                assert_f64_eq(disc1.vel().magnitude(), 2.5);
            }
            if i == 10 {
                assert_f64_eq(sim.time(), disc0_half_life);
                // Check that disc0 velocity is half of its starting velocity
                let disc0 = sim.get_disc_by_id(disc0_id).unwrap();
                assert_f64_eq(disc0.vel().magnitude(), 2.5);
            }
            // Check that disc2 velocity doesn't change
            let disc2 = sim.get_disc_by_id(disc2_id).unwrap();
            assert_f64_eq(disc2.vel().magnitude(), 5.0);
        }
    }

    #[test]
    fn test_iter_discs() {
        // Generate a grid pattern of discs
        const NUM_ROWS: usize = 3;
        const NUM_COLS: usize = 4;
        let disc_r: f64 = 0.5;
        let disc_m: f64 = 1.0;
        let disc_tag = DiscTag::default();
        let arena_width = NUM_COLS as f64 * disc_r * 2.5;
        let arena_height = NUM_ROWS as f64 * disc_r * 2.5;
        let disc_offset = Position::new(
            -arena_width * 0.5 + disc_r * 1.5,
            -arena_height * 0.5 + disc_r * 1.5,
        );
        let arena = Arena::try_new(
            -arena_width * 0.5,  // left,
            arena_height * 0.5,  // top,
            arena_width * 0.5,   // right,
            -arena_height * 0.5, // bottom,
        )
        .unwrap();
        let mut sim = Simulator::new(arena);
        for y_int in 0..NUM_ROWS {
            let y = (disc_r * 2.5) * y_int as f64;
            for x_int in 0..NUM_COLS {
                let x = (disc_r * 2.5) * x_int as f64;
                let pos = Position::new(x, y) + disc_offset.coords;
                let vel = Velocity::new(0.0, 0.0);
                let disc =
                    Disc::try_new(pos, vel, disc_r, disc_m, disc_tag).unwrap();
                sim.create_disc_or_panic(disc);
            }
        }
        assert_eq!(sim.num_discs(), NUM_ROWS * NUM_COLS);
        let mut count: usize = 0;
        let mut maybe_prev_disc_id: Option<DiscID> = None;
        let mut maybe_prev_disc: Option<Disc> = None;
        for (disc_id, disc) in sim.iter_discs() {
            if count > 0 {
                assert!(disc_id != maybe_prev_disc_id.unwrap());
                assert!(disc.pos() != maybe_prev_disc.unwrap().pos());
            }
            maybe_prev_disc_id = Some(disc_id);
            maybe_prev_disc = Some(disc.clone());
            count += 1;
        }
        assert_eq!(count, NUM_ROWS * NUM_COLS);
        assert_eq!(sim.num_discs(), NUM_ROWS * NUM_COLS);
    }
}
