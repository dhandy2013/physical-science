mod arena;
mod collision;
mod disc;
mod discid;
pub mod simulator;
pub mod types;

/// max function that works for floating-point numbers, needed because
/// std::cmp::max() does not work for them.
#[inline]
pub(crate) fn max_f<T: std::cmp::PartialOrd>(a: T, b: T) -> T {
    if a > b {
        a
    } else {
        b
    }
}

/// min function that works for floating-point numbers, needed because
/// std::cmp::min() does not work for them.
#[inline]
pub(crate) fn min_f<T: std::cmp::PartialOrd>(a: T, b: T) -> T {
    if a < b {
        a
    } else {
        b
    }
}

#[cfg(test)]
pub(crate) fn assert_f64_eq(a: f64, b: f64) {
    let abs_diff = (a - b).abs();
    if abs_diff < 1.0e-12 {
        return;
    }
    panic!("f64 values were not equal: a={} b={}", a, b);
}
