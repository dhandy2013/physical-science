# bounce - Simulate perfectly elastic collisions between discs in 2D space

This Rust library is a Physics engine that specializes in simulating the motion
of 2-dimensional discs in a 2-dimensional plane, with or without friction.
Perfectly elastic collisions are simulated perfectly. That means:

- Discs never accidentally pass through walls or through each other
- Time-of-impact of each collision is calculated precisely, no approximations
- Conservation of momentum is maintained

Correctness is valued over speed. For a small number of discs (a couple of
hundred) the perfomance will hopefully be good enough for an animation frame
rate of at least 10 FPS. For thousands of objects at 60-100 FPS get a game
engine. I will work on improving the performance, but will not sacrifice
correctness.


## Simulation Domain

The main simulation object is a Disc, which is a circular 2-dimensional object
with a mass and a radius. The mass must be greater than zero. The radius must be
greater than or equal to zero. A disc of radius zero is a point mass that can
collide with walls or with non-zero-radius discs, but cannot collide with other
discs with radius zero.

The coordinate system is `x` increasing to the right and `y` increasing upward.
Simulation occurs within an Arena which is a two-dimensional rectangular area.
The Arena is constructed with left, top, right, and bottom boundaries. Discs
cannot pass these bounds. They bounce off of them with perfectly reflecting
collisions.

Each disc is constructed with a tag (basically a wrapped small integer) that
identifies the behavior class of the disc. Right now that just specifies what
kind of friction a disc has. Set the friction of a tag to `None` for no
friction. Set it to a number greater than zero to set the frictional half-life
(time for friction to degrade the velocity by one half).

During a simulation time step, discs move as if there was no friction. At the
end of each time step friction is applied by scaling down the velocity of discs
according to their tag's friction half-life, if any.

You can simulate rocket thrust by telling the simulator in between frames to
expel mass from a disc with a given relative exhaust velocity.

You can simulate launching a projectile from a disc with recoil by telling the
simulator in between frames to split off some mass from a disc in order to
create a new disc with a given velocity relative to the parent disc.


## Creating a Simulation scenario

Create an Arena object and then use that to construct a Simulator:
```rust
let mut sim = Simulator::new(
    Arena::try_new(
        -12.0, // left
        12.0,  // top
        12.0,  // right
        -12.0, // bottom
    ).expect("Arena parameters should be correct")
);
```

Then add discs to the simulator:
```rust
// Create disc0 at orgin traveling right and up at 5 meters / second
let tag0 = DiscTag::default(); // same as DiscTag::try_from(0).unwrap()
let disc0_id = sim.create_disc(
    Disc::try_new(
        Position::new(0.0, 0.0),
        Velocity::new(4.0, 3.0),
        1.0, // radius 1 meter
        0.5, // mass 0.5 kg
        tag0,
    ).expect("Disc parameters should be correct")
);
```

For the disc to have friction, set a friction half-life value (time it takes for
friction to reduce disc velocity by one-half) on the tag with which the disc was
created:
```rust
let disc0_half_life = 1.0; // seconds
sim.set_disc_tag_friction_half_life(tag0, Some(disc0_half_life));
```
By default, tags have a friction half-life of `None`, meaning no friction.


## Running the Simulation

For each simulation frame:

- Call methods on the `Simulator` object to add, remove, or modify discs
- Call `sim.update(delta_t)` to update the simulation state one time step
- Call `sim.iter_discs()` to report the position and velocity of each disc
