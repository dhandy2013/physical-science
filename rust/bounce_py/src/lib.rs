use bounce::{
    simulator::{Arena, CollisionCounts, Disc, DiscID, DiscTag, Simulator},
    types::{Length, Mass, Position, Time, Velocity},
};
use pyo3::{
    exceptions::{PyIndexError, PyValueError},
    prelude::*,
};
use std::convert::TryFrom;

/// Unique ID for a disc being tracked by the simulator
#[pyclass(eq, frozen, hash, name = "DiscID")]
#[derive(PartialEq, Hash)]
struct PyDiscID {
    inner: DiscID,
}

#[pymethods]
impl PyDiscID {
    #[new]
    #[pyo3(signature = (value, /))]
    fn new(value: u32) -> PyResult<Self> {
        let disc_id = DiscID::try_from(value)
            .map_err(|err| PyValueError::new_err(err))?;
        Ok(Self { inner: disc_id })
    }

    fn __repr__(&self) -> String {
        format!("{:?}", self.inner)
    }

    fn __int__(&self) -> u32 {
        self.inner.as_u32()
    }
}

/// Disc marker to help implement custom behavior for classes of discs
#[pyclass(eq, frozen, hash, name = "DiscTag")]
#[derive(PartialEq, Hash)]
struct PyDiscTag {
    inner: DiscTag,
}

#[pymethods]
impl PyDiscTag {
    /// The number of possible tag values, used for array size
    #[classattr]
    pub const NUM_TAGS: usize = DiscTag::NUM_TAGS;

    #[new]
    #[pyo3(signature = (value = 0, /))]
    fn new(value: i32) -> PyResult<Self> {
        let tag = DiscTag::try_from(value)
            .map_err(|err| PyValueError::new_err(err))?;
        Ok(Self { inner: tag })
    }

    fn __repr__(&self) -> String {
        format!("{:?}", self.inner)
    }

    fn __index__(&self) -> usize {
        self.inner.index()
    }
}

/// Position in 2-D space
#[pyclass(eq, frozen, name = "Position")]
#[derive(PartialEq)]
pub struct PyPosition {
    pub inner: Position,
}

#[pymethods]
impl PyPosition {
    #[new]
    #[pyo3(signature = (x, y, /))]
    fn new(x: f64, y: f64) -> Self {
        Self {
            inner: Position::new(x, y),
        }
    }

    fn __repr__(&self) -> String {
        format!("{:?}", self.inner)
    }

    #[getter]
    fn x(&self) -> f64 {
        self.inner.x
    }

    #[getter]
    fn y(&self) -> f64 {
        self.inner.y
    }
}

/// Velocity in 2-D space
#[pyclass(eq, frozen, name = "Velocity")]
#[derive(PartialEq)]
struct PyVelocity {
    inner: Velocity,
}

#[pymethods]
impl PyVelocity {
    #[new]
    #[pyo3(signature = (x, y, /))]
    fn new(x: f64, y: f64) -> Self {
        Self {
            inner: Velocity::new(x, y),
        }
    }

    fn __repr__(&self) -> String {
        format!("{:?}", self.inner)
    }

    #[getter]
    fn x(&self) -> f64 {
        self.inner.x
    }

    #[getter]
    fn y(&self) -> f64 {
        self.inner.y
    }
}

/// Disc is a circular object with position, velocity, radius, and mass
#[pyclass(frozen, name = "Disc")]
struct PyDisc {
    inner: Disc,
}

#[pymethods]
impl PyDisc {
    #[new]
    #[pyo3(signature = (pos, vel, /, *, r, m, tag=None))]
    fn new(
        pos: &Bound<'_, PyPosition>,
        vel: &Bound<'_, PyVelocity>,
        r: Length,
        m: Mass,
        tag: Option<&Bound<'_, PyDiscTag>>,
    ) -> PyResult<Self> {
        let pos = pos.borrow().inner;
        let vel = vel.borrow().inner;
        let tag = match tag {
            Some(bound_tag) => bound_tag.borrow().inner,
            None => DiscTag::default(),
        };
        let disc = Disc::try_new(pos, vel, r, m, tag)
            .map_err(|err| PyValueError::new_err(err))?;
        Ok(Self { inner: disc })
    }

    fn __repr__(&self) -> String {
        format!("{:?}", self.inner)
    }

    #[getter]
    fn pos(&self) -> PyPosition {
        PyPosition {
            inner: self.inner.pos(),
        }
    }

    #[getter]
    fn vel(&self) -> PyVelocity {
        PyVelocity {
            inner: self.inner.vel(),
        }
    }

    #[getter]
    fn r(&self) -> f64 {
        self.inner.r()
    }

    #[getter]
    fn m(&self) -> f64 {
        self.inner.m()
    }

    #[getter]
    fn tag(&self) -> PyDiscTag {
        PyDiscTag {
            inner: self.inner.tag(),
        }
    }
}

/// Arena is a rectangular area in which discs are allowed to move
#[pyclass(frozen, name = "Arena")]
struct PyArena {
    inner: Arena,
}

#[pymethods]
impl PyArena {
    #[new]
    fn new(left: f64, top: f64, right: f64, bottom: f64) -> PyResult<Self> {
        let arena = Arena::try_new(left, top, right, bottom)
            .map_err(|err| PyValueError::new_err(err))?;
        Ok(Self { inner: arena })
    }

    fn __repr__(&self) -> String {
        format!("{:?}", self.inner)
    }

    #[getter]
    fn left(&self) -> f64 {
        self.inner.left()
    }

    #[getter]
    fn top(&self) -> f64 {
        self.inner.top()
    }

    #[getter]
    fn right(&self) -> f64 {
        self.inner.right()
    }

    #[getter]
    fn bottom(&self) -> f64 {
        self.inner.bottom()
    }
}

#[pyclass(eq, frozen, name = "CollisionCounts")]
#[derive(Debug, PartialEq)]
struct PyCollisionCounts {
    inner: CollisionCounts,
}

#[pymethods]
impl PyCollisionCounts {
    /// Create new CollisionCounts object with all zero counts
    #[new]
    fn new() -> Self {
        Self {
            inner: CollisionCounts::default(),
        }
    }

    fn __repr__(&self) -> String {
        format!("{:?}", self.inner)
    }

    /// Return the number of collisions with an arena wall since the last update
    #[getter]
    fn num_collisions_with_wall(&self) -> usize {
        self.inner.num_collisions_with_wall()
    }

    /// Return the number of collisions with another disc since the last update
    #[getter]
    fn num_collisions_with_disc(&self) -> usize {
        self.inner.num_collisions_with_disc()
    }

    /// Return the number of collisions of any kind since the last update
    #[getter]
    fn num_collisions(&self) -> usize {
        self.inner.num_collisions()
    }
}

/// The simulation engine.
/// Iterating over this object yields (disc_id, disc) pairs.
#[pyclass(sequence, name = "Simulator")]
struct PySimulator {
    inner: Simulator,
}

#[pymethods]
impl PySimulator {
    #[new]
    fn new(arena: &Bound<'_, PyArena>) -> Self {
        let arena = arena.borrow().inner.clone();
        let sim = Simulator::new(arena);
        Self { inner: sim }
    }

    fn __repr__(&self) -> String {
        format!(
            "Simulator(arena={:?}, num_discs={})",
            self.inner.arena(),
            self.inner.num_discs()
        )
    }

    #[getter]
    fn arena(&self) -> PyArena {
        PyArena {
            inner: self.inner.arena().clone(),
        }
    }

    #[getter]
    fn num_discs(&self) -> usize {
        self.inner.num_discs()
    }

    /// Create a disc in the simulation engine. Return its tracking ID.
    /// Raise ValueError on attempt to create disc outside of arena bounds.
    fn create_disc(&mut self, disc: &Bound<'_, PyDisc>) -> PyResult<PyDiscID> {
        let disc = disc.borrow().inner.clone();
        let disc_id = self
            .inner
            .create_disc(disc)
            .map_err(|err| PyValueError::new_err(err))?;
        Ok(PyDiscID { inner: disc_id })
    }

    /// Arbitrarily set a new position and velocity on a disc.
    /// Return the old position and velocity.
    /// Raise ValueError if `disc_id` did not reference a valid disc,
    /// or if the new position is outside the simulator arena bounds.
    fn set_disc_pos_vel(
        &mut self,
        disc_id: &Bound<'_, PyDiscID>,
        pos: &Bound<'_, PyPosition>,
        vel: &Bound<'_, PyVelocity>,
    ) -> PyResult<(PyPosition, PyVelocity)> {
        let disc_id = disc_id.borrow().inner;
        let pos = pos.borrow().inner;
        let vel = vel.borrow().inner;
        let (old_pos, old_vel) = self
            .inner
            .set_disc_pos_vel(disc_id, pos, vel)
            .map_err(|err| PyValueError::new_err(err))?;
        Ok((PyPosition { inner: old_pos }, PyVelocity { inner: old_vel }))
    }

    /// Launch a new "child" disc from a parent disc.
    ///
    /// The `child_disc` position is ignored and overridden to become the
    /// position of the parent disc. The `child_disc` velocity is the desired
    /// velocity relative to the parent disc, i.e. the launch speed.
    ///
    /// If all goes well, return the ID of the new disc. Raise ValueError if:
    /// - `parent_disc_id` does not reference an existing Disc
    /// - child disc `m` is greater than or equal to mass of parent disc
    /// - there is an error creating the child disc
    fn launch_disc_from_disc(
        &mut self,
        parent_disc_id: &Bound<'_, PyDiscID>,
        child_disc: &Bound<'_, PyDisc>,
    ) -> PyResult<PyDiscID> {
        let parent_disc_id = parent_disc_id.borrow().inner;
        let child_disc = child_disc.borrow().inner.clone();
        let child_disc_id = self
            .inner
            .launch_disc_from_disc(parent_disc_id, child_disc)
            .map_err(|err| PyValueError::new_err(err))?;
        Ok(PyDiscID {
            inner: child_disc_id,
        })
    }

    /// Get a disc given its ID. Return None if no such disc exists.
    fn get_disc_by_id(&self, disc_id: &Bound<'_, PyDiscID>) -> Option<PyDisc> {
        let disc_id = disc_id.borrow().inner;
        let disc = self.inner.get_disc_by_id(disc_id)?;
        Some(PyDisc {
            inner: disc.clone(),
        })
    }

    /// Return the number of discs currently in the simulator.
    /// This is the same value as the `num_discs` property.
    fn __len__(&self) -> usize {
        self.inner.num_discs()
    }

    /// Return (disc_id, disc) pair at index `index` if index is in range
    /// 0..num_discs, otherwise raise IndexError.
    ///
    /// The order of discs is an implementation detail and changes dynamically.
    fn __getitem__(&self, index: usize) -> PyResult<(PyDiscID, PyDisc)> {
        let (disc_id, disc) = self
            .inner
            .get_id_disc_by_index(index)
            .map(|(disc_id, disc)| (disc_id, disc.clone()))
            .ok_or_else(|| {
                PyIndexError::new_err("index must be in range 0..num_discs")
            })?;
        Ok((PyDiscID { inner: disc_id }, PyDisc { inner: disc }))
    }

    // Disable the Python `is` operator that is implemented by default on
    // sequences.
    #[classattr]
    #[pyo3(name = "__contains__")]
    const __CONTAINS__: Option<PyObject> = None;

    /// Expel mass `m` from a disc at a velocity `v` relative to the disc.
    ///
    /// This reduces the mass of the disc by `m` and changes the velocity of the
    /// disc in order to conserve total momentum.  This simulates burning rocket
    /// fuel in discrete chunks.
    ///
    /// If all goes well, return the mass of the disc after reduction by `m`.
    /// Raise ValueError if:
    /// - `disc_id` does not reference a valid disc
    /// - `m` is less than zero
    /// - `m` is greater than or equal to the current disc mass
    fn expel_mass_from_disc(
        &mut self,
        disc_id: &Bound<'_, PyDiscID>,
        m: Mass,
        v: &Bound<'_, PyVelocity>,
    ) -> PyResult<Mass> {
        let disc_id = disc_id.borrow().inner;
        let v = v.borrow().inner;
        let new_m = self
            .inner
            .expel_mass_from_disc(disc_id, m, v)
            .map_err(|err| PyValueError::new_err(err))?;
        Ok(new_m)
    }

    /// Add mass `m` to a disc.
    ///
    /// This simulates things like replenishing fuel. Or, `m` can be negative to
    /// reduce mass for some specialized simulation need. The velocity of the
    /// disc is not changed.
    ///
    /// If all goes well, return the mass of the disc after adjustment.
    /// Raise ValueError if:
    /// - `disc_id` does not reference a valid disc
    /// - The adjusted mass would be less than or equal to zero.
    fn add_disc_mass(
        &mut self,
        disc_id: &Bound<'_, PyDiscID>,
        m: Mass,
    ) -> PyResult<Mass> {
        let disc_id = disc_id.borrow().inner;
        let new_m = self
            .inner
            .add_disc_mass(disc_id, m)
            .map_err(|err| PyValueError::new_err(err))?;
        Ok(new_m)
    }

    /// Remove a Disc from the simulator. If `disc_id` is the ID of an existing
    /// Disc, return it. Otherwise return `None`.
    fn remove_disc(&mut self, disc_id: &Bound<'_, PyDiscID>) -> Option<PyDisc> {
        let disc_id = disc_id.borrow().inner;
        let disc = self.inner.remove_disc(disc_id)?;
        Some(PyDisc { inner: disc })
    }

    /// Update the state of the simulator based on the laws of physics and the
    /// passage of an incremental amount of time.
    ///
    /// `delta_t`: Time step in seconds
    ///
    /// Raise ValueError if `delta_t` is less than or equal to zero.
    fn update(&mut self, delta_t: Time) -> PyResult<()> {
        self.inner
            .update(delta_t)
            .map_err(|err| PyValueError::new_err(err))
    }

    /// Total number of times update() has been called
    #[getter]
    fn frame_count(&self) -> u32 {
        self.inner.frame_count()
    }

    /// Total elapsed simulation time in seconds
    #[getter]
    fn time(&self) -> Time {
        self.inner.time()
    }

    /// Record any collisions involving this disc.
    fn subscribe_disc_collisions(&mut self, disc_id: &Bound<'_, PyDiscID>) {
        let disc_id = disc_id.borrow().inner;
        self.inner.subscribe_disc_collisions(disc_id);
    }

    /// Stop recording collisions involving this disc.
    fn unsubscribe_disc_collisions(&mut self, disc_id: &Bound<'_, PyDiscID>) {
        let disc_id = disc_id.borrow().inner;
        self.inner.unsubscribe_disc_collisions(disc_id);
    }

    /// Return a struct containing counts of collisions involving this disc by
    /// type of collision (with wall, with other disc). The collisions event
    /// records are cleared at the start of update on every frame.
    ///
    /// If no Disc with ID `disc_id` exists, or if no subscription was made for
    /// that disc, then return CollisionCounts with all zeros.
    fn count_disc_collisions(
        &self,
        disc_id: &Bound<'_, PyDiscID>,
    ) -> PyCollisionCounts {
        let disc_id = disc_id.borrow().inner;
        PyCollisionCounts {
            inner: self.inner.count_disc_collisions(disc_id),
        }
    }

    /// Return the number of discs that are subscribed to receive collision
    /// statistics via `.subscribe_disc_collisions()`. This method is for
    /// diagnostic and debug purposes, to make sure we aren't leaking memory nor
    /// slowing updates by accumulating forgotten subscriptions.
    #[getter]
    fn num_disc_collision_subscriptions(&self) -> usize {
        self.inner.num_disc_collision_subscriptions()
    }

    /// Set the friction half-life for all discs with the given tag.
    ///
    /// If `half_life` is `None` then the disc is frictionless.  Otherwise,
    /// `half_life` is the time in seconds it will take for frictional forces to
    /// reduce the disc velocity by half. Raise ValueError if `half_life` is <=
    /// 0.0.
    #[pyo3(signature = (tag, half_life))]
    fn set_disc_tag_friction_half_life(
        &mut self,
        tag: &Bound<'_, PyDiscTag>,
        half_life: Option<f64>,
    ) -> PyResult<()> {
        let tag = tag.borrow().inner;
        self.inner
            .set_disc_tag_friction_half_life(tag, half_life)
            .map_err(|err| PyValueError::new_err(err))
    }

    /// Return the number of disc tags with frictional half-lives set (in other
    /// words, that are not frictionless.)
    #[getter]
    fn num_tags_with_friction(&self) -> usize {
        self.inner.num_tags_with_friction()
    }
}

/// Simulate perfectly elastic collisions between discs in 2D space
#[pymodule(name = "bounce")]
fn bounce_py(m: &Bound<'_, PyModule>) -> PyResult<()> {
    m.add_class::<PyDiscID>()?;
    m.add_class::<PyDiscTag>()?;
    m.add_class::<PyPosition>()?;
    m.add_class::<PyVelocity>()?;
    m.add_class::<PyDisc>()?;
    m.add_class::<PyArena>()?;
    m.add_class::<PyCollisionCounts>()?;
    m.add_class::<PySimulator>()?;
    Ok(())
}
