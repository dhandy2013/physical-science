# bounce_py - PyO3 Python wrapper around bounce simulation crate

`bounce_py` is a Python importable wrapper around the
[bounce](https://bitbucket.org/dhandy2013/physical-science/src/master/rust/bounce/)
crate, using the [pyo3](https://pyo3.rs/) library. The `bounce` create simulates
perfectly elastic collisions between discs in 2D space.

## Setup

Development install:
```
python3 -m venv env
env/bin/pip install -e '.[testing]'
```

Re-build bounce_py when its Rust source code changes:
```
env/bin/pip install -e .
```

Run tests and check for bugs:
```
env/bin/pytest
env/bin/ruff check
env/bin/ruff format
env/bin/mypy .
```

Build wheel file:
```
env/bin/pip wheel -w wheels .
```
This will create the Python installable wheel file, something like:
`wheels/bounce-1.0.1a0-cp311-abi3-linux_x86_64.whl`

To install this wheel file in another virtual environment:
```
pip install <path-to-the-wheel-file>
```

To do a quick check that installation succeeded:
```
$ python
>>> import bounce
>>> bounce.DiscID(1)
DiscID(1)
```

## Tech Notes

I created this project using `maturin` by running this command:
```
maturin new --bindings=pyo3 --mixed bounce_py
```

Then [following this
hint](https://github.com/PyO3/maturin/issues/313#issuecomment-784438527) I
modified the generated files so that the Rust package name stays set to
`bounce_py` but the Python package name is just `bounce`:

- I added `name = "bounce"` in the `[lib]` section of Cargo.toml
- I put `#[pymodule(name = "bounce")]` as the decorator on the generated `bounce_py` function in src/lib.rs
- (Possibly not necessary, but just to be safe) I added this section to Cargo.toml:

```
[package.metadata.maturin]
name = "bounce"
```

In the `[project.optional-dependencies]` section I changed the name `tests`
to `testing` since that is the usual name I use for testing dependencies.
