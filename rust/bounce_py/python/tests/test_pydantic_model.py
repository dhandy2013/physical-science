"""
Test using objects from bounce written in Rust in Pydantic models
"""

import pytest
from bounce import DiscID
from bounce.model import DiscIDModel
from pydantic import BaseModel, ConfigDict
from pydantic_core import ValidationError


class ColoredDisc(BaseModel):
    """
    Example of a Pydantic model containing a class defined using PyO3 (DiscID)
    """

    model_config = ConfigDict(strict=True)

    disc_id: DiscIDModel
    color: str


def test_colored_disc_round_trip_via_dict() -> None:
    colored_disc_as_dict = {'disc_id': 1, 'color': 'red'}
    colored_disc = ColoredDisc.model_validate(colored_disc_as_dict)
    assert isinstance(colored_disc.disc_id, DiscID)
    assert colored_disc.disc_id == DiscID(1)
    assert colored_disc.color == 'red'
    colored_disc_dumped = ColoredDisc.model_dump(colored_disc)
    assert isinstance(colored_disc_dumped, dict)
    assert isinstance(colored_disc_dumped['disc_id'], int)
    assert colored_disc_dumped['disc_id'] == 1
    assert colored_disc_dumped['color'] == 'red'


def test_colored_disc_from_dict_with_float_int() -> None:
    colored_disc_json = '{"disc_id": 1.0, "color": "red"}'
    colored_disc_from_json = ColoredDisc.model_validate_json(colored_disc_json)
    assert isinstance(colored_disc_from_json, ColoredDisc)
    assert isinstance(colored_disc_from_json.disc_id, DiscID)
    assert colored_disc_from_json.disc_id == DiscID(1)
    assert colored_disc_from_json.color == 'red'

    colored_disc_as_dict = {'disc_id': 1.0, 'color': 'red'}
    colored_disc_from_dict = ColoredDisc.model_validate(colored_disc_as_dict)
    assert isinstance(colored_disc_from_dict, ColoredDisc)
    assert isinstance(colored_disc_from_dict.disc_id, DiscID)
    assert colored_disc_from_dict.disc_id == DiscID(1)
    assert colored_disc_from_dict.color == 'red'


def test_colored_disc_round_trip_via_json() -> None:
    colored_disc = ColoredDisc(disc_id=DiscID(1), color='red')
    colored_disc_as_json = colored_disc.model_dump_json()
    assert isinstance(colored_disc_as_json, (str, bytes))
    colored_disc_from_json = ColoredDisc.model_validate_json(
        colored_disc_as_json
    )
    assert colored_disc_from_json.disc_id == colored_disc.disc_id
    assert colored_disc_from_json.color == colored_disc.color


def test_colored_disc_validate_unhappy() -> None:
    colored_disc_as_dict_1 = {'disc_id': 1.5, 'color': 'red'}
    with pytest.raises(ValidationError):
        ColoredDisc.model_validate(colored_disc_as_dict_1)

    colored_disc_as_dict_2 = {'disc_id': (), 'color': 'red'}
    with pytest.raises(ValidationError):
        ColoredDisc.model_validate(colored_disc_as_dict_2)

    colored_disc_as_dict_3 = {'disc_id': 1, 'color': 3}
    with pytest.raises(ValidationError):
        ColoredDisc.model_validate(colored_disc_as_dict_3)


def test_colored_disc_construct() -> None:
    # Happy path - parameter types are exactly as specified
    colored_disc_0 = ColoredDisc(disc_id=DiscID(1), color='red')
    assert isinstance(colored_disc_0.disc_id, DiscID)
    assert colored_disc_0.disc_id == DiscID(1)
    assert isinstance(colored_disc_0.color, str)
    assert colored_disc_0.color == 'red'

    # Even with strict=True on model_config, integer is allowed for disc_id
    # parameter.
    colored_disc_1 = ColoredDisc(disc_id=1, color='red')  # type: ignore
    assert isinstance(colored_disc_1.disc_id, DiscID)
    assert colored_disc_1.disc_id == DiscID(1)

    # float is Ok for disc_id parameter as long as it represents an integer.
    # This is tolerated because we can be deserializing JSON coming from
    # Javascript.
    colored_disc_2 = ColoredDisc(disc_id=1.0, color='red')  # type: ignore
    assert isinstance(colored_disc_2.disc_id, DiscID)
    assert colored_disc_2.disc_id == DiscID(1)

    # float disc_id is _not_ Ok if it does not represent an integer
    with pytest.raises(ValidationError):
        ColoredDisc(disc_id=1.5, color='red')  # type: ignore

    # bytes instead of str is _not_ allowed for color parameter!
    with pytest.raises(ValidationError):
        ColoredDisc(disc_id=DiscID(1), color=b'red')  # type: ignore
