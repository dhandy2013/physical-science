from bounce import DiscID, DiscTag


def test_disc_id_happy() -> None:
    """
    Test all the things you can do with a DiscID without errors
    """
    i = DiscID(1)
    j = DiscID(2)
    k = DiscID(1)
    assert i != j
    assert i == k
    assert repr(i) == "DiscID(1)"
    d = {i: 'a', j: 'b'}
    assert d[i] == 'a'
    assert d[j] == 'b'
    assert int(j) == 2


def test_disc_id_unhappy() -> None:
    """
    Test errors creating DiscID
    """
    try:
        DiscID(0)
    except ValueError:
        pass
    else:
        raise AssertionError("DiscID should not have value 0")

    try:
        DiscID(-1)
    except OverflowError:
        pass
    else:
        raise AssertionError("DiscID(-1) should have raised OverflowError")

    try:
        DiscID("blah")  # type: ignore
    except TypeError:
        pass
    else:
        raise AssertionError("DiscID('blah') should have raised TypeError")

    a_list = [0, 1]
    i = DiscID(1)
    try:
        a_list[i]  # type: ignore
    except TypeError:
        pass
    else:
        raise AssertionError("DiscID should not be usable as array index")


def test_disc_tag_happy() -> None:
    """
    Test normal, successful actions with DiscTag objects
    """
    t0 = DiscTag()
    assert repr(t0) == "DiscTag(0)"
    assert int(t0) == 0

    t1 = DiscTag(1)
    assert repr(t1) == "DiscTag(1)"
    assert int(t1) == 1

    item_list = list(range(DiscTag.NUM_TAGS))
    assert item_list[t0] == 0
    assert item_list[t1] == 1


def test_disc_tag_unhappy() -> None:
    """
    Test errors with DiscTag construction
    """
    try:
        DiscTag(-1)
    except ValueError:
        pass
    else:
        raise AssertionError("DiscTag(-1) shoud have raised ValueError")

    one_too_many = DiscTag.NUM_TAGS
    try:
        DiscTag(one_too_many)
    except ValueError:
        pass
    else:
        raise AssertionError(
            f"DiscTag({one_too_many}) should have raised ValueError"
        )

    try:
        DiscTag('blah')  # type: ignore
    except TypeError:
        pass
    else:
        raise AssertionError("DiscTag('blah') should have raised TypeError")
