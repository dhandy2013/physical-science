from bounce import (
    Arena,
    CollisionCounts,
    Disc,
    DiscID,
    DiscTag,
    Position,
    Simulator,
    Velocity,
)


def test_simulator_create_empty() -> None:
    arena = Arena(-2.0, 1.5, 2.0, -1.5)
    sim = Simulator(arena)
    assert sim.arena.left == -2.0
    assert sim.arena.top == 1.5
    assert sim.arena.right == 2.0
    assert sim.arena.bottom == -1.5
    assert sim.num_discs == 0
    assert len(sim) == 0
    assert repr(sim).startswith('Simulator')


def test_create_disc_happy() -> None:
    arena = Arena(-2.0, 1.5, 2.0, -1.5)
    sim = Simulator(arena)
    pos = Position(1.5, 0.5)
    vel = Velocity(-1.0, -0.5)
    disc = Disc(pos, vel, r=0.25, m=0.75)
    sim.create_disc(disc)
    assert sim.num_discs == 1
    assert len(sim) == 1


def test_create_disc_out_of_bounds() -> None:
    arena = Arena(-2.0, 1.5, 2.0, -1.5)
    sim = Simulator(arena)
    pos = Position(1.5, 0.5)
    vel = Velocity(-1.0, -0.5)
    disc = Disc(pos, vel, r=5.0, m=0.75)  # r is way too big
    try:
        sim.create_disc(disc)
    except ValueError:
        pass
    else:
        assert False, "Should not be able to create disc bigger than arena"


def test_set_disc_pos_vel() -> None:
    arena = Arena(-2.0, 1.5, 2.0, -1.5)
    sim = Simulator(arena)
    pos = Position(1.5, 0.5)
    vel = Velocity(-1.0, -0.5)
    disc = Disc(pos, vel, r=0.25, m=0.75)
    disc_id = sim.create_disc(disc)
    new_pos = Position(0.0, 0.0)
    new_vel = Velocity(1.0, 0.5)
    old_pos, old_vel = sim.set_disc_pos_vel(disc_id, new_pos, new_vel)
    assert old_pos == Position(1.5, 0.5)
    assert old_vel == Velocity(-1.0, -0.5)

    bad_disc_id = DiscID(2)
    assert disc_id != bad_disc_id
    try:
        sim.set_disc_pos_vel(bad_disc_id, new_pos, new_vel)
    except ValueError:
        pass
    else:
        assert False, "Bad disc_id should have raised ValueError"

    out_of_bounds_pos = Position(100.0, 100.0)
    try:
        sim.set_disc_pos_vel(bad_disc_id, out_of_bounds_pos, new_vel)
    except ValueError:
        pass
    else:
        assert False, "Out-of-bounds pos should have raised ValueError"


def test_get_disc_by_id() -> None:
    arena = Arena(-2.0, 1.5, 2.0, -1.5)
    sim = Simulator(arena)
    pos = Position(0.25, 0.25)
    vel = Velocity(0.0, 0.0)
    disc = Disc(pos, vel, r=0.25, m=1.0)
    disc_id = sim.create_disc(disc)
    fetched_disc = sim.get_disc_by_id(disc_id)
    assert fetched_disc is not None
    assert fetched_disc.pos == disc.pos
    assert fetched_disc.vel == disc.vel
    assert fetched_disc.r == disc.r
    assert fetched_disc.m == disc.m
    assert fetched_disc.tag == disc.tag
    sim.remove_disc(disc_id)
    assert sim.get_disc_by_id(disc_id) is None


def test_launch_disc_from_disc() -> None:
    arena = Arena(-2.0, 1.5, 2.0, -1.5)
    sim = Simulator(arena)
    pos = Position(0.25, 0.25)
    vel = Velocity(0.0, 0.0)
    disc = Disc(pos, vel, r=0.25, m=1.0)
    disc_id = sim.create_disc(disc)
    child_pos = Position(0.0, 0.0)
    child_vel = Velocity(4.0, 3.0)
    child_disc = Disc(child_pos, child_vel, r=0.03125, m=0.0625)
    child_disc_id = sim.launch_disc_from_disc(disc_id, child_disc)
    # Check the child disc
    modified_child_disc = sim.get_disc_by_id(child_disc_id)
    assert modified_child_disc is not None
    assert modified_child_disc.pos.x == 0.25
    assert modified_child_disc.pos.y == 0.25
    assert modified_child_disc.m == 0.0625
    # Check parent disc
    modified_parent_disc = sim.get_disc_by_id(disc_id)
    assert modified_parent_disc is not None
    assert modified_parent_disc.m == 0.9375
    assert modified_parent_disc.vel.x < 0.0
    assert modified_parent_disc.vel.y < 0.0

    child_disc = Disc(child_pos, child_vel, r=0.03125, m=0.9375)
    try:
        sim.launch_disc_from_disc(disc_id, child_disc)
    except ValueError as err:
        assert "Child disc mass must be less than parent disc mass" in str(err)
    else:
        assert False, "Child mass too big, should have raised ValueError"

    bad_disc_id = DiscID(99)
    assert bad_disc_id != disc_id
    assert bad_disc_id != child_disc_id
    child_disc = Disc(child_pos, child_vel, r=0.03125, m=0.0625)
    try:
        sim.launch_disc_from_disc(bad_disc_id, child_disc)
    except ValueError as err:
        assert "Parent disc ID is invalid" in str(err)
    else:
        assert False, "Invalid disc_id should have raised ValueError"


def test_expel_mass_from_disc() -> None:
    arena = Arena(-2.0, 1.5, 2.0, -1.5)
    sim = Simulator(arena)
    pos = Position(0.25, 0.25)
    vel = Velocity(0.0, 0.0)
    disc = Disc(pos, vel, r=0.25, m=1.0)
    disc_id = sim.create_disc(disc)
    exhaust_vel = Velocity(-4.0, -3.0)
    exhaust_m = 0.0625
    new_m = sim.expel_mass_from_disc(disc_id, exhaust_m, exhaust_vel)
    assert new_m == 0.9375
    modified_disc = sim.get_disc_by_id(disc_id)
    assert modified_disc is not None
    assert modified_disc.m == 0.9375
    assert modified_disc.vel.x > 0.0
    assert modified_disc.vel.y > 0.0

    bad_disc_id = DiscID(99)
    assert bad_disc_id != disc_id
    try:
        sim.expel_mass_from_disc(bad_disc_id, exhaust_m, exhaust_vel)
    except ValueError as err:
        assert "invalid disc_id" in str(err)
    else:
        assert False, "Invalid disc_id should have raised ValueError"

    try:
        sim.expel_mass_from_disc(disc_id, -0.25, exhaust_vel)
    except ValueError as err:
        assert "Expelled mass must be >= 0.0" in str(err)
    else:
        assert False, "Expelled mass < 0.0 should have raised ValueErro"

    try:
        sim.expel_mass_from_disc(disc_id, 1.0, exhaust_vel)
    except ValueError as err:
        assert "Expelled mass must be less than disc mass" in str(err)
    else:
        assert (
            False
        ), "Expelled mass >= 0.0 disc mass should have raised ValueError"


def test_add_disc_mass() -> None:
    arena = Arena(-2.0, 1.5, 2.0, -1.5)
    sim = Simulator(arena)
    pos = Position(0.25, 0.25)
    vel = Velocity(0.0, 0.0)
    disc = Disc(pos, vel, r=0.25, m=1.0)
    disc_id = sim.create_disc(disc)
    new_m = sim.add_disc_mass(disc_id, -0.25)
    assert new_m == 0.75
    modified_disc = sim.get_disc_by_id(disc_id)
    assert modified_disc is not None
    assert modified_disc.m == 0.75

    bad_disc_id = DiscID(99)
    assert bad_disc_id != disc_id
    try:
        sim.add_disc_mass(bad_disc_id, -0.25)
    except ValueError as err:
        assert "invalid disc_id" in str(err)
    else:
        assert False, "Invalid disc_id should have raised ValueError"

    try:
        sim.add_disc_mass(disc_id, -1.0)
    except ValueError as err:
        assert "final disc mass must be > 0.0" in str(err)
    else:
        assert (
            False
        ), "Trying to reduce mass to zero or below should raise ValueError"


def test_remove_disc() -> None:
    arena = Arena(-2.0, 1.5, 2.0, -1.5)
    sim = Simulator(arena)
    pos = Position(0.25, 0.25)
    vel = Velocity(0.0, 0.0)
    disc = Disc(pos, vel, r=0.25, m=1.0)
    disc_id = sim.create_disc(disc)
    removed_disc = sim.remove_disc(disc_id)
    # sanity check on some of the fields of the removed disc
    assert removed_disc is not None
    assert removed_disc.r == 0.25
    assert removed_disc.m == 1.0

    # Removing already-removed disc should return None
    twice_removed_disc = sim.remove_disc(disc_id)
    assert twice_removed_disc is None


def test_sim_getitem() -> None:
    arena = Arena(0.0, 1.0, 1.0, 0.0)
    sim = Simulator(arena)
    pos = Position(0.5, 0.5)
    vel = Velocity(0.0, 0.0)
    disc = Disc(pos, vel, r=0.25, m=1.0)
    disc_id = sim.create_disc(disc)
    fetched_disc_id, fetched_disc = sim[0]
    assert fetched_disc_id == disc_id
    assert fetched_disc.pos == disc.pos

    try:
        sim[1]
    except IndexError:
        pass
    else:
        assert (
            False
        ), "Indexing simulator past end should have raised IndexError"


def test_iter_discs() -> None:
    arena = Arena(0.0, 1.0, 1.0, 0.0)
    sim = Simulator(arena)

    # Create 10 discs, alternating between two tags
    tag0 = DiscTag(0)
    tag1 = DiscTag(1)
    disc_id_set = set()
    for i in range(10):
        tag = tag0 if i % 2 == 0 else tag1
        pos = Position(i * 0.1 + 0.05, i * 0.1 + 0.05)
        vel = Velocity(0.0, 0.0)
        disc = Disc(pos, vel, r=0.04, m=1.0, tag=tag)
        disc_id = sim.create_disc(disc)
        disc_id_set.add(disc_id)
    assert len(sim) == 10
    assert len(sim) == len(disc_id_set)

    # Iterate over the discs, counting tags and verifying IDs
    count_by_tag = [0] * DiscTag.NUM_TAGS
    for disc_id, disc in sim:
        disc_id_set.remove(disc_id)
        count_by_tag[disc.tag] += 1
    assert len(disc_id_set) == 0, "All disc IDs should have been found"
    assert count_by_tag[tag0] == 5
    assert count_by_tag[tag1] == 5
    assert sum(count_by_tag) == len(sim)


def test_update() -> None:
    # Just do the most basic, trivial testing of the update method.
    # The functional testing of the update method is done by the Rust tests.
    arena = Arena(0.0, 1.0, 1.0, 0.0)
    sim = Simulator(arena)
    assert sim.frame_count == 0
    assert sim.time == 0.0

    sim.update(0.5)
    assert sim.frame_count == 1
    assert sim.time == 0.5

    try:
        sim.update('blah')  # type: ignore
    except TypeError:
        pass
    else:
        assert False, "Simulator update method only takes float not str"

    try:
        sim.update(0.0)
    except ValueError:
        pass
    else:
        assert (
            False
        ), "Simulator update should raise ValueError on delta_t == 0.0"

    try:
        sim.update(-0.5)
    except ValueError:
        pass
    else:
        assert (
            False
        ), "Simulator update should raise ValueError on delta_t < 0.0"


def test_collision_counts() -> None:
    # This is just basic interface testing.
    # The detailed behavior of collisions is verified by the Rust tests.
    arena = Arena(0.0, 1.0, 1.0, 0.0)
    sim = Simulator(arena)

    # Create 3 discs: one to collide with a wall, the other two to collide with
    # each other.
    r = 0.05
    m = 1.0
    disc0 = Disc(Position(0.1, 0.1), Velocity(-1.0, 0.0), r=r, m=m)
    disc1 = Disc(Position(0.5, 0.1), Velocity(1.0, 0.0), r=r, m=m)
    disc2 = Disc(Position(0.9, 0.1), Velocity(-1.0, 0.0), r=r, m=m)

    disc0_id = sim.create_disc(disc0)
    disc1_id = sim.create_disc(disc1)
    disc2_id = sim.create_disc(disc2)
    assert sim.num_disc_collision_subscriptions == 0

    sim.subscribe_disc_collisions(disc0_id)
    sim.subscribe_disc_collisions(disc1_id)
    assert sim.num_disc_collision_subscriptions == 2

    # Step enough time for each disc to particpate in exactly 1 collision
    sim.update(0.2)

    disc0_collisions = sim.count_disc_collisions(disc0_id)
    assert disc0_collisions.num_collisions == 1
    assert disc0_collisions.num_collisions_with_wall == 1
    assert disc0_collisions.num_collisions_with_disc == 0

    disc1_collisions = sim.count_disc_collisions(disc1_id)
    assert disc1_collisions.num_collisions == 1
    assert disc1_collisions.num_collisions_with_wall == 0
    assert disc1_collisions.num_collisions_with_disc == 1

    # disc2 wasn't subscribed so all counts should be zero
    disc2_collisions = sim.count_disc_collisions(disc2_id)
    assert disc2_collisions.num_collisions == 0
    assert disc2_collisions.num_collisions_with_wall == 0
    assert disc2_collisions.num_collisions_with_disc == 0
    # Should compare equal to newly-constructed all-zeros CollisionCounts obj
    assert disc2_collisions == CollisionCounts()

    sim.unsubscribe_disc_collisions(disc0_id)
    assert sim.num_disc_collision_subscriptions == 1

    # Step enough time for each disc to experience more collisions
    sim.update(1.0)

    # disc0 was unsubscribed so all counts should be zero
    disc0_collisions2 = sim.count_disc_collisions(disc0_id)
    assert disc0_collisions2.num_collisions_with_wall == 0
    assert disc0_collisions2.num_collisions_with_disc == 0


def test_friction() -> None:
    # Very basic test of setting friction on disc tags.
    # The complete verification of friction is done by the Rust tests.
    arena = Arena(0.0, 1.0, 1.0, 0.0)
    sim = Simulator(arena)
    tag0 = DiscTag()
    tag1 = DiscTag(1)
    assert tag0 != tag1
    sim.set_disc_tag_friction_half_life(tag0, 1.5)
    sim.set_disc_tag_friction_half_life(tag1, 2.5)
    assert sim.num_tags_with_friction == 2
    sim.set_disc_tag_friction_half_life(tag1, None)
    assert sim.num_tags_with_friction == 1

    try:
        sim.set_disc_tag_friction_half_life(tag0, 0.0)
    except ValueError:
        pass
    else:
        assert (
            False
        ), "Setting disc tag friction half-life of 0.0 should raise ValueError"

    try:
        sim.set_disc_tag_friction_half_life(tag0, -1.5)
    except ValueError:
        pass
    else:
        assert False, (
            "Setting disc tag friction half-life less than 0 "
            "should raise ValueError"
        )
