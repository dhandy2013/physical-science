from bounce import Disc, DiscTag, Position, Velocity


def test_disc_happy_create() -> None:
    tag0 = DiscTag()
    disc0 = Disc(Position(3.0, 4.0), Velocity(-6.0, -7.0), r=12.5, m=39.5)
    assert disc0.pos.x == 3.0
    assert disc0.pos.y == 4.0
    assert disc0.vel.x == -6.0
    assert disc0.vel.y == -7.0
    assert disc0.r == 12.5
    assert disc0.m == 39.5
    assert disc0.tag == tag0

    tag1 = DiscTag(1)
    assert tag0 != tag1
    disc1 = Disc(
        Position(-0.5, 5.5), Velocity(3.5, -4.5), r=2.5, m=1.5, tag=tag1
    )
    assert disc1.pos.x == -0.5
    assert disc1.pos.y == 5.5
    assert disc1.vel.x == 3.5
    assert disc1.vel.y == -4.5
    assert disc1.r == 2.5
    assert disc1.m == 1.5
    assert disc1.tag == tag1

    # disc2 is like disc1 except radius is zero, which is Ok
    disc2 = Disc(
        Position(-0.5, 5.5), Velocity(3.5, -4.5), r=0.0, m=1.5, tag=tag1
    )
    assert disc2.pos.x == -0.5
    assert disc2.pos.y == 5.5
    assert disc2.vel.x == 3.5
    assert disc2.vel.y == -4.5
    assert disc2.r == 0.0
    assert disc2.m == 1.5
    assert disc2.tag == tag1


def test_disc_unhappy_create() -> None:
    try:
        Disc(Position(3.0, 4.0), Velocity(-6.0, -7.0), r=-0.001, m=39.5)
    except ValueError:
        pass
    else:
        raise AssertionError("Disc should never allow r < 0.0")

    try:
        Disc(Position(3.0, 4.0), Velocity(-6.0, -7.0), r=12.5, m=0.0)
    except ValueError:
        pass
    else:
        raise AssertionError("Disc should never allow m <= 0.0")
