from bounce import Arena


def test_arena_create_happy() -> None:
    arena = Arena(-2.0, 1.5, 2.0, -1.5)
    assert arena.left == -2.0
    assert arena.top == 1.5
    assert arena.right == 2.0
    assert arena.bottom == -1.5

    arena2 = Arena(left=-10.0, top=5.0, right=10.0, bottom=-5.0)
    assert arena2.left == -10.0
    assert arena2.top == 5.0
    assert arena2.right == 10.0
    assert arena2.bottom == -5.0


def test_arena_create_unhappy() -> None:
    try:
        Arena(-2.0, 1.5, 2.0, 'blah')  # type: ignore
    except TypeError:
        pass
    else:
        assert False, "Arena should only take numeric parameters"

    try:
        Arena(-2.0, 1.5, 2.0)  # type: ignore
    except TypeError:
        pass
    else:
        assert False, "Arena has 4 required parameters"

    try:
        Arena(2.0, 1.5, -2.0, -1.5)
    except ValueError:
        pass
    else:
        assert False, "Arena left must be < right"

    try:
        Arena(2.0, 1.5, 2.0, -1.5)
    except ValueError:
        pass
    else:
        assert False, "Arena left must be != right"

    try:
        Arena(-2.0, -1.5, 2.0, 1.5)
    except ValueError:
        pass
    else:
        assert False, "Arena top must be > bottom"

    try:
        Arena(-2.0, 1.5, 2.0, 1.5)
    except ValueError:
        pass
    else:
        assert False, "Arena top must be != bottom"
