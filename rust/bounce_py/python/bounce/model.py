"""
Support for Pydantic models containing objects from bounce module
"""

from collections.abc import Callable
from typing import Annotated, Any, Generic, TypeVar

from pydantic import GetCoreSchemaHandler
from pydantic_core import PydanticCustomError, core_schema

from bounce import DiscID

__all__ = ['DiscIDModel']


def convert_disc_id_to_int(value: int | DiscID) -> int:
    """Turn a DiscID into an integer"""
    # Yes we need to handle `value` of type int or DiscID.
    if isinstance(value, DiscID):
        return int(value)
    elif isinstance(value, int):
        return value
    raise PydanticCustomError(
        "convert_disc_id_to_int",
        "Cannot convert value {value!r} to int as if it were a Disc ID",
        {'value': value},
    )


def convert_int_to_disc_id(value: int | float | DiscID) -> DiscID:
    """Turn an integer into a DiscID"""
    # Yes we need to handle `value` of type int or DiscID.
    # Also handling integers in float format because of JSON that comes from
    # Javascript.
    if isinstance(value, int):
        return DiscID(value)
    elif isinstance(value, DiscID):
        return value
    elif isinstance(value, float):
        value_int = int(value)
        if value_int == value:
            return DiscID(value_int)
    raise PydanticCustomError(
        "convert_int_to_disc_id",
        "Cannot convert {value!r} to DiscID as if it were an int",
        {'value': value},
    )


# Following the pattern from the Pydantic Docs: "Handling third-party types"
# https://docs.pydantic.dev/latest/concepts/types/#handling-third-party-types
#
# The Pydantic docs say this about using __get_pydantic_core_schema__:
# """
# While pydantic uses pydantic-core internally to handle validation and
# serialization, it is a new API for Pydantic V2, thus it is one of the areas
# most likely to be tweaked in the future and you should try to stick to the
# built-in constructs like those provided by annotated-types, pydantic.Field, or
# BeforeValidator and so on.
# """
# But a major advantage of using __get_pydantic_core_schema__ is that you don't
# have to put arbitrary_types_allowed=True on your model_config.
#
# Just in case __get_pydantic_core_schema__ breaks in the future, here is the
# the way to do this without using __get_pydantic_core_schema__:
#
# from pydantic.functional_serializers import PlainSerializer
# from pydantic.functional_validators import BeforeValidator
#
# DiscIDModel = Annotated[
#     DiscID,
#     PlainSerializer(convert_disc_id_to_int),
#     BeforeValidator(convert_int_to_disc_id),
# ]


T = TypeVar('T')


class _IntWrapperAnnotation(Generic[T]):
    """
    Use an instance of this class in a typing Annotation to make Pydantic allow
    an arbitrary "integer like" type to be stored in a Pydantic model.
    """

    def __init__(
        self,
        target_type: type[T],
        *,
        convert_int_to_obj: Callable[[int | float | T], T],
        convert_obj_to_int: Callable[[int | T], int],
    ):
        self.target_type = target_type
        self.convert_int_to_obj = convert_int_to_obj
        self.convert_obj_to_int = convert_obj_to_int

    def __get_pydantic_core_schema__(
        self, _src_type: Any, _handler: GetCoreSchemaHandler
    ) -> core_schema.CoreSchema:
        from_number_schema = core_schema.chain_schema(
            [
                core_schema.union_schema(
                    [core_schema.int_schema(), core_schema.float_schema()]
                ),
                core_schema.no_info_plain_validator_function(
                    self.convert_int_to_obj
                ),
            ]
        )
        return core_schema.json_or_python_schema(
            json_schema=from_number_schema,
            python_schema=core_schema.union_schema(
                [
                    # check if it's an instance first before doing any further work
                    core_schema.is_instance_schema(self.target_type),
                    from_number_schema,
                ]
            ),
            serialization=core_schema.plain_serializer_function_ser_schema(
                self.convert_obj_to_int
            ),
        )


# Annotated wrapper around DiscID to use for DiscID fields in Pydantic models
DiscIDModel = Annotated[
    DiscID,
    _IntWrapperAnnotation[DiscID](
        DiscID,
        convert_obj_to_int=convert_disc_id_to_int,
        convert_int_to_obj=convert_int_to_disc_id,
    ),
]
