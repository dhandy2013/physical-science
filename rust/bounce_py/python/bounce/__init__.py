import importlib.metadata

from . import bounce
from .bounce import *  # noqa: F403

__doc__ = bounce.__doc__
if hasattr(bounce, "__all__"):
    __all__ = bounce.__all__  # type: ignore [unused-ignore]
__version__ = importlib.metadata.version('bounce')
