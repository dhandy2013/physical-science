import init, { Controller, ColorNameIterator } from "./pkg/bounce_web.js";

let wasm = null;
let controller = null;
let colorArray = null;

async function run() {
    console.log("index.js:run(): Initializing the WASM module");
    wasm = await init();
    controller = Controller.new();

    // Initalize colorArray with the names of all colors known to the simulator,
    // in order. Form an array to be indexed using color numbers from the
    // simulator.
    colorArray = [];
    const colorIterator = ColorNameIterator.new();
    while (true) {
        const colorName = colorIterator.next();
        if (colorName === null) {
            break;
        }
        colorArray.push(colorName);
    }
}

const onLoad = () => {
    console.log("onLoad");
    finishSetup();
};

// Arrange code to be called when page finishes loading
window.onload = onLoad;

const controlPanel = document.getElementById("control-panel");
const homeButton = document.getElementById("id-button-home");
const showHideButton = document.getElementById("id-button-show-hide");
const restartButton = document.getElementById("id-button-restart");
const playPauseButton = document.getElementById("id-button-play-pause");
const infoArea = document.getElementById("info-area");
const infoStats = document.getElementById("info-stats");

let canvas = null;          // the drawing canvas
let initPromise = run();    // load WASM module asynchronously
let redrawType = "draw";    // animate, draw, or clear
let setupPhase = 0;         // to control initialization


// Buffers containing display info
const maxNumObjs = 500;
const xCoords = new Float64Array(maxNumObjs);
const yCoords = new Float64Array(maxNumObjs);
const radii = new Float64Array(maxNumObjs);
const colorCodes = new Uint8Array(maxNumObjs);

// animationPeriodMS controls the speed of animation.
// A bigger number means slower animation.
const animationPeriodMS = 4000.0; // milliseconds per animation time period
let lastRedrawId = null;    // To avoid stacking up multiple redraws
let lastTimestamp = null;   // Timestamp of last call to onRedraw

// Request a call to onRedraw if one has not already been requested.
const requestRedraw = () => {
    if (lastRedrawId === null) {
        lastRedrawId = window.requestAnimationFrame(onRedraw);
    }
}

// Schedule a call to onRedraw without regard to any previous timestamp.
const forceRedraw = () => {
    lastTimestamp = null;
    requestRedraw();
}

// Do not call this directly. Call requestRedraw() or forceRedraw() instead.
const onRedraw = (timestamp) => {
    // Clear the redraw request ID so we can draw again later.
    lastRedrawId = null;

    if (lastTimestamp === timestamp) {
        // Skip duplicate redraw
        console.log("onRedraw: Skip duplicate redraw at timestamp " + timestamp);
    } else {
        // Redraw stuff
        let dt = 0.0;   // delta time
        if (redrawType === "animate" && lastTimestamp !== null) {
            dt = (timestamp - lastTimestamp) / animationPeriodMS;
        }
        lastTimestamp = timestamp;

        setupCanvas();

        const t0 = performance.now();
        // Clearing the canvas in Javascript is about 2x faster than in Rust.
        clearCanvas();  // clear canvas in Javascript code
        // Controller.clear(canvas); // clear canvas from Rust code
        const t1 = performance.now();
        let t2 = t1;
        let t3 = t1;
        if (redrawType !== "clear") {
            if (dt > 0.0) {
                controller.update(dt);
            }
            t2 = performance.now();
            // Note: Drawing in Javascript is usually faster than drawing in Rust.
            drawObjects();  // draw in Javascript
            // controller.draw_objects(canvas); // draw in Rust
            t3 = performance.now();
        }

        if (!infoStats.classList.contains("hidden")) {
            updateDisplayInfo({
                clearTime: t1 - t0,
                calcTime: t2 - t1,
                drawTime: t3 - t1,
            });
        }
    }

    // Schedule the next animation frame if we are animating.
    if (redrawType === "animate") {
        requestRedraw();
    }
};

const drawObjects = () => {
    const context = initContext();
    const ctx = context.ctx;
    const n = controller.copy_display_list_bufs(
        xCoords,
        yCoords,
        radii,
        colorCodes,
    );
    for (let i = 0; i < n; ++i) {
        ctx.beginPath();
        ctx.arc(xCoords[i], yCoords[i], radii[i], 0.0, Math.PI * 2.0);
        ctx.fillStyle = colorArray[colorCodes[i]];
        ctx.fill();
    }
}

let initContext = () => {
    const half_width = canvas.width / 2.0;
    const half_height = canvas.height / 2.0;
    const s = (half_width < half_height) ? half_width : half_height;
    const scale = (s <= 0.0) ? 1.0 : s;
    const pxsize = 1.0 / scale;
    const ctx = canvas.getContext('2d', { "alpha": false });
    ctx.setTransform(scale, 0.0, 0.0, -scale, half_width, half_height);
    return { "ctx": ctx, "pxsize": pxsize };
};

let clearCanvas = () => {
    const ctx = canvas.getContext("2d", { "alpha": false });
    ctx.fillStyle = "black";
    const x = 0.0;
    const y = 0.0;
    const w = canvas.width;
    const h = canvas.height;
    // Get original 2D transformation matrix
    const t = ctx.getTransform();
    // Set to straight pixel transformation
    ctx.setTransform(1.0, 0.0, 0.0, 1.0, 0.0, 0.0);
    // Clear the canvas
    ctx.fillRect(x, y, w, h);
    // Restore the original transformation
    ctx.setTransform(t.a, t.b, t.c, t.d, t.e, t.f);
};

const setupCanvas = () => {
    // Calculate desired size of canvas
    // Unfortunately if I don't apply this padding I get a vertical scrollbar
    // on desktop browsers (but not on mobile browsers.)
    const pad = 4;
    const de = document.documentElement;
    const canvasWidth = Math.max(de.clientWidth - pad, pad);
    const canvasHeight = Math.max(de.clientHeight - pad, pad);

    if (canvas === null) {
        // Create the drawing canvas and set the `canvas` global variable
        canvas = document.createElement("canvas");
        canvas.id = "viewer-canvas";
        canvas.width = canvasWidth;
        canvas.height = canvasHeight;
        // Put the canvas inside its container
        const container = document.getElementById("canvas-container");
        container.appendChild(canvas);
    } else if (canvas.width !== canvasWidth || canvas.height !== canvasHeight) {
        canvas.width = canvasWidth;
        canvas.height = canvasHeight;
    }
};

const updateDisplayInfo = (timingInfo) => {
    document.getElementById("id-clear-time").innerText =
        (timingInfo.clearTime).toFixed(3) + "ms";
    document.getElementById("id-calc-time").innerText =
        (timingInfo.calcTime).toFixed(3) + "ms";
    document.getElementById("id-draw-time").innerText =
        (timingInfo.drawTime).toFixed(3) + "ms";
    document.getElementById("id-canvas-width").innerText = canvas.width;
    document.getElementById("id-canvas-height").innerText = canvas.height;
    const de = document.documentElement;
    document.getElementById("id-client-width").innerText = de.clientWidth;
    document.getElementById("id-client-height").innerText = de.clientHeight;
};

const onButtonHome = (event) => {
    // Reset control panel and info area to their inital state
    setButtonImage(showHideButton, "images/plus-sign-svgrepo-com.svg");

    // Reset play/pause button
    setButtonImage(playPauseButton, "images/play-svgrepo-com.svg");
    playPauseButton.title = "play";

    infoArea.classList.add("hidden");
    infoStats.classList.add("hidden");

    // Go back to main menu
    document.location.hash = "";
}

const setButtonImage = (button, newHref) => {
    const svg = button.firstElementChild;
    const image = svg.firstElementChild;
    svg.removeChild(image);
    const newImage = document.createElementNS("http://www.w3.org/2000/svg",
        "image");
    newImage.setAttribute("href", newHref);
    svg.appendChild(newImage);
};

const onButtonShowHide = (event) => {
    const button = event.currentTarget;
    let newHref = null;
    if (infoArea.classList.contains("hidden")) {
        infoArea.classList.remove("hidden");
        if (redrawType === "animate") {
            infoStats.classList.add("hidden");
        } else {
            infoStats.classList.remove("hidden");
        }
        // Set button image to minus sign SVG file
        newHref = "images/minus-sign-of-a-line-in-horizontal-position-svgrepo-com.svg";
    } else {
        infoArea.classList.add("hidden");
        infoStats.classList.add("hidden");
        // Set button image to plus sign SVG file
        newHref = "images/plus-sign-svgrepo-com.svg";
    }
    setButtonImage(button, newHref);
};

const onButtonRestart = (event) => {
    // Restart animation from the beginning.
    controller.set_plot_mode(controller.plot_mode_id());
    forceRedraw();
};

const onButtonPlayPause = (event) => {
    const button = event.currentTarget;
    let newHref = null;
    if (button.title === "pause") {
        redrawType = "draw";    // stop animation
        button.title = "play";
        infoStats.classList.remove("hidden");
        newHref = "images/play-svgrepo-com.svg";
    } else {
        redrawType = "animate";    // restart animation
        lastTimestamp = null;
        button.title = "pause";
        infoStats.classList.add("hidden");
        newHref = "images/pause-svgrepo-com.svg";
    }
    setButtonImage(button, newHref);
    requestRedraw();
};

const onResize = () => {
    // const de = document.documentElement;
    // console.log("onResize: " + de.clientWidth + "X" + de.clientHeight);
    requestRedraw();
}

const modeList = [
    "main-menu",
    "plot-big-small",
    "plot-diffusion",
];

// Handle hash routing - determines what happens when following a link
// to a fragment on this "page".
const doRouting = (event) => {
    const prevModeId = controller.plot_mode_id();
    let prevElement = null;
    if (prevModeId) {
        prevElement = document.getElementById(prevModeId);
    }
    if (prevElement) {
        prevElement.classList.add("hidden");
    }
    const prevElementTitle = document.getElementById(prevModeId + "-title");
    if (prevElementTitle) {
        prevElementTitle.classList.add("hidden");
    }
    let newModeHash = window.location.hash;
    let newModeId = "main-menu"; // default in case hash not recognized
    if (newModeHash) {
        newModeHash = newModeHash.substring(1); // remove leading "#"
        if (modeList.includes(newModeHash)) {
            newModeId = newModeHash;
        }
    }
    modeList.forEach((value, _index, _array) => {
        let elementId = value;
        let element = document.getElementById(elementId);
        if (element) {
            if (elementId === newModeId) {
                element.classList.remove("hidden");
                const elementTitle = document.getElementById(newModeId + "-title");
                if (elementTitle) {
                    elementTitle.classList.remove("hidden");
                }
                if (prevModeId !== newModeId) {
                    // Mode change
                    controller.set_plot_mode(newModeId);
                    // Clear previous timestamp and redraw.
                    forceRedraw();
                }
            } else {
                element.classList.add("hidden");
            }
        }
    });
    if (newModeId === "main-menu") {
        controlPanel.classList.add("hidden");
        infoArea.classList.remove("hidden");
        infoStats.classList.add("hidden");
    } else {
        controlPanel.classList.remove("hidden");
        infoArea.classList.add("hidden");
        infoStats.classList.add("hidden");
    }
    redrawType = "draw"; // draw whenever we have a routing event
}

const finishSetup = () => {
    // Only execute if this is exactly the second time it is called
    setupPhase += 1;
    if (setupPhase !== 2) {
        return;
    }

    // Handle home button
    homeButton.addEventListener("click", onButtonHome);

    // Handle show/hide button
    showHideButton.addEventListener("click", onButtonShowHide);

    // Handle restart button
    restartButton.addEventListener("click", onButtonRestart);

    // Handle play/pause button
    playPauseButton.addEventListener("click", onButtonPlayPause);

    // Handle window resize events
    window.onresize = onResize;

    // Handle hash routing
    window.onhashchange = doRouting;

    // Schedule the first canvas draw.
    forceRedraw();

    // Just in case URL has "#" in it, call hash routing function
    doRouting({});
}

initPromise.then(() => {
    // When this code runs it means that the WASM module has finished loading,
    // therefore we can call functions defined in WASM now.
    console.log("Reached initPromise.then");
    finishSetup();
});
