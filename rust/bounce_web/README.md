# Bounce - perfectly elastic collisions

This browser application is an animated demonstration of "perfectly elastic collisions". It is written in Rust with Web Assembly](https://www.rust-lang.org/what/wasm) (a.k.a. "WASM") and Javascript.

## Compile and run the demo

To build this demo:
```
wasm-pack build --target=web
```

To run this demo:
```
python3 -m http.server
```
and then navigate your web browser to the URL that it prints.

## Physics

The information display next to the moving objects demonstrates two laws of nature:

- Conservation of momentum
- Conservation of mass-energy

Notice that the total momentum and total energy numbers do not change from frame to frame.