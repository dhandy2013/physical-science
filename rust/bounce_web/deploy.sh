#!/bin/bash -e
if [ "$1" = "" -o "$1" = "--help" ] ; then
    echo "Usage: ./deploy.sh <remote-host>"
    exit 1
fi
remote_host="$1"
target="bounce-web"
ssh $remote_host mkdir -p /var/www/html/public/${target}
rsync -av ./*.html ./*.css ./*.js ./images ./pkg ${remote_host}:/var/www/html/public/${target}/
