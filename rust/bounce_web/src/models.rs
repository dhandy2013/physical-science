use crate::colors::Color;
use bounce::simulator::{
    Arena, Disc, DiscID, DiscTag, Position, Simulator, Velocity,
};
use core::f64::consts::PI;
use rand::{Rng, SeedableRng};
use rand_xoshiro::SplitMix64;

pub struct SimObj {
    disc_id: DiscID,
    color: Color,
}

impl SimObj {
    pub fn new(disc_id: DiscID, color: Color) -> Self {
        Self { disc_id, color }
    }
    pub fn disc_id(&self) -> DiscID {
        self.disc_id
    }
    pub fn color(&self) -> Color {
        self.color
    }
}

pub fn create_big_small_model() -> (Simulator, Vec<SimObj>) {
    let mut sim = Simulator::new(
        Arena::try_new(
            -0.5, // left
            0.7,  // top
            0.5,  // right
            -0.7, // bottom
        )
        .expect("Arena parameters should be correct"),
    );
    let mut obj_list = Vec::new();
    let s = 2.0; // speed scale
    let v2 = 0.3156854249492381 * s;
    let v1 = v2 * 0.25;
    let a = PI / 4.0;

    let disc_id = sim
        .create_disc(
            Disc::try_new(
                Position::new(0.0, 0.0),
                Velocity::new(0.0, 0.0),
                0.2, // radius in meters
                1.0, // mass in kilograms
                DiscTag::default(),
            )
            .expect("Disc parameters should be correct"),
        )
        .expect("Disc should have been within arena bounds");
    obj_list.push(SimObj::new(disc_id, Color::Red));

    let disc_id = sim
        .create_disc(
            Disc::try_new(
                Position::new(-0.26793786053180507, -0.26793786053180507),
                Velocity::new(v1 * a.cos(), v1 * a.sin()),
                0.1,  // radius in meters
                0.25, // mass in kilograms
                DiscTag::default(),
            )
            .expect("Disc parameters should be correct"),
        )
        .expect("Disc should have been within arena bounds");
    obj_list.push(SimObj::new(disc_id, Color::Red));

    let disc_id = sim
        .create_disc(
            Disc::try_new(
                Position::new(0.4, 0.4),
                Velocity::new(-v2 * a.cos(), -v2 * a.sin()),
                0.05,   // radius in meters
                0.0625, // mass in kilograms
                DiscTag::default(),
            )
            .expect("Disc parameters should be correct"),
        )
        .expect("Disc should have been within arena bounds");
    obj_list.push(SimObj::new(disc_id, Color::Red));

    (sim, obj_list)
}

pub fn create_diffusion_model() -> (Simulator, Vec<SimObj>) {
    let (left, bottom, width, height) = (-1.0, -0.5, 2.0, 1.0);
    let mut sim = Simulator::new(
        Arena::try_new(
            left,            // left
            bottom + height, // top
            left + width,    // right
            bottom,          // bottom
        )
        .expect("Arena parameters should be correct"),
    );
    let mut obj_list = Vec::new();
    // Create a rectangular array of evenly spaced objects,
    // all with the same speed but different directions.
    let (x_scale, y_scale) = (0.2, 0.2);
    let (x_offset, y_offset) = (x_scale / 2.0, y_scale / 2.0);
    let r = x_offset / 3.0;
    let base_speed = 0.2;
    let mut rng: SplitMix64 = SeedableRng::seed_from_u64(1776);
    let mut create_discs = |x_orig, y_orig, color| {
        for j in 0..5 {
            for i in 0..5 {
                let x = x_orig + (i as f64 * x_scale) + x_offset;
                let y = y_orig + (j as f64 * y_scale) + y_offset;
                let a: f64 = rng.gen_range(0.0..(2.0 * PI));
                let vx = base_speed * a.cos();
                let vy = base_speed * a.sin();
                let disc_id = sim
                    .create_disc(
                        Disc::try_new(
                            Position::new(x, y),
                            Velocity::new(vx, vy),
                            r,   // radius in meters
                            0.1, // mass in kilograms
                            DiscTag::default(),
                        )
                        .expect("Disc parameters should be correct"),
                    )
                    .expect("Disc should have been within arena bounds");
                obj_list.push(SimObj::new(disc_id, color));
            }
        }
    };
    create_discs(left, bottom, Color::Blue);
    create_discs(left + (width / 2.0), bottom, Color::Green);
    (sim, obj_list)
}
