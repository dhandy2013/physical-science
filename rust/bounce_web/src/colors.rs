//! Color enum for very simple graphics applications.

/// CSS color name for each enumerated color.
/// The numeric value of the color variant is the index into this array.
const COLOR_NAMES: [&'static str; 8] = [
    "black", "red", "orange", "yellow", "green", "blue", "violet", "white",
];

#[derive(Clone, Copy, Debug)]
pub enum Color {
    Black = 0,
    Red = 1,
    Orange = 2,
    Yellow = 3,
    Green = 4,
    Blue = 5,
    Violet = 6,
    White = 7,
}

impl Color {
    /// Return the CSS color name for the color value.
    pub fn as_str(&self) -> &'static str {
        COLOR_NAMES[*self as usize]
    }

    pub const fn into_color_index(self) -> u8 {
        self as u8
    }

    pub const fn highest_color_index() -> u8 {
        Self::White as u8
    }
}

/// This is used mainly to transfer the list of color names to JavaScript.  The
/// JavaScript code then builds an Array of color name strings that can be
/// indexed by color number.
pub struct ColorNameIterator {
    index: usize,
}

impl ColorNameIterator {
    pub fn new() -> Self {
        Self { index: 0 }
    }
}

impl Iterator for ColorNameIterator {
    type Item = &'static str;

    fn next(&mut self) -> Option<Self::Item> {
        if self.index < COLOR_NAMES.len() {
            let i = self.index;
            self.index += 1;
            Some(COLOR_NAMES[i])
        } else {
            None
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_color_name_iterator() {
        // FYI: ``iter`` does not need to be mutable because it gets consumed.
        let iter = ColorNameIterator::new();
        let mut color_count: usize = 0;
        for color_name in iter {
            // Do a spot check on the first and last colors
            if color_count == 0 {
                assert_eq!(color_name, "black");
            } else if color_count == Color::highest_color_index() as usize {
                assert_eq!(color_name, "white");
            }
            color_count += 1;
        }
        assert_eq!(color_count, COLOR_NAMES.len());
        assert_eq!(color_count, Color::highest_color_index() as usize + 1);
    }

    #[test]
    fn test_color_enum_indexing() {
        use Color::*;
        let colors = [Black, Red, Orange, Yellow, Green, Blue, Violet, White];
        for i in 0..colors.len() {
            let color = colors[i];
            assert_eq!(color.into_color_index() as usize, i);
            assert!(i < COLOR_NAMES.len());
        }
    }
}
