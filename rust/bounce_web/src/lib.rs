use bounce::simulator::Simulator;
use log::info;
use wasm_bindgen::prelude::{wasm_bindgen, JsValue};

pub mod colors;
pub mod models;

/// UI controller object with methods callable from JavaScript
#[wasm_bindgen]
pub struct Controller {
    sim: Option<Simulator>,
    obj_list: Vec<models::SimObj>,
    plot_mode_id: String,
}

#[wasm_bindgen]
impl Controller {
    pub fn new() -> Self {
        Self {
            sim: None,
            obj_list: Vec::new(),
            plot_mode_id: String::default(),
        }
    }

    /// Change the plot mode based on the `plot_mode_id` string.
    pub fn set_plot_mode(&mut self, plot_mode_id: &str) -> Result<(), JsValue> {
        info!(
            "bounce_web::set_plot_mode({:?}) - prev mode: {:?}",
            plot_mode_id, self.plot_mode_id
        );
        match plot_mode_id {
            "main-menu" => {
                self.sim = None;
                self.obj_list = Vec::new();
            }
            "plot-big-small" => {
                let (sim, obj_list) = models::create_big_small_model();
                self.sim = Some(sim);
                self.obj_list = obj_list;
            }
            "plot-diffusion" => {
                let (sim, obj_list) = models::create_diffusion_model();
                self.sim = Some(sim);
                self.obj_list = obj_list;
            }
            _ => {
                return Err(JsValue::from_str(
                    format!("Invalid plot mode: {}", plot_mode_id).as_str(),
                ))
            }
        };
        self.plot_mode_id = plot_mode_id.to_owned();
        Ok(())
    }

    pub fn plot_mode_id(&self) -> String {
        self.plot_mode_id.clone()
    }

    /// Update the state of the simulator for delta time `dt`.
    pub fn update(&mut self, dt: f64) -> Result<(), JsValue> {
        match &mut self.sim {
            Some(sim) => sim.update(dt).map_err(|err| JsValue::from_str(err)),
            None => Ok(()),
        }
    }

    /// Return the total elapsed seconds of simulator running time since
    /// the current model was started, or zero if there is no current model.
    pub fn get_time(&self) -> f64 {
        match &self.sim {
            Some(sim) => sim.time(),
            None => 0.0,
        }
    }

    /// Return the number of objects in the display list.
    pub fn get_display_list_size(&self) -> u32 {
        match &self.sim {
            Some(sim) => sim.num_discs() as u32,
            None => 0,
        }
    }

    /// Copy the current positions, sizes, and colors of all objects to be
    /// displayed into buffers. Return the number of objects for which data was
    /// actually copied (may be less than the size of the buffers if the buffers
    /// were larger than necessary.)
    pub fn copy_display_list_bufs(
        &self,
        x_coords: &mut [f64],
        y_coords: &mut [f64],
        radii: &mut [f64],
        color_codes: &mut [u8],
    ) -> u32 {
        if let Some(sim) = &self.sim {
            // Get the size of the smallest of the supplied buffers.
            let buf_size = usize::min(
                x_coords.len(),
                usize::min(
                    y_coords.len(),
                    usize::min(radii.len(), color_codes.len()),
                ),
            );
            let num_objs_to_copy = usize::min(buf_size, self.obj_list.len());
            for (i, sim_obj) in
                self.obj_list[..num_objs_to_copy].iter().enumerate()
            {
                if let Some(disc) = sim.get_disc_by_id(sim_obj.disc_id()) {
                    x_coords[i] = disc.x();
                    y_coords[i] = disc.y();
                    radii[i] = disc.r();
                    color_codes[i] = sim_obj.color().into_color_index();
                } else {
                    // Somehow disc_id was not valid - make invisible object
                    x_coords[i] = 0.0;
                    y_coords[i] = 0.0;
                    radii[i] = 0.0;
                    color_codes[i] = colors::Color::Black.into_color_index();
                }
            }
            num_objs_to_copy as u32
        } else {
            0
        }
    }
}

/// Iterator that is constructable and usable from Javascript that returns the
/// names of the colors known to the simulator in index order.
#[wasm_bindgen]
pub struct ColorNameIterator {
    iter: colors::ColorNameIterator,
}

#[wasm_bindgen]
impl ColorNameIterator {
    pub fn new() -> Self {
        Self {
            iter: colors::ColorNameIterator::new(),
        }
    }

    /// Return the next color name as a JavaScript string,
    /// or JavaScript null if we are at the end of iteration.
    pub fn next(&mut self) -> JsValue {
        match self.iter.next() {
            Some(color_name) => JsValue::from_str(color_name),
            None => JsValue::null(),
        }
    }
}

#[wasm_bindgen(start)]
pub fn start() -> Result<(), JsValue> {
    // Initialize panic logging
    #[cfg(feature = "console_error_panic_hook")]
    console_error_panic_hook::set_once();

    // Initialize regular logging
    wasm_logger::init(wasm_logger::Config::new(log::Level::Info));
    info!("bounce_web::start() - initialized logging");
    Ok(())
}
