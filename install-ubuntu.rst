Installation on Ubuntu Linux
============================

Experienced developers can probably adapt these instructions for other
operating systems.

Install these binary dependencies first::

    sudo apt-get install python3-tk libagg-dev libfreetype6-dev libpng-dev libtk-img-dev libcairo2-dev tk-dev texlive pandoc

Run these commands in the repository root directory to create a Python virtual
environment::

    python3 -m venv env

Activate the virtual environment and install the Python dependencies::

    source env/bin/activate
    pip install -r requirements.txt

Technical Notes
---------------

You can skip this section if ``pip install`` worked for you. Otherwise,
maybe this info will help you resolve missing dependencies.

If you tried "pip install" without having first intalled all of the C/C++
dependencies, such as tk-dev, then matplotlib will get built and the build
will get cached with missing modules such as _tkagg.so. After installing
tk-dev, re-install matplotlib like this::

    pip uninstall -y matplotlib
    pip --no-cache-dir install -U matplotlib

See: http://stackoverflow.com/questions/32188180/from-matplotlib-backends-import-tkagg-importerror-cannot-import-name-tkagg

These are C/C++ dependencies that had to be satisfied in order for ``pip
install`` to be able to compile the necessary Python extensions (discovered
through trial-and-error):

- libagg
- freetype2 (ft2build.h)
- libpng
- tkagg (The C/C++ header for Tk (tk.h))
- tk-dev
- cairocffi or pycairo
