Physical Science Simulations
============================

This is an open source repository of software and data I created for
demonstrating and teaching principles of Physical Science.

See the `LICENSE.txt <LICENSE.txt>`__ file for terms of use. There is NO
WARRANTY. These files are shared in the hope that someone finds this
helpful.

Installation
------------

This software has been developed on Linux using Python_. Experienced
developers can probably get it to work on other platforms.

.. _Python: https://www.python.org/

First you either need to `clone this repository`_ or else `download the files`_
in this repository and extract them.

.. _`clone this repository`: https://confluence.atlassian.com/bitbucket/clone-a-repository-223217891.html
.. _`download the files`: https://bitbucket.org/dhandy2013/physical-science/downloads/

Next, see `install-ubuntu.rst <install-ubuntu.rst>`__ for instructions on
how to prepare to run this software on Ubuntu Linux. (TL;DR for those
familiar with Python: create a `Python virtual environment`_, then ``pip install -r
requirements.txt``.  But there are some C/C++ dependencies to ``apt-get
install`` first.)

.. _`Python virtual environment`: https://docs.python.org/3/library/venv.html


Running the Spring-Mass Simulations
-----------------------------------

I created a physics engine that simulates the motion of arbitrary masses
connected to one another by ideal springs, with simple friction proportional
to the speed of the masses. I then set up several scenarios that can be run
from a simple text menu.

Some of these scenarios are designed to demonstrate wave propagation,
reflection, and superposition. But other scenarios are just for fun and
curiousity.

If you have difficulty running this software (or just want to see the
results without the hassle), see my `video of the stretch-rope scenario
<http://handysoftware.com/physical-science/out-rope-720p.mp4>`__ (800K).

After activating your `Python virtual environment`_ according to the
`installation`_ instructions above, run this command in the root directory
of this repository::

    ./springmass

It will print a text menu of simulation scenarios. Enter the name or number
of the scenario and press Enter to start it. A graphical window will come
up. Press the green start button in the upper-right corner of the window to
run the scenario. Click the red stop button (visible when the scenario is
running) to pause the scenario and "freeze" the motion. Click the green
start button again to resume.

The first time you run this program, I suggest you run scenario #9
(``transverse_wave``). Then try #8 (``stretch_rope``). Enjoy!


Keeping Things Real
-------------------

I made an effort to ensure that my physics engine is an accurate
mathematical model based on Newton's laws of motion. As part of that effort
I created some `ipython notebooks`_ with the relevant math. You can run
these notebooks from the root of the repository clone using Jupyter
Notebook, which was installed when you followed the `installation`_
instructions.

.. _`ipython notebooks`: https://plot.ly/python/ipython-notebook-tutorial/

How to Run the Jupyter Notebooks
................................

Run this command in the root of the repository clone::

    jupyter notebook

It will pop up a new page in your web browser showing the contents of the
repository. The ``.ipynb`` files are ipython notebooks; click on those to
run them.

Try clicking on ``demo-signal-gen.ipynb``. The notebook will open up in a
new browser tab. In the menu at the top of the page select Cell -> Run All.
Then scroll down till you see the graph of the damped oscillation. Click the
play button and you can hear what it sounds like.

How to Produce HTML Files from Notebook Files
.............................................

Run these commands::

    jupyter nbconvert --to html forcefield.ipynb
    jupyter nbconvert --to html force-friction-problem-0.ipynb
    jupyter nbconvert --to html force-friction-problem-1.ipynb
