#!/usr/bin/env python3
"""
Read CSV data files containing weather data, plot high and low temps.

Data files must be in the current directory.
Their names must match this pattern:

    WeatherRecord-<name>*.csv

Where "<name>" is assumed to be a person's name.

Each row in the file will be processed if it matches this format:

    mm/dd/YYYY,High Temp,Low Temp

Where "mm/dd/YYYY" is a date in month/day/4-digit-year format.
Extra rows and columns are ignored.

For each input data file found, picture files named
"WeatherRecord-<name>.png" is produced containing a plot.
"""
import csv
from datetime import datetime
import os
import re
import sys
import time

# http://matplotlib.org/faq/usage_faq.html
import matplotlib
#matplotlib.use('TkAgg')  # This ended up being the default
matplotlib.use('agg')
import matplotlib.pyplot as plt


def convert_col_to_number(row, i):
    """
    row: A sequence of items in a csv file row
    i: zero-based index of column
    Return the column value converted to a number, or None
    if it isn't a number.
    """
    if i >= len(row):
        return None
    m = re.match(r'([\d\.]+)', row[i].strip())
    if not m:
        return None
    try:
        return float(m.group(1))
    except ValueError:
        return None


def load_data_from_file(filename):
    """
    filename: Name of .csv file
    return: A list of dictionaries with these keys:
        date: datetime object
        high_temp: High temperature for the day
        low_temp: Low temperature for the day
    """
    print("Reading", filename)
    result = []
    with open(filename) as f:
        reader = csv.reader(f)
        # Data columns:
        # Date, High Temp, Low Temp, ...
        for row in reader:
            if not row:
                continue
            try:
                x_date = datetime.strptime(row[0], '%m/%d/%Y')
            except ValueError:
                continue
            day_info = {}
            day_info['date'] = x_date
            day_info['high_temp'] = convert_col_to_number(row, 1)
            day_info['low_temp'] = convert_col_to_number(row, 2)
            result.append(day_info)
    return result


def plot_tempdata_as_lines(data_list, title='Temperature plot'):
    x_data = []
    high_temp_data = []
    low_temp_data = []
    for day_info in data_list:
        x_data.append(day_info['date'])
        high_temp_data.append(day_info['high_temp'])
        low_temp_data.append(day_info['low_temp'])
    fig = plt.figure()  # Create a "figure window"
    nrows, ncols, axnum = 1, 1, 1
    ax = fig.add_subplot(nrows, ncols, axnum)
    ax.set_title(title)
    ax.set_xlabel('Day')
    ax.set_ylabel('Temperature')
    ax.xaxis_date()
    # Comment following line for auto scale
    #ax.set_ylim(0, 10)
    ax.plot(x_data, high_temp_data)
    ax.plot(x_data, low_temp_data)
    fig.autofmt_xdate()
    return fig


def plot_tempdata_as_bars(data_list, title='Temperature plot'):
    x_data = []
    height_list = []
    bottom_list = []
    for day_info in data_list:
        high_temp = day_info['high_temp']
        low_temp = day_info['low_temp']
        if high_temp is None or low_temp is None:
            continue
        x_data.append(day_info['date'])
        bottom_list.append(low_temp)
        height_list.append(high_temp - low_temp)
    # Create a "figure window"
    fig = plt.figure(frameon=False, figsize=(10.24, 7.68), dpi=100)
    nrows, ncols, axnum = 1, 1, 1
    ax = fig.add_subplot(nrows, ncols, axnum)
    ax.set_title(title)
    ax.set_xlabel('Day')
    ax.set_ylabel('Temperature')
    ax.xaxis_date()
    # Comment following line for auto scale
    #ax.set_ylim(0, 10)
    ax.bar(x_data, height_list, bottom=bottom_list)
    fig.autofmt_xdate()
    return fig


def get_weather_record_files():
    """
    Return list of (filename, personname)
    """
    weather_record_files = []
    for filename in os.listdir(os.curdir):
        m = re.match(r'weatherrecord-(\w+).*\.csv', filename, re.I)
        if not m:
            continue
        if not os.path.isfile(filename):
            continue
        personname = m.group(1)
        weather_record_files.append((filename, personname))
    return weather_record_files


def main():
    # Configuration
    show_interactive = False
    plot_func = plot_tempdata_as_bars
    # Find the data files in the current directory
    weather_record_files = get_weather_record_files()
    for filename, personname in weather_record_files:
        data_list = load_data_from_file(filename)
        title = "Weather data from {0}".format(personname)
        # Plot the data
        print("matplotlib backend:", matplotlib.get_backend())
        plt.ioff()  # turn off interactive updates
        fig = plot_func(data_list, title=title)
        plot_filename = "WeatherRecord-{0}.png".format(personname)
        print("Saving the data plot to", plot_filename)
        fig.savefig(plot_filename)
        if show_interactive:
            plt.ion()  # Turn on interactive manipulation
            fig.show()
            if matplotlib.get_backend().lower() == 'tkagg':
                # Work around bug in tkagg backend that causes it not to wait
                manager = fig.canvas.manager
                window = manager.window  # a tkinter.Tk object
                window.mainloop()
        plt.close(fig)

if __name__ == '__main__':
    sys.exit(main())
